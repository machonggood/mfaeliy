package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.FriendCircleAdapter;
import com.minfeng.project.aeliy.adapter.GeneralAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.FriendCircleComment;
import com.minfeng.project.aeliy.entity.FriendCircleInfo;
import com.minfeng.project.aeliy.entity.User;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.listener.OnScrollYListener;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.utils.BlurTransformation;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 朋友圈
 */
public class FriendCircleActivity extends BaseActivity implements SpringView.OnFreshListener, SwipeRefreshLayout.OnRefreshListener, OnScrollYListener.OnListScrollListener {

    private int screenWidth;    //屏幕宽度
    private TextView headerTv;
    private ListView listView;
    private SpringView springView;
    private GeneralAdapter baseAdapter;
    private View headerView, headerLineView;
    private ImageButton leftView, rightView;
    private SwipeRefreshLayout refreshLayout;
    private ImageButton sendMsgIb, pushCircleIb;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_friend_circle);
    }

    @Override
    protected void initViews() {
        this.headerLineView = super.findViewById(R.id.activity_friend_circle_header_line_view);
        this.sendMsgIb = super.findViewById(R.id.activity_friend_circle_send_msg_ib);
        this.pushCircleIb = super.findViewById(R.id.activity_friend_circle_push_ib);
        this.refreshLayout = super.findViewById(R.id.general_refresh_list_sr_view);
        this.screenWidth = super.getResources().getDisplayMetrics().widthPixels;
        this.springView = super.findViewById(R.id.general_refresh_list_sp_view);
        this.listView = super.findViewById(R.id.general_refresh_list_view);
        this.headerView = super.findViewById(R.id.header_content_layout);
        this.springView.setFooter(new DefaultFooter(App.mContext));
        this.baseAdapter = new FriendCircleAdapter(this);
        this.refreshLayout.setDistanceToTriggerSync(100);
        this.listView.setAdapter(this.baseAdapter);
        this.springView.setEnableHeader(false);
        this.springView.setEnableFooter(false);
        this.rightView = super.findViewById(R.id.header_layout_right_operate_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.headerLineView.setBackgroundColor(Color.parseColor("#00f3f3f3"));
        this.headerView.setBackgroundColor(Color.parseColor("#00ffffff"));
        this.headerTv.setTextColor(Color.parseColor("#00000000"));
        this.rightView.setImageResource(R.mipmap.icon_white_more);
        this.leftView.setImageResource(R.mipmap.icon_white_back);
        this.headerTv.setText("朋友圈");
        this.addListHeaderView();


        List<FriendCircleInfo> friendCircleInfos = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            FriendCircleInfo friendCircleInfo = new FriendCircleInfo();
            friendCircleInfo.setPortrait("FtzeUuMxhmqBYQ8_zV_PYvcMblOS");
            friendCircleInfo.setNickname("说不出的爱");
            friendCircleInfo.setCreate_time("2019-05-13 11:22:02");
            friendCircleInfo.setContent("大叔带我去吃了海鲜自助，聊得非常开心！");
            List<String> photos = new ArrayList<>();
            photos.add("FgGQcpnIJPrBDlfqA4wFb20MjO5l");
            if (i % 2 == 0) {
                photos.add("FqEdQaKJapGeqp2-W6vlzPEo7PHK");
                photos.add("FkYhy3bbEb-3c09kS6KTv9uMZDJY");
                photos.add("FvKku6FkATCK0ebrCHyLcDteCR0r");
            }
            friendCircleInfo.setPicture(new Gson().toJson(photos));
            List<FriendCircleComment> circleComments = new ArrayList<>();
            circleComments.add(new FriendCircleComment("放羊的星星", "今晚出来耍不？"));
            circleComments.add(new FriendCircleComment("放羊的星星", "吃9块钱的麻辣烫，不喝汤。"));
            friendCircleInfo.setComment(circleComments);
            friendCircleInfos.add(friendCircleInfo);
        }
        this.baseAdapter.onRefresh(friendCircleInfos);
    }

    /**
     * 添加header
     */
    private void addListHeaderView() {
        View view = LayoutInflater.from(App.mContext).inflate(R.layout.friend_circle_header_item, null);
        View shieldLayerView = view.findViewById(R.id.friend_circle_header_item_shield_layer_view);
        View userView = view.findViewById(R.id.friend_circle_header_item_user_view);
        ImageView userIv = view.findViewById(R.id.friend_circle_header_item_user_iv);
        ImageView bgIv = view.findViewById(R.id.friend_circle_header_item_bg_iv);
        ViewGroup.LayoutParams slLp = shieldLayerView.getLayoutParams();
        ViewGroup.LayoutParams userLp = userView.getLayoutParams();
        ViewGroup.LayoutParams bgLp = bgIv.getLayoutParams();
        bgLp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        bgLp.height = (int) (this.screenWidth * 0.75f);
        bgIv.setLayoutParams(bgLp);
        userLp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        userLp.height = (int) (this.screenWidth * 0.75f);
        userView.setLayoutParams(userLp);
        slLp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        slLp.height = (int) (this.screenWidth * 0.75f);
        shieldLayerView.setLayoutParams(slLp);
        this.listView.addHeaderView(view);
        this.initUserInfo(userIv, bgIv);
    }

    private void initUserInfo(ImageView userIv, ImageView bgIv) {
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        if (userInfo != null) {
            User user = userInfo.getUser();
            if (user.getPhoto() != null) {
                String photo = Tools.getPhoto(user.getPhoto()[0]);
                RequestOptions options = new RequestOptions().placeholder(R.mipmap.icon_user_default).centerCrop().circleCrop();
                Glide.with(App.mContext).asBitmap().load(photo).apply(options).into(userIv);
                options = new RequestOptions().transform(new BlurTransformation());
                Glide.with(App.mContext).asBitmap().load(photo).apply(options).into(bgIv);
            }
        }
    }

    @Override
    protected void initListener() {
        this.listView.setOnScrollListener(new OnScrollYListener(this.listView, this));
        this.listView.setOnItemClickListener(new ClickItemListener());
        this.pushCircleIb.setOnClickListener(new ClickListener());
        this.sendMsgIb.setOnClickListener(new ClickListener());
        this.rightView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.refreshLayout.setOnRefreshListener(this);
        this.springView.setListener(this);
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_friend_circle_push_ib) {
            Intent intent = new Intent(this, FriendCirclePushActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_friend_circle_send_msg_ib) {

        }
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadmore() {

    }

    @Override
    public void onScrollY(int scrolledY, boolean enable) {
        this.refreshLayout.setEnabled(enable);
        this.conversionHeaderColor(scrolledY);
    }

    /**
     * 变换header颜色
     */
    private void conversionHeaderColor(int scrolledY) {
        float pct = scrolledY / (this.screenWidth * 0.75f - UIHelper.getStatusBarHeight());
        pct = pct > 1.0 ? 1.0f : pct < 0.0 ? 0f : pct;
        String alpha = this.getAlpha(pct);
        this.headerLineView.setBackgroundColor(Color.parseColor(String.format("#%sf3f3f3", alpha)));
        this.headerView.setBackgroundColor(Color.parseColor(String.format("#%sffffff", alpha)));
        this.rightView.setColorFilter(Color.parseColor(String.format("#%s000000", alpha)));
        this.leftView.setColorFilter(Color.parseColor(String.format("#%s000000", alpha)));
        this.headerTv.setTextColor(Color.parseColor(String.format("#%s000000", alpha)));
    }

    private String getAlpha(double pct) {
        double i = Math.round(pct * 100) / 100.0d;
        int alpha = (int) Math.round(i * 255);
        String hex = Integer.toHexString(alpha).toUpperCase();
        if (hex.length() == 1) hex = "0" + hex;
        return hex;
    }

}
