package com.minfeng.project.aeliy.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.mvp.presenter.BasePresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.view.LoadingDialog;

public abstract class BaseMvpActivity<V extends IBaseView, T extends BasePresenter<V>> extends BaseActivity {

    protected T mPresenter;
    protected LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.mPresenter = this.createPresenter();
        this.mPresenter.attach((V) this);
        super.onCreate(savedInstanceState);
    }

    protected void showDialog() {
        this.loadingDialog = new LoadingDialog();
        Bundle args = new Bundle();
        args.putInt("strId", R.string.loading);
        this.loadingDialog.setArguments(args);
        FragmentTransaction ft = super.getFragmentManager().beginTransaction();
        ft.add(this.loadingDialog, "loadingDialog");
        ft.commitAllowingStateLoss();
    }

    protected void dismissDialog() {
        if (this.loadingDialog != null) {
            this.loadingDialog.dismissAllowingStateLoss();
            this.loadingDialog = null;
        }
    }

    public Activity getBusActivity() {
        return this.mActivity;
    }

    public FragmentManager getMyFragmentManager() {
        return super.getFragmentManager();
    }

    public abstract T createPresenter();
}
