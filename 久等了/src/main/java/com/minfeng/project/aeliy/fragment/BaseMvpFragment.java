package com.minfeng.project.aeliy.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.minfeng.project.aeliy.mvp.presenter.BasePresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;

public abstract class BaseMvpFragment<V extends IBaseView, T extends BasePresenter<V>> extends BaseFragment {

    protected T mPresenter;

    public abstract T createPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPresenter = this.createPresenter();
        this.mPresenter.attach((V)this);
    }

    public Activity getBusActivity() {
        return this.mActivity;
    }

    public FragmentManager getMyFragmentManager() {
        return super.mActivity.getFragmentManager();
    }
}
