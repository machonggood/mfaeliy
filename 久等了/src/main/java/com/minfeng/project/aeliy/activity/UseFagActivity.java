package com.minfeng.project.aeliy.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;

/**
 * 常见问题
 */
public class UseFagActivity extends BaseActivity {

    private ImageButton leftView;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_use_fag);
    }

    @Override
    protected void initViews() {
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        View headerView = super.findViewById(R.id.header_content_layout);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        headerView.setBackgroundColor(Color.WHITE);
        headerTv.setTextColor(Color.BLACK);
        headerTv.setText("常见问题");
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {

    }

}
