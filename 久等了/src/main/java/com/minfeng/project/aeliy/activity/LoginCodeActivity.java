package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.mvp.presenter.SystemPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.List;

/**
 * 验证码登录
 */
public class LoginCodeActivity extends BaseMvpActivity<IBaseView, SystemPresenter> implements IBaseView {

    private Button loginBt;
    private ImageButton leftView;
    private EditText phoneEt, authCodeEt;
    private CountDownTimer countDownTimer;
    private Button sendCodeBt, passLoginBt;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_login_code);
    }

    @Override
    public SystemPresenter createPresenter() {
        return new SystemPresenter();
    }

    @Override
    protected void initViews() {
        this.passLoginBt = super.findViewById(R.id.activity_login_code_pass_login_bt);
        this.authCodeEt = super.findViewById(R.id.activity_login_code_auth_et);
        this.phoneEt = super.findViewById(R.id.activity_login_code_phone_et);
        this.sendCodeBt = super.findViewById(R.id.activity_login_code_send_bt);
        this.loginBt = super.findViewById(R.id.activity_login_code_login_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
    }

    @Override
    protected void initListener() {
        this.passLoginBt.setOnClickListener(new ClickListener());
        this.sendCodeBt.setOnClickListener(new ClickListener());
        this.loginBt.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_login_code_login_bt) {
            String authCode = this.authCodeEt.getText().toString();
            String phone = this.phoneEt.getText().toString();
            if (StringUtils.isEmpty(phone) || phone.length() != 11) {
                UIHelper.showToast("请输入11位的电话号码！");
            } else if (StringUtils.isEmpty(authCode)) {
                UIHelper.showToast("请输入验证码！");
            } else {
                this.mPresenter.loginByPhone(phone, authCode);
            }
        } else if (v.getId() == R.id.activity_login_code_send_bt) { //发送验证码
            String phone = this.phoneEt.getText().toString();
            if (StringUtils.isEmpty(phone)) {
                UIHelper.showToast("请输入11位的电话号码！");
            } else {
                this.mPresenter.sendCode(phone);
            }
        } else if (v.getId() == R.id.activity_login_code_pass_login_bt) {   //密码登录
            Intent intent = new Intent(this, LoginPassActivity.class);
            super.startActivity(intent);
        }
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 200) {
            if (result.getTag().equals("sendCode")) {
                this.setDownTimer();
                return;
            } else if (result.getTag().equals("loginByPhone")) {
                if (!StringUtils.isEmpty(result.getData())) {
                    TokenInfo tokenInfo = new Gson().fromJson(result.getData(), TokenInfo.class);
                    TokenManager.Instance().setTokenInfo(tokenInfo);
                    SocketManager.Instance().connectSocket();
                    return;
                }
            }
        }
        UIHelper.showToast(result, "请求失败，请重试！");
    }

    /**
     * 验证码发送成功倒计时
     */
    private void setDownTimer() {
        if (this.countDownTimer != null) {
            this.countDownTimer.cancel();
        }
        this.countDownTimer = new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                sendCodeBt.setText(millisUntilFinished / 1000 + "s后重发");
                sendCodeBt.setEnabled(false);
            }

            @Override
            public void onFinish() {
                sendCodeBt.setText("获取验证码");
                sendCodeBt.setEnabled(true);
            }
        };
        this.countDownTimer.start();
    }

    @Override
    protected void onPause() {
        if (this.countDownTimer != null) this.countDownTimer.cancel();
        this.sendCodeBt.setText("获取验证码");
        this.sendCodeBt.setEnabled(true);
        super.onPause();
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("userLoginSuccess");            //Socket已连接，登录成功
        return methodNames;
    }

    /**
     * socket连接成功
     */
    public void socketLoginSuccess(UserInfo userInfo) {
        UIHelper.userLoginSuccess(this.mActivity, userInfo);
    }
}
