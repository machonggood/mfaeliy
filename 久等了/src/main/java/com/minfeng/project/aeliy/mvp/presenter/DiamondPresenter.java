package com.minfeng.project.aeliy.mvp.presenter;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.mvp.model.DiamondModel;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * 钻石业务
 */
public class DiamondPresenter extends BasePresenter<IBaseView> {

    private DiamondModel model = new DiamondModel();

    /**
     * 获取钻石账单
     * @param type 0全部 1充值 2消费
     * @param page
     */
    public void findDiamondBill(int type, int page) {
        if (this.wef.get() != null) {
            List<String> strings = new ArrayList<>();
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            strings.add("参加一次线下约会");
            String data = new Gson().toJson(strings);
            Result result = new Result("findDiamondBill", Constants.SUCCESS, 101, data);
            this.wef.get().findDataCallBack(result, page);
        }
    }
}
