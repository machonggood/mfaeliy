package com.minfeng.project.aeliy.im;


/**
 * Created by mc on 2018/8/17.
 */
public interface IUIKitCallBack {

    void onSuccess(Object data);

    void onError(String module, int errCode, String errMsg);

}
