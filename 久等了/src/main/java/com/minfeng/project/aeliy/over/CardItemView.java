package com.minfeng.project.aeliy.over;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.minfeng.project.aeliy.entity.NearUserInfo;

/**
 * 卡片View项
 */
@SuppressLint("NewApi")
public class CardItemView extends FrameLayout {

    private NearUserInfo nearUserInfo;
    private ObjectAnimator alphaAnimator;

    public void setNearUserInfo(NearUserInfo nearUserInfo) {
        this.nearUserInfo = nearUserInfo;
    }

    public NearUserInfo getNearUserInfo() {
        return nearUserInfo;
    }

    public CardItemView(Context context) {
        this(context, null);
    }

    public CardItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void bindLayoutResId(int layoutResId) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(layoutResId, null);
        super.addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public void setVisibilityWithAnimation(final int visibility, int delayIndex) {
        if (visibility == View.VISIBLE && getVisibility() != View.VISIBLE) {
            super.setAlpha(0);
            super.setVisibility(visibility);
            if (this.alphaAnimator != null) this.alphaAnimator.cancel();
            this.alphaAnimator = ObjectAnimator.ofFloat(this, "alpha", 0.0f, 1.0f);
            this.alphaAnimator.setDuration(360);
            this.alphaAnimator.setStartDelay(delayIndex * 200);
            this.alphaAnimator.start();
        }
    }

}
