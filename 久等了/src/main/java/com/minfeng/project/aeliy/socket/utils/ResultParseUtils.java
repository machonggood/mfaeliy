package com.minfeng.project.aeliy.socket.utils;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.MethodManager;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.socket.bean.CommandID;
import com.minfeng.project.aeliy.socket.bean.NearUsersResponse;
import com.minfeng.project.aeliy.socket.bean.SocketResponse;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.UIHelper;

/**
 * socket回调数据解析
 */
public class ResultParseUtils {

    /**
     * 数据解析
     *
     * @param result
     */
    public static void doParse(String result) {
        result = result.trim();
        SocketResponse socketResponse = new Gson().fromJson(result, SocketResponse.class);
        int m = socketResponse.getM();
        int s = socketResponse.getS();
        if (s > 0) {
            if (s == 1 && m == 1) {  //登录
                int retCode = socketResponse.getRetCode();  //0成功 其他失败
                if (retCode == 0) {
                    UserInfo userInfo = new Gson().fromJson(result, UserInfo.class);
                    MethodManager.Instance().invokingMethod(CommandID.USER_LOGIN_SUCCESS_MESSAGE, userInfo);
                } else {
                    TokenManager.Instance().setTokenInfo(null);
                    SocketManager.Instance().disconnectSocket();
                    if (retCode == 2) {         //账号被禁
                        MethodManager.Instance().invokingMethod(CommandID.LOGIN_FAIL_ACCOUNT_FORBID_MESSAGE);
                    } else if (retCode == 1) {  //设备在其他地方登录
                        MethodManager.Instance().invokingMethod(CommandID.DEVICES_RESTS_LOGIN_MESSAGE);
                    }
                }
            } else if (s == 3 && m == 3) {  //经纬度
                UIHelper.log(socketResponse.getRetCode() == 0 ? "位置信息上报成功！" : "位置信息上报失败！");
            } else if (s == 1 && m == 3) {  //设备信息
                UIHelper.log(socketResponse.getRetCode() == 0 ? "设备信息上报成功！" : "设备信息上报失败！");
            } else if (s == 2 && m == 3) {  //推送关系绑定
                UIHelper.log(socketResponse.getRetCode() == 0 ? "推送关系绑定成功！" : "推送关系绑定失败！");
            }



            else if (s == 101 && m == 3) {  //编辑个人信息

            } else if (s == 103 && m == 3) {  //第一次登陆时设置 昵称性别生日
                if (socketResponse.getRetCode() == 0) { //编辑成功
                    MethodManager.Instance().invokingMethod(CommandID.EDIT_USER_BASICS_SUCCESS_MESSAGE);
                } else {    //编辑失败
                    MethodManager.Instance().invokingMethod(CommandID.EDIT_USER_BASICS_FAILED_MESSAGE, socketResponse.getReason());
                }
            } else if (s == 102 && m == 4) {  //设置漫游位置

            } else if (s == 104 && m == 4) {  //获取向我显示的信息

            } else if (s == 101 && m == 4) {  //向我显示设置

            } else if (s == 105 && m == 4) {  //附近的人
                NearUsersResponse nearUsersResponse = new Gson().fromJson(result, NearUsersResponse.class);
                MethodManager.Instance().invokingMethod(CommandID.FIND_NEAR_BY_USER_MESSAGE, nearUsersResponse.getData());
            } else if (s == 101 && m == 5) {  //喜欢/不喜欢 /关注/取消关注 收藏/取消收藏

            } else if (s == 102 && m == 5) {  //获取人物关系

            } else if (s == 103 && m == 5) {  //通知有人喜欢你

            } else if (s == 104 && m == 5) {  //获取匹配的人的详

            } else if (s == 101 && m == 7) {  //发表朋友圈

            } else if (s == 102 && m == 7) {  //朋友圈刷新

            } else if (s == 103 && m == 7) {  //朋友圈评论

            } else if (s == 101 && m == 8) {  //单独获取用户余额

            } else if (s == 102 && m == 8) {  //A端用户获取消费等级

            } else if (s == 101 && m == 10) { //A端发布约会

            } else if (s == 102 && m == 10) { //报名

            } else if (s == 103 && m == 10) { //报名通知

            } else if (s == 104 && m == 10) { //约会发起者同意某人报名

            } else if (s == 105 && m == 10) { //通知用户报名被同意

            } else if (s == 106 && m == 10) { //同意赴约

            } else if (s == 107 && m == 10) { //通知约会发起者有人同意赴约

            } else if (s == 108 && m == 10) { //获取附近的约会

            } else if (s == 113 && m == 10) { //发起点对点约会

            } else if (s == 114 && m == 10) { //点对点约会通知

            } else if (s == 115 && m == 10) { //同意/拒绝 点对点约会

            } else if (s == 116 && m == 10) { //通知对方同意或拒绝点对点约会

            } else if (s == 101 && m == 11) { //匿名举报

            } else if (s == 101 && m == 12) { //服务器通知客户端拉取消息

            } else if (s == 102 && m == 12) { //客户端拉取未读消息列表

            }
        }
    }

}
