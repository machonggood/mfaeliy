package com.minfeng.project.aeliy.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class BasePagerAdapter extends FragmentStatePagerAdapter {

	private View mCurrentView;
	private FragmentManager fm;
	private List<Fragment> fragments;

	public BasePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
		super(fm);
		this.fm = fm;
		this.fragments = fragments != null ? fragments : new ArrayList<Fragment>();
	}

	@Override
	public int getCount() {
		return this.fragments.size();
	}

	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	@Override
	public int getItemPosition(Object object) {
		return PagerAdapter.POSITION_NONE;
	}

	public void onRefresh(List<Fragment> fragments) {
		if (this.fragments != null) {
			FragmentTransaction ft = this.fm.beginTransaction();
			for (Fragment f : this.fragments) {
				ft.remove(f);
			}
			ft.commit();
			ft = null;
			this.fm.executePendingTransactions();
		}
		this.fragments.addAll(fragments);
		notifyDataSetChanged();
	}

	public void delete(int position) {
		this.fragments.remove(position);
		super.notifyDataSetChanged();
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		Object obj = super.instantiateItem(container, position);
		return obj;
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		if (object instanceof View) {
			this.mCurrentView = (View) object;
		} else if (object instanceof Fragment) {
			Fragment fragment = (Fragment) object;
			if (fragment != null) {
				fragment.setMenuVisibility(true);
				fragment.setUserVisibleHint(true);
			}
			this.mCurrentView = fragment.getView();
		}
	}

	public View getPrimaryItem() {
		return mCurrentView;
	}

	public List<Fragment> getFragments() {
		return this.fragments;
	}
}