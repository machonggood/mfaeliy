package com.minfeng.project.aeliy.im;

import android.graphics.Bitmap;

import com.minfeng.project.aeliy.utils.Tools;

import java.io.Serializable;

/**
 * Created by mc on 15/8/27.
 */
public class Emoji implements Serializable {

    private String desc;
    private String filter;
    private Bitmap icon;
    private int width = deaultSize;
    private int height = deaultSize;
    private static final int deaultSize = Tools.getPxByDp(32);

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
