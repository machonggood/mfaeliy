package com.minfeng.project.aeliy.socket.bean;

/**
 * 上报设备信息
 */
public class ReportDeviceInfo extends SocketRequest {

    private int type;       //1安卓 2苹果
    private String brand;   //品牌
    private String model;   //型号

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
