package com.minfeng.project.aeliy.entity;

import java.io.File;

public class FileParam {

    private String name;
    private File file;

    public FileParam(String name, File file) {
        this.name = name;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
