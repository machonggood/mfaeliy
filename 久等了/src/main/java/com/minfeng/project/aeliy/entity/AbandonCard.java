package com.minfeng.project.aeliy.entity;

public class AbandonCard {

    private int finalX, finalY;
    private NearUserInfo nearUserInfo;

    public AbandonCard(int finalX, int finalY, NearUserInfo nearUserInfo) {
        this.finalX = finalX;
        this.finalY = finalY;
        this.nearUserInfo = nearUserInfo;
    }

    public int getFinalX() {
        return finalX;
    }

    public void setFinalX(int finalX) {
        this.finalX = finalX;
    }

    public int getFinalY() {
        return finalY;
    }

    public void setFinalY(int finalY) {
        this.finalY = finalY;
    }

    public NearUserInfo getNearUserInfo() {
        return nearUserInfo;
    }

    public void setNearUserInfo(NearUserInfo nearUserInfo) {
        this.nearUserInfo = nearUserInfo;
    }
}
