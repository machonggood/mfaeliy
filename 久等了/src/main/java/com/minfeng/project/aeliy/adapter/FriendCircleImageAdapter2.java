package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.view.ConstrainImageView2;

/**
 * 巴士圈图片适配器
 */
public class FriendCircleImageAdapter2 extends GeneralAdapter<String> {

    public FriendCircleImageAdapter2(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ImageView imageView = new ConstrainImageView2(App.mContext);
        String imgPath = (String) super.getItem(position);
        RequestOptions options = new RequestOptions().placeholder(R.mipmap.default_square_pic).centerCrop();
        Glide.with(App.mContext).load(Tools.getPhoto(imgPath)).apply(options).into(imageView);
        return imageView;
    }

}
