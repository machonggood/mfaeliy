package com.minfeng.project.aeliy.entity;

import java.io.Serializable;

/**
 * Created by stevem on 2016/9/13 0013.
 */
public class Result implements Serializable {

    private String tag;
    private int status;
    private int retCode;    //200正常操作  201数据验证失败  202无访问权限  203服务器问题  429请求太频繁
    private String msg;
    private String data;

    public Result(int retCode, String data) {
        this.retCode = retCode;
        this.data = data;
    }

    public Result(String tag, int status) {
        this.tag = tag;
        this.status = status;
    }

    public Result(String tag, int status, String msg) {
        this.tag = tag;
        this.msg = msg;
        this.status = status;
    }

    public Result(String tag, int status, int retCode, String msg, String data) {
        this.tag = tag;
        this.status = status;
        this.retCode = retCode;
        this.msg = msg;
        this.data = data;
    }

    public Result(String tag, int status, int retCode, String data) {
        this.tag = tag;
        this.status = status;
        this.retCode = retCode;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
