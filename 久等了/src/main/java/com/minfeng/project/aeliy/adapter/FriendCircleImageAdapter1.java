package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.view.ConstrainImageView2;

/**
 * 巴士圈图片适配器
 */
public class FriendCircleImageAdapter1 extends GeneralAdapter<String> {

    public FriendCircleImageAdapter1(Context context) {
        super(context);
    }

    @Override
    public int getCount() {
        return this.objects.size() > 3 ? 3 : this.objects.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ImageView imageView = new ConstrainImageView2(App.mContext);
        String imgPath = (String) super.getItem(position);
        RequestOptions options = new RequestOptions().placeholder(R.mipmap.default_square_pic).centerCrop();
        Glide.with(App.mContext).load(Tools.getPhoto(imgPath)).apply(options).into(imageView);
        if (position == 2) {
            RelativeLayout relativeLayout = new RelativeLayout(App.mContext);
            relativeLayout.addView(imageView);
            TextView textView = new TextView(App.mContext);
            textView.setTextColor(Color.WHITE);
            textView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            textView.setText(this.objects.size() + "张");
            textView.setPadding(15, 15, 15, 15);
            LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            textView.setBackgroundColor(0x70000000);
            relativeLayout.addView(textView, lp);
            return relativeLayout;
        } else {
            return imageView;
        }
    }

}
