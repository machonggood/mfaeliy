package com.minfeng.project.aeliy.mvp.view;

import android.app.Activity;
import android.app.FragmentManager;

import com.minfeng.project.aeliy.entity.Result;

public interface IBaseView {

    Activity getBusActivity();

    FragmentManager getMyFragmentManager();

    void findDataCallBack(Result result, Object object);

}
