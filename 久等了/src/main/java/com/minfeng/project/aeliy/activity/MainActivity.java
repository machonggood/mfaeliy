package com.minfeng.project.aeliy.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.GeneralAdapter;
import com.minfeng.project.aeliy.adapter.LikeChatAdapter;
import com.minfeng.project.aeliy.adapter.ObjectCardAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.NearUserInfo;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.User;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.mvp.presenter.MainPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.over.CardItemView;
import com.minfeng.project.aeliy.over.CardSlidePanel;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.minfeng.project.aeliy.view.EffectDialog;
import com.umeng.analytics.MobclickAgent;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseMvpActivity<IBaseView, MainPresenter> implements IBaseView, CardSlidePanel.CardSwitchListener, SpringView.OnFreshListener, SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {

    private ListView listView;
    private SpringView springView;
    private GeneralAdapter baseAdapter;
    private SwipeRefreshLayout refreshLayout;
    private ImageView userIconIv;
    private View abandonView, likeView;
    private DrawerLayout drawerLayout;
    private CardSlidePanel cardSlidePanel;
    private ObjectCardAdapter cardAdapter;
    private ImageButton leftView, rightView;
    private ImageButton qIb, collectIb, friendIb;
    private TextView nickNameTv, jiuDengLeTv, privilegeTv;
    private View friendCircleView, headerView, editUserView;

    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_main);
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("findNearByUser");            //获取附近的人
        return methodNames;
    }

    @Override
    protected void initViews() {
        this.refreshLayout = super.findViewById(R.id.general_refresh_list_sr_view);
        this.springView = super.findViewById(R.id.general_refresh_list_sp_view);
        this.listView = super.findViewById(R.id.general_refresh_list_view);
        this.springView.setFooter(new DefaultFooter(App.mContext));
        this.baseAdapter = new LikeChatAdapter(this.mActivity);
        this.listView.setDivider(new ColorDrawable( ContextCompat.getColor(this.mActivity, R.color.color_eeeeee)));
        this.listView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.item_0_5));
        this.listView.setAdapter(this.baseAdapter);
        this.springView.setEnableHeader(false);
        this.springView.setEnableFooter(false);
        this.setRefreshing(this.refreshLayout, true);

        this.friendCircleView = super.findViewById(R.id.user_center_item_friend_circle_tv);
        this.cardSlidePanel = super.findViewById(R.id.activity_main_card_slide_view);
        this.editUserView = super.findViewById(R.id.user_center_item_user_edit_view);
        this.drawerLayout = super.findViewById(R.id.activity_main_drawer_layout);
        this.abandonView = super.findViewById(R.id.activity_main_abandon_card_ib);
        this.userIconIv = super.findViewById(R.id.user_center_item_user_icon_iv);
        this.jiuDengLeTv = super.findViewById(R.id.user_center_item_jiudengle_tv);
        this.privilegeTv = super.findViewById(R.id.user_center_item_privilege_tv);
        this.nickNameTv = super.findViewById(R.id.user_center_item_nick_tv);
        this.likeView = super.findViewById(R.id.activity_main_like_card_ib);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.headerView = super.findViewById(R.id.header_content_layout);
        this.rightView = super.findViewById(R.id.header_layout_right_operate_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.collectIb = super.findViewById(R.id.activity_main_collect_ib);
        this.friendIb = super.findViewById(R.id.activity_main_friend_ib);
        this.qIb = super.findViewById(R.id.activity_main_q_ib);
        this.rightView.setImageResource(R.mipmap.icon_right);
        this.drawerLayout.setScrimColor(Color.TRANSPARENT);
        this.leftView.setImageResource(R.mipmap.icon_left);
        this.headerView.setBackgroundColor(Color.WHITE);
        this.leftView.setTag("self-control");    //自控
        headerTv.setText(R.string.app_name);
        headerTv.setTextColor(Color.BLACK);
        AnimationDrawable animationDrawable = (AnimationDrawable) this.qIb.getDrawable();
        this.cardAdapter = new ObjectCardAdapter(this.cardSlidePanel);
        this.cardSlidePanel.setAdapter(this.cardAdapter);
        this.sendNearByUserRequest();
        animationDrawable.start();
        this.initUserInfo();


        this.nickNameTv.setText("潇洒帅哥");
        List<NearUserInfo> nearUserInfos = new ArrayList<>();
        nearUserInfos.add(new NearUserInfo("1", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632547&di=b65d420e0040db457994e9c5c5a0c918&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201512%2F12%2F20151212193107_ujGZV.jpeg"));
        nearUserInfos.add(new NearUserInfo("2", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632546&di=39449254752e8693f11ca17fa642443d&imgtype=0&src=http%3A%2F%2Fimg5.hao123.com%2Fdata%2F1_42242836bf29aca0fb3dab75b4a9f9b6_510"));
        nearUserInfos.add(new NearUserInfo("3", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632546&di=bca70680fc107c897436f609dd9d64dd&imgtype=0&src=http%3A%2F%2Fi0.hexunimg.cn%2F2013-06-06%2F154926028.jpg"));
        nearUserInfos.add(new NearUserInfo("4", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632546&di=dbd253e1a6123b301af6c9d67a640fc4&imgtype=0&src=http%3A%2F%2Fimg2.ph.126.net%2F8Y1u9aYRhqT4KHumTO_y1w%3D%3D%2F6619210632305894354.jpg"));
        nearUserInfos.add(new NearUserInfo("5", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632545&di=fb5cf790dc7d963a37e54963fd7583a5&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F26f87e751478ca4b08d6259a5af9aa082ebea364127e7-cIjTwC_fw658"));
        nearUserInfos.add(new NearUserInfo("6", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632544&di=d7801cbec6cfa8aaac024ca0f52dc614&imgtype=0&src=http%3A%2F%2Fbbswater-fd.zol-img.com.cn%2Ft_s1200x5000%2Fg5%2FM00%2F0E%2F08%2FChMkJlrdXhCIHezjAAvWP5q8sbMAAn0iAA90kYAC9ZX715.jpg"));
        nearUserInfos.add(new NearUserInfo("7", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632543&di=7e3fc8821a821ff13e90616c5955ecb2&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2Fb4364e79ca2797d74b8134b4bb48fea40af428ef48d40-imG7pw_fw658"));
        nearUserInfos.add(new NearUserInfo("8", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587632542&di=8ff840f1e663d72f1854ffd090b97803&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Fsports%2Fpics%2Fhv1%2F62%2F42%2F1903%2F123753347.jpg"));
        nearUserInfos.add(new NearUserInfo("9", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724660&di=13447acdce360fe317561285fedd5c8c&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201608%2F22%2F20160822094104_N8zRM.jpeg"));
        nearUserInfos.add(new NearUserInfo("10", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724660&di=f49b691ac00ffad238e7768e60cc8475&imgtype=0&src=http%3A%2F%2Fimg0.ph.126.net%2Fiihq6Z4z1gnrMZQxQylEPA%3D%3D%2F1343480063940055185.jpg"));
        nearUserInfos.add(new NearUserInfo("11", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724660&di=95ec2393aa908f117e88c2279f0e46c5&imgtype=0&src=http%3A%2F%2Fimg0.ph.126.net%2FPMex2DbVlSJPCSiZzY7Nig%3D%3D%2F6598061526145408302.jpg"));
        nearUserInfos.add(new NearUserInfo("12", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724660&di=361802e156aa896e2c4db674f130b740&imgtype=0&src=http%3A%2F%2Fpic30.nipic.com%2F20130624%2F7447430_170522539000_2.jpg"));
        nearUserInfos.add(new NearUserInfo("13", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724659&di=dbccba03f7de46f1e1040b1ac2498de9&imgtype=0&src=http%3A%2F%2Fimg1.ph.126.net%2ForQJkDaxzIBgKXJWCmGTHA%3D%3D%2F6598293523099543147.jpg"));
        nearUserInfos.add(new NearUserInfo("14", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724658&di=57328fa45c0dfc17855aeb5eb393380f&imgtype=0&src=http%3A%2F%2Fs14.sinaimg.cn%2Fmiddle%2F69944b27hbcea4019854d%26690"));
        nearUserInfos.add(new NearUserInfo("15", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724658&di=7942830e59caf6d7d73b5eba4f6ffd80&imgtype=0&src=http%3A%2F%2Fimg0.ph.126.net%2FcgNRWG7IwsA9z0JXxX1FqA%3D%3D%2F6597206106099526960.jpg"));
        new Handler() {
            @Override
            public void handleMessage(Message msg) {
                cardAdapter.onRefresh(nearUserInfos);
                super.handleMessage(msg);
            }
        }.sendEmptyMessageDelayed(1000, 1500);
    }

    /**
     * 初始化用户信息
     */
    private void initUserInfo() {
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        if (userInfo != null) {
            User user = userInfo.getUser();
            this.nickNameTv.setText(user.getNickname());
            if (user.getPhoto() != null) {
                String photo = Tools.getPhoto(user.getPhoto()[0]);
                RequestOptions options = new RequestOptions().placeholder(R.mipmap.icon_user_default).centerCrop().circleCrop();
                Glide.with(App.mContext).asBitmap().load(photo).apply(options).into(this.userIconIv);
            }
        }
    }

    /**
     * 发送附近人的请求
     */
    private void sendNearByUserRequest() {
//        SocketRequest request = new SocketRequest();
//        request.setM(4);
//        request.setS(205);
//        String requestMessage = new Gson().toJson(request);
//        SocketManager.Instance().sendData(requestMessage);
    }

    /**
     * 获取附近的人
     */
    public void findNearByUser(final List<NearUserInfo> nearUserInfos) {
        if (nearUserInfos != null && nearUserInfos.size() > 0) {
            super.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cardAdapter.onRefresh(nearUserInfos);
                }
            });
        }
    }

    private ActionBarDrawerToggle getDrawerToggle() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,
                this.drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        return drawerToggle;
    }

    @Override
    protected void initListener() {
        this.cardSlidePanel.setOnClickListener(new ClickListener());
        this.friendCircleView.setOnClickListener(new ClickListener());
        this.editUserView.setOnClickListener(new ClickListener());
        this.drawerLayout.addDrawerListener(this.getDrawerToggle());
        this.privilegeTv.setOnClickListener(new ClickListener());
        this.jiuDengLeTv.setOnClickListener(new ClickListener());
        this.userIconIv.setOnClickListener(new ClickListener());
        this.abandonView.setOnClickListener(new ClickListener());
        this.rightView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.likeView.setOnClickListener(new ClickListener());
        this.collectIb.setOnClickListener(new ClickListener());
        this.friendIb.setOnClickListener(new ClickListener());
        this.cardSlidePanel.setOnCardSwitchListener(this);
        this.qIb.setOnClickListener(new ClickListener());
        this.refreshLayout.setOnRefreshListener(this);
        this.listView.setOnScrollListener(this);
        this.springView.setListener(this);
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.header_layout_right_operate_view) {
            this.toggleRightSliding();
        } else if (v.getId() == R.id.header_layout_left_operate_view) {
            this.toggleLeftSliding();
        } else if (v.getId() == R.id.activity_main_abandon_card_ib) {
            this.cardSlidePanel.speedinessBackOnBtnClick();
        } else if (v.getId() == R.id.activity_main_like_card_ib) {
            this.cardSlidePanel.speedinessLikeOnBtnClick();
        } else if (v.getId() == R.id.user_center_item_user_edit_view) {
            Intent intent = new Intent(this, UserEditActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.user_center_item_user_icon_iv) {
            Intent intent = new Intent(this, UpdatePhotoActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_main_q_ib) {          //打Q
            this.showEffectDialog(R.drawable.q_effect_play_receiver);
        } else if (v.getId() == R.id.activity_main_collect_ib) {    //收藏
            this.showEffectDialog(R.drawable.s_effect_play_receiver);
        } else if (v.getId() == R.id.activity_main_friend_ib) {     //加好友
            this.showEffectDialog(R.drawable.f_effect_play_receiver);
        } else if (v.getId() == R.id.user_center_item_jiudengle_tv) {
            this.toggleLeftSliding();
        } else if (v.getId() == R.id.user_center_item_friend_circle_tv) {   //朋友圈
            Intent intent = new Intent(this, FriendCircleActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.user_center_item_privilege_tv) {   //我的特权
            Intent intent = new Intent(this, UserPrivilegeActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_main_card_slide_view) {   //卡片
            CardItemView cardItemView = this.cardSlidePanel.getCardItemView();
            if (cardItemView != null) {
                Intent intent = new Intent(this, UserCenterActivity.class);
                super.startActivity(intent);
            }
        }
    }

    /**
     * 显示效果弹窗
     *
     * @param effectId
     */
    private void showEffectDialog(int effectId) {
        EffectDialog effectDialog = new EffectDialog();
        Bundle args = new Bundle();
        args.putInt("effectId", effectId);
        effectDialog.setArguments(args);
        FragmentTransaction ft = super.getFragmentManager().beginTransaction();
        ft.add(effectDialog, "effectDialog");
        ft.commitAllowingStateLoss();
    }

    /**
     * 该方法控制左侧边栏的显示和隐藏
     */
    private void toggleLeftSliding() {
        if (this.drawerLayout.isDrawerOpen(Gravity.END)) {  //右边打开
            this.drawerLayout.closeDrawer(Gravity.END);
            this.drawerLayout.openDrawer(Gravity.START);
        } else if (this.drawerLayout.isDrawerOpen(Gravity.START)) {
            this.drawerLayout.closeDrawer(Gravity.START);
        } else {
            this.drawerLayout.openDrawer(Gravity.START);
        }
    }

    /**
     * 该方法控制右侧边栏的显示和隐藏
     */
    private void toggleRightSliding() {
        if (this.drawerLayout.isDrawerOpen(Gravity.START)) {  //左边打开
            this.drawerLayout.closeDrawer(Gravity.START);
            this.drawerLayout.openDrawer(Gravity.END);
        } else if (this.drawerLayout.isDrawerOpen(Gravity.END)) {
            this.drawerLayout.closeDrawer(Gravity.END);
        } else {
            this.drawerLayout.openDrawer(Gravity.END);
        }
    }

    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - this.exitTime) > 2000) {
                UIHelper.showToast("再按一次退出程序!");
                this.exitTime = System.currentTimeMillis();
            } else {
                SocketManager.Instance().disconnectSocket();
                MobclickAgent.onKillProcess(this);
                UserManager.Instance().setUserInfo(null);
                super.finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void residueCardNumber(int index) {
        if (index == 0) this.sendNearByUserRequest();
    }

    @Override
    public void cardOperate(int type, NearUserInfo nearUserInfo) {
//        UserAction request = new UserAction();
//        request.setM(5);
//        request.setS(201);
//        request.setType(type == 0 ? 2 : 1);
//        request.setTo_id(nearUserInfo.getUid());
//        String requestMessage = new Gson().toJson(request);
//        SocketManager.Instance().sendData(requestMessage);
    }

    /**
     * 下拉刷新
     */
    @Override
    public void onRefresh() {
        this.mPresenter.findMyLikeChat(1, 1);
    }

    /**
     * 加载更多
     */
    @Override
    public void onLoadmore() {
        int pageIndex = 1;
        if (this.baseAdapter != null && this.baseAdapter.getCount() > 0) {
            pageIndex += (int) Math.ceil(this.baseAdapter.getCount() / 20.f);
        }
        this.mPresenter.findMyLikeChat(1, pageIndex);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        boolean enable = true;
        if (view != null && view.getChildCount() > 0) {
            boolean firstItemVisible = view.getFirstVisiblePosition() == 0;
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() == 0;
            enable = firstItemVisible && topOfFirstItemVisible;
        }
        this.refreshLayout.setEnabled(enable);
    }

    private void setRefreshing(final SwipeRefreshLayout mSRLayout, final boolean refreshing) {
        mSRLayout.post(new Runnable() {
            @Override
            public void run() {
                mSRLayout.setRefreshing(refreshing);
                if (refreshing) onRefresh();
            }
        });
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        this.springView.setEnableFooter(false);
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 101) {
            int page = Integer.parseInt(object.toString());
            Type typeOfT = new TypeToken<List<String>>() {}.getType();
            List<String> diamonds = new Gson().fromJson(result.getData(), typeOfT);
            this.baseAdapter.onRefresh(diamonds, page == 1);
//            this.springView.setEnableFooter(rechargeRecord.getPageIndex() < rechargeRecord.getPageCount());
        }
        this.fragmentHide();
    }

    protected void fragmentHide() {
        this.springView.onFinishFreshAndLoad();
        this.setRefreshing(this.refreshLayout, false);
    }
}
