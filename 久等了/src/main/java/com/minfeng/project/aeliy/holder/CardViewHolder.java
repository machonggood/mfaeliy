package com.minfeng.project.aeliy.holder;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.over.CardSlidePanel;

public class CardViewHolder {

    public ImageView objectIv;
    public ViewGroup contentVg;
    private View shieldLayerView;
    public ImageView likeIv, dontLikeIv, userIconIv;
    public TextView dacoNumberTv, likeNumberTv, nickNameTv, basicsInfoTv, photoNumberTv;

    public CardViewHolder(View view, CardSlidePanel cardSlidePanel) {
        Resources resources = App.mContext.getResources();
        this.shieldLayerView = view.findViewById(R.id.object_card_item_shield_layer_view);
        this.dontLikeIv = view.findViewById(R.id.object_card_item_dont_like_status_iv);
        this.photoNumberTv = view.findViewById(R.id.object_card_item_photo_number_tv);
        this.dacoNumberTv = view.findViewById(R.id.object_card_item_daco_number_tv);
        this.likeNumberTv = view.findViewById(R.id.object_card_item_like_number_tv);
        this.basicsInfoTv = view.findViewById(R.id.object_card_item_user_basics_tv);
        this.nickNameTv = view.findViewById(R.id.object_card_item_nick_name_tv);
        this.userIconIv = view.findViewById(R.id.object_card_item_user_icon_iv);
        this.contentVg = view.findViewById(R.id.object_card_item_content_view);
        this.likeIv = view.findViewById(R.id.object_card_item_like_status_iv);
        this.objectIv = view.findViewById(R.id.object_card_item_object_iv);

        int marginBottom = resources.getDimensionPixelSize(R.dimen.item_95);
        int padding = resources.getDimensionPixelSize(R.dimen.item_10_5);
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        ViewGroup.LayoutParams ivLp = this.objectIv.getLayoutParams();
        int heightPixels = cardSlidePanel.getMeasuredHeight();
        int widthPixels = displayMetrics.widthPixels;
        ivLp.height = heightPixels - marginBottom;
        ivLp.width = widthPixels - padding * 2;
        this.objectIv.setLayoutParams(ivLp);

        ViewGroup.LayoutParams slLp = this.shieldLayerView.getLayoutParams();
        slLp.width = ivLp.width;
        slLp.height = ivLp.height;
        this.shieldLayerView.setLayoutParams(slLp);

        padding = resources.getDimensionPixelSize(R.dimen.item_0_5);
        ViewGroup.LayoutParams conLp = this.contentVg.getLayoutParams();
        conLp.height = heightPixels - marginBottom + padding * 2;
        conLp.width = widthPixels;
        this.contentVg.setLayoutParams(conLp);
    }

}
