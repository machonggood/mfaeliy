package com.minfeng.project.aeliy.entity;

/**
 * 对象卡片
 */
public class ObjectCard {

    private int index;
    private String imgUrl = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1555927688182&di=09a64f9593d399813a538b72bff5699c&imgtype=0&src=http%3A%2F%2Fimg4.duitang.com%2Fuploads%2Fitem%2F201312%2F05%2F20131205171748_BeJcN.thumb.600_0.jpeg";

    public ObjectCard(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
