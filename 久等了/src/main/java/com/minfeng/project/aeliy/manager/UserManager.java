package com.minfeng.project.aeliy.manager;

import com.minfeng.project.aeliy.entity.UserInfo;

/**
 * 用户管理器
 * @author 马冲
 * @version 1.0
 */
public class UserManager {

    private UserInfo userInfo;
    private volatile static UserManager instance = null;

    public static UserManager Instance() {
        if (instance == null) {
            synchronized (UserManager.class) {
                if (instance == null) {
                    instance = new UserManager();
                }
            }
        }
        return instance;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
