package com.minfeng.project.aeliy.socket.bean;

import com.minfeng.project.aeliy.entity.NearUserInfo;

import java.util.List;

public class NearUsersResponse extends SocketResponse {

    private List<NearUserInfo> data;

    public List<NearUserInfo> getData() {
        return data;
    }

    public void setData(List<NearUserInfo> data) {
        this.data = data;
    }
}
