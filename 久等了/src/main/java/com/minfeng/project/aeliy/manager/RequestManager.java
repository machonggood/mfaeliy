package com.minfeng.project.aeliy.manager;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.HeaderParam;
import com.minfeng.project.aeliy.entity.NormalParam;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.utils.CommonUtils;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.minfeng.project.aeliy.view.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by stevem on 2016/7/11 0011.
 */
public class RequestManager implements Handler.Callback {

    private String tag;
    private Handler mHandler;
    private ResultCallback callback;
    private LoadingDialog loadingDialog;

    public void showDialog(FragmentManager fragmentManager) {
        this.loadingDialog = new LoadingDialog();
        Bundle args = new Bundle();
        args.putInt("strId", R.string.loading);
        this.loadingDialog.setArguments(args);
        this.loadingDialog.setCancelable(false);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(this.loadingDialog, "loadingDialog");
        ft.commitAllowingStateLoss();
    }

    public void dismissDialog() {
        if (this.loadingDialog != null) {
            this.loadingDialog.dismissAllowingStateLoss();
            this.loadingDialog = null;
        }
    }

    public RequestManager(String tag, ResultCallback callback) {
        this.mHandler = new Handler(this);
        this.callback = callback;
        this.tag = tag;
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == Constants.ERROR) {
            Result result = new Result(this.tag, Constants.ERROR);
            this.requestResult(result);
        } else if (msg.what == Constants.SUCCESS) {
            this.requestSuccess(msg.obj.toString());
        }
        return false;
    }

    private void requestSuccess(String resultStr) {
        try {
            if (StringUtils.isEmpty(resultStr)) {
                Result result = new Result(this.tag, Constants.ERROR);
                this.requestResult(result);
            } else {
                JSONObject obj = new JSONObject(resultStr);
                int code = obj.optInt("retCode");
                String msg = obj.optString("msg");
                String data = obj.optString("data");
                if (!StringUtils.isEmpty(data)) UIHelper.log(data);
                Result result = new Result(this.tag, Constants.SUCCESS, code, msg, data);
                this.requestResult(result);
            }
        } catch (JSONException e) {
            Result result = new Result(this.tag, Constants.ERROR);
            this.requestResult(result);
        }
    }

    public void requestError() {
        this.mHandler.sendEmptyMessage(Constants.ERROR);
    }

    public void requestResult(String resultStr) {
        Message msg = new Message();
        msg.what = Constants.SUCCESS;
        msg.obj = resultStr;
        this.mHandler.sendMessage(msg);
    }

    /**
     * @return 返回各个项目状态下的密钥
     */
    private String[] getSecretKey() {
        if (Constants.PROJECT_STATE == 1) {
            return Constants.DEVELOP_KEY_SECRET;
        } else if (Constants.PROJECT_STATE == 2) {
            return Constants.TEST_KEY_SECRET;
        } else {
            return Constants.PRODUCT_KEY_SECRET;
        }
    }

    /**
     * @return  返回各个项目状态下的定值
     */
    private String getSalt() {
        if (Constants.PROJECT_STATE == 1) {
            return Constants.DEVELOP_SALE;
        } else if (Constants.PROJECT_STATE == 2) {
            return Constants.TEST_SALE;
        } else {
            return Constants.PRODUCT_SALE;
        }
    }

    public void postAsync(String url, Map<String, String> paramMaps) {
        List<NormalParam> normalParams = new ArrayList<>();
        if (paramMaps != null) {
            for (Map.Entry entry: paramMaps.entrySet()) {
                String key = entry.getKey().toString();
                normalParams.add(new NormalParam(key, paramMaps.get(key)));
            }
            String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
            paramMaps.put("timestamp", timestamp);
            List<String> keyList = new ArrayList<>(paramMaps.keySet());
            Collections.sort(keyList);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < keyList.size(); i++) {
                String key = keyList.get(i);
                Object value = paramMaps.get(key);
                sb.append(key + "=" + value);
                if (i < keyList.size() - 1) sb.append("&");
            }
            String[] secretKey = this.getSecretKey();
            String paramStr = "secret=" + secretKey[1] + "&" + sb.toString();
            String sign = CommonUtils.MD5(paramStr).toUpperCase();
            normalParams.add(new NormalParam("appkey", secretKey[0]));
            normalParams.add(new NormalParam("timestamp", timestamp));
            normalParams.add(new NormalParam("sign", sign));
        }
        OkHttpManager okHttpManager = new OkHttpManager();
        okHttpManager.postAsync(url, normalParams, new OkHttpCallback());
    }

    public void getAsync(String url, Map<String, String> paramMaps) {
        if (paramMaps != null) {
            String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
            url += "?timestamp=" + timestamp;
            for (Map.Entry entry: paramMaps.entrySet()) {
                String key = entry.getKey().toString();
                url += "&" + key + "=" + paramMaps.get(key);
            }
            paramMaps.put("timestamp", timestamp);
            List<String> keyList = new ArrayList<>(paramMaps.keySet());
            Collections.sort(keyList);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < keyList.size(); i++) {
                String key = keyList.get(i);
                Object value = paramMaps.get(key);
                sb.append(key + "=" + value);
                if (i < keyList.size() - 1) sb.append("&");
            }
            String[] secretKey = this.getSecretKey();
            String paramStr = "secret=" + secretKey[1] + "&" + sb.toString();
            String sign = CommonUtils.MD5(paramStr).toUpperCase();
            url += "&appkey=" + secretKey[0] + "&sign=" + sign;
        }
        OkHttpManager okHttpManager = new OkHttpManager();
        okHttpManager.getAsync(url, new OkHttpCallback());
    }

    public void getAsync(String url) {
        OkHttpManager okHttpManager = new OkHttpManager();
        okHttpManager.getAsync(url, new OkHttpCallback());
    }

    public void postAsync(String url, List<NormalParam> normalParams) {
//        UserInfo userInfo = UserManager.Instance().getUserInfo();
//        if (userInfo != null) {
//            String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
//            StringBuffer sb = new StringBuffer(String.valueOf(userInfo.getId()));
//            sb.append(timestamp).append(userInfo.getToken()).append(this.getSalt());
//            String sig = CommonUtils.MD5(sb.toString());
//            List<HeaderParam> headerParams = new ArrayList<>();
//            headerParams.add(new HeaderParam("uid", String.valueOf(userInfo.getId())));
//            headerParams.add(new HeaderParam("timestamp", timestamp));
//            headerParams.add(new HeaderParam("sig", sig));
//            this.postAsync(url, normalParams, headerParams);
//        } else {
//            OkHttpManager okHttpManager = new OkHttpManager();
//            okHttpManager.postAsync(url, normalParams, new OkHttpCallback());
//        }
    }

    public void postAsync(String url, List<NormalParam> normalParams, List<HeaderParam> headerParams) {
        OkHttpManager okHttpManager = new OkHttpManager();
        okHttpManager.postAsync(url, normalParams, headerParams, new OkHttpCallback());
    }

    public class OkHttpCallback implements OkHttpManager.ResultCallback {
        @Override
        public void onError() {
            requestError();
        }
        @Override
        public void onResponse(Response response) {
            if (response.isSuccessful()) {
                try {
                    requestResult(response.body().string());
                } catch (Exception e) {
                    requestError();
                }
            } else {
                requestError();
            }
        }
    }

    public void requestResult(Result result) {
        this.dismissDialog();
        if (this.callback != null) {
            this.callback.findDataCallBack(result);
        }
    }

    public interface ResultCallback {

        void findDataCallBack(Result result);

    }
}
