package com.minfeng.project.aeliy.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.listener.NoDoubleClickListener;
import com.minfeng.project.aeliy.listener.NoDoubleItemClickListener;
import com.minfeng.project.aeliy.manager.ActivityManager;
import com.minfeng.project.aeliy.manager.MethodManager;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;

public abstract class BaseActivity extends AppCompatActivity {

    protected Activity mActivity;

    protected final String name = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = this;
        this.onMyCreate(savedInstanceState);
        ActivityManager.Instance().addAssignGroupActivity("allActivity", this);
        this.initViews();
        this.setBarStatus();
        this.setStatusBarStyle();
        this.setSystemBarTint();
        this.initListener();
        this.initCallBack();
    }

    /**
     * 初始化回调方法
     */
    private void initCallBack() {
        List<String> methodNames = this.getMethodNames();
        if (methodNames != null) {
            for (String methodName: methodNames) {
                MethodManager.Instance().put(methodName, this);
            }
        }
    }

    public class ClickListener extends NoDoubleClickListener {
        @Override
        public void onNoDoubleClick(View v) {
            if (v.getId() == R.id.header_layout_left_operate_view) {
                String tag = (String) v.getTag();
                if (tag == null || !tag.equals("self-control")) {
                    finish();
                    return;
                }
            }
            onDelayClick(v);
        }
    }

    public class ClickItemListener extends NoDoubleItemClickListener {
        @Override
        public void onNoDoubleItemClick(AdapterView<?> parent, View view, int position, long id) {
            onDelayItemClick(parent, view, position, id);
        }
    }

    /**
     * listView单项点击事件
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    public void onDelayItemClick(AdapterView<?> parent, View view, int position, long id) {
        UIHelper.log("listView item click");
    }

    /**
     * 设置沉浸式状态栏样式
     */
    private void setBarStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ViewGroup viewGroup = super.findViewById(R.id.header_content_layout);
            if (viewGroup != null) {
                View statusView = UIHelper.createStatusView(Color.TRANSPARENT);
                viewGroup.addView(statusView, 0);
            }
        }
    }

    /**
     * 解决4.4~5.0之间状态栏背景颜色的问题
     */
    protected void setSystemBarTint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {        //大于5.0
            Window window = super.getWindow();
            window.setStatusBarColor(Color.parseColor("#61000000"));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {   //大于4.4
            super.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            super.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintColor(Color.parseColor("#61000000"));
            tintManager.setStatusBarTintEnabled(true);
        }
    }

    /**
     * 设置沉浸式状态栏样式
     */
    protected void setStatusBarStyle() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.mActivity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.name);
        JPushInterface.onPause(this);
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.name);
        JPushInterface.onResume(this);
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onDestroy() {
        this.removeCallBack();
        super.onDestroy();
    }

    private void removeCallBack() {
        List<String> methodNames = this.getMethodNames();
        if (methodNames != null) {
            for (String methodName: methodNames) {
                MethodManager.Instance().remove(methodName, this);
            }
        }
    }

    protected abstract void initViews();

    protected abstract void initListener();

    protected abstract void onDelayClick(View v);

    protected abstract void onMyCreate(Bundle savedInstanceState);

    /**
     * 本类要监听的所有方法都在这里面
     * @return
     */
    protected List<String> getMethodNames() {
        List<String> methodNames = new ArrayList<>();
        methodNames.add("socketLoginFailForbid");       //登录失败，账号被禁
        methodNames.add("devicesRestsLogin");           //设备在其他地方登陆
        return methodNames;
    }

    /**
     * 登录失败，账号被禁
     */
    public void socketLoginFailForbid() {
        super.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                dialog.setTitle(R.string.app_name);
                dialog.setMessage("因为您的账号被人举报纯在违禁行为，所以被封停，如果需要申述请发邮件至：xxxxxxxxx，24小时内工作人员会根据您的反馈联系您。谢谢您的厚爱！");
                dialog.setCancelable(false);
                dialog.show();
            }
        });
    }

    /**
     * 设备在其他地方登陆
     */
    public void devicesRestsLogin() {
        super.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                dialog.setTitle(R.string.app_name);
                dialog.setMessage("账号在其他设备上登陆，如非本人操作请尽快修改密码！");
                dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(mActivity, LauncherActivity.class);
                        TokenManager.Instance().setTokenInfo(null);
                        UserManager.Instance().setUserInfo(null);
                        ActivityManager.Instance().finishAll();
                        startActivity(intent);
                    }
                });
                dialog.setCancelable(false);
                dialog.show();
            }
        });
    }
}
