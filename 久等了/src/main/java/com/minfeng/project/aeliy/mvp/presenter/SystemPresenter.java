package com.minfeng.project.aeliy.mvp.presenter;

import android.app.FragmentManager;

import com.minfeng.project.aeliy.mvp.model.SystemModel;
import com.minfeng.project.aeliy.mvp.view.IBaseView;

/**
 * 系统处理器
 */
public class SystemPresenter extends BasePresenter<IBaseView> {

    private SystemModel model = new SystemModel();

    /**
     * 发送验证码
     */
    public void sendCode(String phone) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.sendCode("sendCode", phone, fragmentManager, this);
        }
    }

    /**
     * 获取绑定手机验证码
     * @param phone
     */
    public void sendBindCode(String phone) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.sendBindCode("sendBindCode", phone, fragmentManager, this);
        }
    }

    /**
     * 获取修改密码手机验证码
     */
    public void sendUpdatePwdCaptcha() {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.sendUpdatePwdCaptcha("sendUpdatePwdCaptcha", fragmentManager, this);
        }
    }

    /**
     * 电话验证码登录
     * @param phone
     * @param authCode
     */
    public void loginByPhone(String phone, String authCode) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.loginByPhone("loginByPhone", phone, authCode, fragmentManager, this);
        }
    }

    /**
     * 手机号码绑定
     * @param phone
     * @param authCode
     */
    public void bindByPhone(String phone, String authCode) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.bindByPhone("bindByPhone", phone, authCode, fragmentManager, this);
        }
    }

    /**
     * 密码设置
     * @param password
     * @param captcha
     */
    public void updatePwd(String password, String captcha) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.updatePwd("updatePwd", password, captcha, fragmentManager, this);
        }
    }

    /**
     * 密码登录
     * @param phone
     * @param pass
     */
    public void loginByPhonePwd(String phone, String pass) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.loginByPhonePwd("loginByPhonePwd", phone, pass, fragmentManager, this);
        }
    }

    /**
     * 微信登录
     * @param openId
     * @param refreshToken
     * @param nickName
     * @param iconUrl
     */
    public void wxLogin(String openId, String refreshToken, String nickName, String iconUrl) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.wxLogin("wxLogin", openId, refreshToken, nickName, iconUrl, fragmentManager, this);
        }
    }

    /**
     * qq登录
     * @param openid
     * @param token
     */
    public void qqLogin(String openid, String token) {
        if (this.wef.get() != null) {
            FragmentManager fragmentManager = this.wef.get().getMyFragmentManager();
            this.model.qqLogin("qqLogin", openid, token, fragmentManager, this);
        }
    }
}
