package com.minfeng.project.aeliy.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * 避免和viewPager冲突
 * Created by machong on 2018/4/11.
 */
public class DefinedListView extends ListView {

    public DefinedListView(Context context) {
        super(context);
    }

    public DefinedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefinedListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private float xDistance, yDistance, xLast, yLast;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.xDistance = this.yDistance = 0f;
                this.xLast = ev.getX();
                this.yLast = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();
                this.xDistance += Math.abs(curX - this.xLast);
                this.yDistance += Math.abs(curY - this.yLast);
                this.xLast = curX;
                this.yLast = curY;
                if (this.xDistance > this.yDistance) {
                    return false;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }
}