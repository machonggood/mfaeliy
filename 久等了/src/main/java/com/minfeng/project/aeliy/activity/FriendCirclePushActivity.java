package com.minfeng.project.aeliy.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.search.core.PoiInfo;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.FriendCirclePushAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.FileParam;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.manager.CloudUploadManager;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.utils.GlideEngine;
import com.minfeng.project.aeliy.utils.MiniSizeFilter;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.SelectionCreator;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 朋友圈发布
 */
public class FriendCirclePushActivity extends BaseActivity {

    private GridView gridView;
    private ImageButton leftView;
    private Button pushBt, locationBt;
    private FriendCirclePushAdapter baseAdapter;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_friend_circle_push);
    }

    @Override
    protected void initViews() {
        this.locationBt = super.findViewById(R.id.activity_friend_circle_push_location_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.gridView = super.findViewById(R.id.activity_friend_circle_push_gv);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.pushBt = super.findViewById(R.id.activity_friend_circle_push_bt);
        this.baseAdapter = new FriendCirclePushAdapter(App.mContext);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        this.gridView.setAdapter(this.baseAdapter);
        headerTv.setTextColor(Color.BLACK);
        headerTv.setText("发布");
    }

    @Override
    protected void initListener() {
        this.gridView.setOnItemClickListener(new ClickItemListener());
        this.locationBt.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.pushBt.setOnClickListener(new ClickListener());
    }

    @Override
    public void onDelayItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (this.baseAdapter.getObjectCount() < 9 && position == this.baseAdapter.getCount() - 1) {   //点击添加图片
            if (UIHelper.isGetWritePermission()) {
                this.startImageSelectActivity();
            } else {
                UIHelper.showToast("文件读取权限被拒绝,请开启后重试!");
            }
        } else {    //点击查看图片
            Intent intent = new Intent(this, NativeImageCheckActivity.class);
            intent.putParcelableArrayListExtra("Uris", this.baseAdapter.getObjects());
            intent.putExtra("position", position);
            super.startActivityForResult(intent, 10010);
        }
    }

    /**
     * 开启图片选择器
     */
    private void startImageSelectActivity() {
        Set<MimeType> mimeTypes = new HashSet<>();
        mimeTypes.add(MimeType.JPEG);
        mimeTypes.add(MimeType.PNG);
        SelectionCreator sCreator = Matisse.from(this).choose(mimeTypes);
        if (UIHelper.isGetCameraPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sCreator.capture(true);
            String authority = UIHelper.getPackageName() + ".fileProvider";
            sCreator.captureStrategy(new CaptureStrategy(true, authority));
        }
        int minWidth = 400, minHeight = 600;
        int maxSizeInBytes = 8 * Filter.K * Filter.K;
        sCreator.countable(true)    //有序选择图片
                .maxSelectable(9)   //最大选择数量为9
                .addFilter(new MiniSizeFilter(minWidth, minHeight, maxSizeInBytes, mimeTypes)) // 控制宽高为400*400 以上，大小为8M以下
                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.item_120))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)  //图像选择和预览活动方向
                .thumbnailScale(0.85f)  //缩放比例
                .theme(R.style.Matisse_mfAeliy_Dracula)
                .imageEngine(new GlideEngine()) //加载方式
                .forResult(10010);  //请求码
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10010 && resultCode == RESULT_OK) {
            if (data != null) {
                List<Uri> selectUrls = Matisse.obtainResult(data);
                if (selectUrls != null) this.baseAdapter.onRefresh(selectUrls, true);
            }
        } else if (requestCode == 10020 && resultCode == RESULT_OK) {
            PoiInfo poiInfo = data.getParcelableExtra("poiInfo");
            String name = poiInfo != null ? poiInfo.getName() : "所在位置";
            this.locationBt.setTag(poiInfo);
            this.locationBt.setText(name);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_friend_circle_push_bt) {

        } else if (v.getId() == R.id.activity_friend_circle_push_location_bt) {
            BDLocation bdLocation = ((App)super.getApplication()).getBdLocation();
            if (bdLocation == null) {
                UIHelper.showToast("没有获取到位置信息！");
            } else {
                Intent intent = new Intent(this, LocationActivity.class);
                super.startActivityForResult(intent, 10020);
            }
        }
    }

    public void submitPhoto() {
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        if (tokenInfo == null) return;
        List<FileParam> fileParams = this.baseAdapter.getFileParams();
        final SparseArray<String> imgNetWorkUrls = new SparseArray<>();
        if (fileParams.size() > 0) {
            final ProgressDialog pDialog;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                pDialog = new ProgressDialog(this, android.R.style.Theme_Material_Light_Dialog);
            } else {
                pDialog = new ProgressDialog(this);
            }
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setMax(fileParams.size());
            pDialog.setCancelable(false);
            pDialog.setTitle("图片上传");
            pDialog.show();
            new CloudUploadManager() {
                @Override
                public void uploadCallback(String key, int index) {
                    imgNetWorkUrls.put(index, key);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialog.setProgress(imgNetWorkUrls.size());
                            if (pDialog.getProgress() == pDialog.getMax()) {
                                pDialog.dismiss();
//                                submit(imgNetWorkUrls, content);
                            }
                        }
                    });
                }
            }.uploadImgUri(fileParams, tokenInfo.getQiniu_token());
        }
    }

    /**
     * 提交数据到服务器
     */
    private void submit(SparseArray<String> imgNetWorkUrls, String content) {
        List<String> photos = new ArrayList<>();
        for (int i = 0; i < imgNetWorkUrls.size(); i++) {
            String imgNetWorkUrl = imgNetWorkUrls.valueAt(i);
            if (!StringUtils.isEmpty(imgNetWorkUrl)) photos.add(imgNetWorkUrl);
        }
//        this.mPresenter.addPostPush(communityTypeInfo.gettId(), content, images.toString());
    }
}
