package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;

/**
 * 用户钻石
 */
public class UserRechargeActivity extends BaseActivity {

    private Button rightView;
    private ImageButton leftView;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_user_recharge);
    }

    @Override
    protected void initViews() {
        this.rightView = super.findViewById(R.id.header_layout_right_operate_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        this.rightView.setTextColor(Color.BLACK);
        headerTv.setTextColor(Color.BLACK);
        this.rightView.setText("历史充值");
        headerTv.setText("钻石充值");
    }

    @Override
    protected void initListener() {
        this.rightView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.header_layout_right_operate_view) {
            Intent intent = new Intent(this, UserDiamondActivity.class);
            super.startActivity(intent);
        }
    }
}
