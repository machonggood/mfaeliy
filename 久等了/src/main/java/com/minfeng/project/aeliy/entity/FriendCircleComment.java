package com.minfeng.project.aeliy.entity;

public class FriendCircleComment {

    private String icon;
    private String nickname;
    private String content;

    public FriendCircleComment(String nickname, String content) {
        this.nickname = nickname;
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
