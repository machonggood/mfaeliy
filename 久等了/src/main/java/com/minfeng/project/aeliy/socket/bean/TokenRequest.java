package com.minfeng.project.aeliy.socket.bean;

public class TokenRequest extends SocketRequest {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
