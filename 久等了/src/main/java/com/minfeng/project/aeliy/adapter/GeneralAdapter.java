package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stevem on 2016/7/6 0006.
 */
public class GeneralAdapter<T> extends BaseAdapter {

    protected Context context;
    protected ArrayList<T> objects;

    public GeneralAdapter(Context context) {
        this.context = context;
        this.objects = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return this.objects.size();
    }

    @Override
    public Object getItem(int position) {
        return this.objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public void onRefresh(List<T> objects, boolean isReFresh) {
        if (isReFresh) {
            this.objects.clear();
        }
        this.onRefresh(objects);
    }

    public void onRefresh(List<T> objects) {
        if (objects != null) this.objects.addAll(objects);
        super.notifyDataSetChanged();
    }

    public void onRefresh(T object) {
        if (object != null) this.objects.add(object);
        super.notifyDataSetChanged();
    }

    public void delete(T obj) {
        this.objects.remove(obj);
        super.notifyDataSetChanged();
    }

    public void clear() {
        this.objects.clear();
        super.notifyDataSetChanged();
    }
}
