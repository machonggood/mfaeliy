package com.minfeng.project.aeliy.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.NearUserInfo;
import com.minfeng.project.aeliy.holder.CardViewHolder;
import com.minfeng.project.aeliy.over.CardAdapter;
import com.minfeng.project.aeliy.over.CardItemView;
import com.minfeng.project.aeliy.over.CardSlidePanel;
import com.minfeng.project.aeliy.utils.GlideRoundTransform;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.Tools;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 马冲
 * @version 1.0
 */
public class ObjectCardAdapter extends CardAdapter {

    private CardSlidePanel cardSlidePanel;
    private List<NearUserInfo> nearUserInfos;

    public ObjectCardAdapter(CardSlidePanel cardSlidePanel) {
        this.cardSlidePanel = cardSlidePanel;
        this.nearUserInfos = new ArrayList<>();
    }

    public void onRefresh(List<NearUserInfo> nearUserInfos) {
        this.nearUserInfos.addAll(nearUserInfos);
        super.notifyDataSetChanged();
    }

    @Override
    public int getLayoutId() {
        return R.layout.object_card_item;
    }

    @Override
    public int getCount() {
        return this.nearUserInfos.size();
    }

    @Override
    public void bindView(CardItemView cardItemView, int index) {
        CardViewHolder viewHolder;
        Object tag = cardItemView.getTag();
        if (tag != null) {
            viewHolder = (CardViewHolder) tag;
        } else {
            viewHolder = new CardViewHolder(cardItemView, this.cardSlidePanel);
            cardItemView.setTag(viewHolder);
        }
        NearUserInfo nearUserInfo = (NearUserInfo) this.getItem(index);
        cardItemView.setNearUserInfo(nearUserInfo);

        int resId = nearUserInfo.getSex() == 0 ? R.mipmap.card_default_woman : R.mipmap.card_default_man;
        RequestOptions options = new RequestOptions().placeholder(resId).transform(new GlideRoundTransform(App.mContext));
        String portraitUrl = Tools.getPhoto(nearUserInfo.getPortrait());
        Glide.with(App.mContext).load(portraitUrl).apply(options).into(viewHolder.objectIv);

        options = new RequestOptions().placeholder(R.mipmap.icon_user_default).centerCrop().circleCrop();
        Glide.with(App.mContext).load(portraitUrl).apply(options).into(viewHolder.userIconIv);

        viewHolder.basicsInfoTv.setText(null);
        viewHolder.nickNameTv.setText(nearUserInfo.getNickname());
        int sexResId = nearUserInfo.getSex() == 0 ? R.mipmap.nvhai : R.mipmap.man;
        String constellation = Tools.getConstellation("yyyy-MM-dd", nearUserInfo.getBirthday());
        viewHolder.basicsInfoTv.setCompoundDrawablesWithIntrinsicBounds(sexResId, 0, 0, 0);
        viewHolder.basicsInfoTv.setText("/ " + Tools.getAge("yyyy-MM-dd", nearUserInfo.getBirthday()) + "岁");
        if (!StringUtils.isEmpty(constellation)) viewHolder.basicsInfoTv.append(" / " + constellation);
        if (!StringUtils.isEmpty(nearUserInfo.getCity())) viewHolder.basicsInfoTv.append(" / " + nearUserInfo.getCity());
        int photoNumber = 0;
        if (!StringUtils.isEmpty(nearUserInfo.getPhoto())) {
            Type typeOfT = new TypeToken<List<String>>() {}.getType();
            List<String> photos = new Gson().fromJson(nearUserInfo.getPhoto(), typeOfT);
            photoNumber = photos.size();
        }
        viewHolder.photoNumberTv.setText(photoNumber + "张");
        viewHolder.likeNumberTv.setText(this.getLikeNumber(nearUserInfo.getLike_num()));
    }

    private String getLikeNumber(int likeNumber) {
        if (likeNumber < 1000) {
            return String.valueOf(likeNumber);
        } else {
            return Math.floor(likeNumber / 10000.f) + "W";
        }
    }

    @Override
    public Object getItem(int index) {
        return this.nearUserInfos.get(index);
    }

    @Override
    public int getObjectIndex(Object object) {
        return this.nearUserInfos.indexOf(object);
    }

    @Override
    public void addObject(NearUserInfo nearUserInfo) {
        this.nearUserInfos.add(0, nearUserInfo);
    }

    @Override
    public void removeObject(NearUserInfo nearUserInfo) {
        this.nearUserInfos.remove(nearUserInfo);
    }

}
