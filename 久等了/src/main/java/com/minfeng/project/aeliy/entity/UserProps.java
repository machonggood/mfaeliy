package com.minfeng.project.aeliy.entity;

import java.io.Serializable;

public class UserProps implements Serializable {

    private int num;
    /*
    *  1 . 砖石 2.:1天会员  3：3天会员 4：7天会员 5：15天会员 6：31天会员 7：鲜花 8：收藏 9：关注 10：短信 11：曝光
    * */
    private int item_id;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }
}
