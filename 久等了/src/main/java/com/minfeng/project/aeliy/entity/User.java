package com.minfeng.project.aeliy.entity;

public class User {

    private long uid; // ID
    private long timestamp; // 创建时间
    private String openId; // 对外公开的ID
    private String nickname; // 别名
    private String[] photo; //默认展示5张照片地址
    private String birthday; //生日
    private int age; //年龄，根据生日计算
    private String star; //星座
    private String sign; //?
    private String industry; //产业
    private String position; //中文地址?
    private String hometown; //家乡
    private String city; //城市
    private String[] hobby; //喜好
    private String[] music; //喜欢的音乐
    private String[] food; //喜欢的食物
    private String[] film; //喜欢的电影
    private String[] book; //喜欢的书
    private String[] visitedCities; //去过的城市
    private String qa; // 安全问题，找回密码？
    private String[] tag; //标签
    private int giftNum; //打q数量
    private int collectNum; //收藏我的数量
    private int followNum; //关注我的数量
    private int dateScore;// 百分制存储 约会结束之后的评分，最高9.0分
    private String log; // 最近的一条日志
    private int dateRate; //百分制存储，约会之后能成功到付款环节的几率
    private int liveness; //连续用登陆的天数，最大7天，每少一天数字减少1，最小为0
    private int state; // 20以下是登陆隐身等状态,20异常，21删号等
    private int sex;// 0女，1男，2：未设置

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String[] getPhoto() {
        return photo;
    }

    public void setPhoto(String[] photo) {
        this.photo = photo;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String[] getHobby() {
        return hobby;
    }

    public void setHobby(String[] hobby) {
        this.hobby = hobby;
    }

    public String[] getMusic() {
        return music;
    }

    public void setMusic(String[] music) {
        this.music = music;
    }

    public String[] getFood() {
        return food;
    }

    public void setFood(String[] food) {
        this.food = food;
    }

    public String[] getFilm() {
        return film;
    }

    public void setFilm(String[] film) {
        this.film = film;
    }

    public String[] getBook() {
        return book;
    }

    public void setBook(String[] book) {
        this.book = book;
    }

    public String[] getVisitedCities() {
        return visitedCities;
    }

    public void setVisitedCities(String[] visitedCities) {
        this.visitedCities = visitedCities;
    }

    public String getQa() {
        return qa;
    }

    public void setQa(String qa) {
        this.qa = qa;
    }

    public String[] getTag() {
        return tag;
    }

    public void setTag(String[] tag) {
        this.tag = tag;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public int getCollectNum() {
        return collectNum;
    }

    public void setCollectNum(int collectNum) {
        this.collectNum = collectNum;
    }

    public int getFollowNum() {
        return followNum;
    }

    public void setFollowNum(int followNum) {
        this.followNum = followNum;
    }

    public int getDateScore() {
        return dateScore;
    }

    public void setDateScore(int dateScore) {
        this.dateScore = dateScore;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public int getDateRate() {
        return dateRate;
    }

    public void setDateRate(int dateRate) {
        this.dateRate = dateRate;
    }

    public int getLiveness() {
        return liveness;
    }

    public void setLiveness(int liveness) {
        this.liveness = liveness;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
