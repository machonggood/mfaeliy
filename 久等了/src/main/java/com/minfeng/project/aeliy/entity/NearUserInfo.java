package com.minfeng.project.aeliy.entity;

import java.util.List;

public class NearUserInfo {

    private float last_login;
    private boolean online;
    private long uid;
    private String birthday;
    private int sex;
    private String nickname;
    private String photo;
    private String sign;
    private String portrait;
    private String industry;
    private String position;
    private String hometown;
    private String city;
    private String hobby;
    private String music;
    private String food;
    private String film;
    private String book;
    private String visited_cities;
    private String qa;
    private String tag;
    private int student_auth;
    private float distance;
    private int like_num;
    private String brand;
    private String platform;
    private String model;
    private List<NearUserMoment> moments;

    public NearUserInfo(String nickname, String portrait) {
        this.nickname = nickname;
        this.portrait = portrait;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public float getLast_login() {
        return last_login;
    }

    public void setLast_login(float last_login) {
        this.last_login = last_login;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getVisited_cities() {
        return visited_cities;
    }

    public void setVisited_cities(String visited_cities) {
        this.visited_cities = visited_cities;
    }

    public String getQa() {
        return qa;
    }

    public void setQa(String qa) {
        this.qa = qa;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getStudent_auth() {
        return student_auth;
    }

    public void setStudent_auth(int student_auth) {
        this.student_auth = student_auth;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getLike_num() {
        return like_num;
    }

    public void setLike_num(int like_num) {
        this.like_num = like_num;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<NearUserMoment> getMoments() {
        return moments;
    }

    public void setMoments(List<NearUserMoment> moments) {
        this.moments = moments;
    }
}
