package com.minfeng.project.aeliy.over;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.AbandonCard;
import com.minfeng.project.aeliy.entity.NearUserInfo;
import com.minfeng.project.aeliy.holder.CardViewHolder;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.ArrayList;
import java.util.List;

public class CardSlidePanel extends ViewGroup implements Handler.Callback {

    private Handler mHandler;
    private int yOffsetStep;            // view叠加垂直偏移量的步长
    private int viewCount = 4;          // viewGroup中view的最大个数
    private int itemMarginTop;          // 卡片距离顶部的偏移量
    private int bottomMarginTop;        // 底部按钮与卡片的margin值
    private float scaleStep = 0.08f;    // 逐步缩放倍率
    private CardAdapter baseAdapter;
    private float xDistanceThreshold;       // 左右滑动消失的临界值
    private ViewDragHelper mDragHelper;
    private boolean animationLock = false;  //动画锁
    private CardSwitchListener cardSwitchListener;
    private List<CardItemView> viewList = new ArrayList<>();            // 存放的是每一层的view，从顶到底
    private static final int MAX_SLIDE_DISTANCE_LINKAGE = 500;          // 水平距离+垂直距离
    private List<AbandonCard> abandonUsers = new ArrayList<>();         // 被丢弃的卡片
    private List<CardItemView> releasedViewList = new ArrayList<>();    // 手指松开后存放的view列表

    public CardSlidePanel(Context context) {
        this(context, null);
    }

    public CardSlidePanel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardSlidePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.card);
        this.yOffsetStep = (int) a.getDimension(R.styleable.card_yOffsetStep, this.yOffsetStep);
        this.itemMarginTop = (int) a.getDimension(R.styleable.card_itemMarginTop, this.itemMarginTop);
        this.bottomMarginTop = (int) a.getDimension(R.styleable.card_bottomMarginTop, this.bottomMarginTop);
        // 滑动相关类
        this.mDragHelper = ViewDragHelper.create(this, 10f, new DragHelperCallback());
        int widthPixels = App.mContext.getResources().getDisplayMetrics().widthPixels;
        this.mDragHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_BOTTOM);
        this.xDistanceThreshold = widthPixels * 0.20f;
        this.mHandler = new Handler(this);
        a.recycle();
    }

    public CardItemView getCardItemView() {
        return this.viewList.get(0);
    }

    public void setOnCardSwitchListener(CardSwitchListener cardSwitchListener) {
        this.cardSwitchListener = cardSwitchListener;
    }

    /**
     * 绑定适配器
     */
    private void doBindAdapter(int layoutId) {
        // 1. addView添加到ViewGroup中
        for (int i = 0; i < this.viewCount; i++) {
            LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            CardItemView cardItemView = new CardItemView(getContext());
            this.viewList.add(0, cardItemView);
            cardItemView.bindLayoutResId(layoutId);
            cardItemView.setVisibility(INVISIBLE);
            super.addView(cardItemView, lp);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.measureChildren(widthMeasureSpec, heightMeasureSpec);
        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int maxHeight = MeasureSpec.getSize(heightMeasureSpec);
        int measuredWidth = super.resolveSizeAndState(maxWidth, widthMeasureSpec, 0);
        int measuredHeight = super.resolveSizeAndState(maxHeight, heightMeasureSpec, 0);
        super.setMeasuredDimension(measuredWidth, measuredHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = super.getChildCount();
        for (int i = 0; i < childCount; i++) {
            CardItemView cardItemView = this.viewList.get(i);
            // 1. 先layout出来
            int childHeight = cardItemView.getMeasuredHeight();
            int viewLeft = (super.getWidth() - cardItemView.getMeasuredWidth()) / 2;
            cardItemView.layout(viewLeft, this.itemMarginTop, viewLeft + cardItemView.getMeasuredWidth(), this.itemMarginTop + childHeight);
            // 2. 调整位置
            int offset = this.yOffsetStep * (i > 2 ? 2 : i);
            float scale = 1 - this.scaleStep * (i > 2 ? 2 : i);
            cardItemView.offsetTopAndBottom(offset);
            // 3. 调整缩放、重心等
            cardItemView.setPivotY(cardItemView.getMeasuredHeight());
            cardItemView.setPivotX(cardItemView.getMeasuredWidth() / 2);
            cardItemView.setScaleX(scale);
            cardItemView.setScaleY(scale);
        }
    }

    public void setAdapter(CardAdapter cardAdapter) {
        this.baseAdapter = cardAdapter;
        this.doBindAdapter(this.baseAdapter.getLayoutId());
        this.baseAdapter.registerDataSetObserver(new CardDataSetObserver());
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == 1000) {
            this.mDragHelper.abort();
            this.computeScroll();
        }
        return false;
    }

    public class CardDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            int delay = 0;
            int count = baseAdapter.getCount();
            for (int i = 0; i < viewCount; i++) {
                CardItemView itemView = viewList.get(i);
                if (i < count) {
                    baseAdapter.bindView(itemView, i);
                    itemView.setVisibilityWithAnimation(View.VISIBLE, delay++);
                } else {
                    itemView.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    /**
     * 设置喜欢和丢弃透明度
     */
    private void setCardViewBtAlpha(CardItemView changedView, int left) {
        CardViewHolder vHolder = (CardViewHolder) changedView.getTag();
        float abc = (float) Math.abs(left) / Math.abs(this.xDistanceThreshold);
        if (left < 0) {
            vHolder.likeIv.setAlpha(0f);
            vHolder.dontLikeIv.setAlpha(abc > 1f ? 1f : abc);
        } else {
            vHolder.dontLikeIv.setAlpha(0f);
            vHolder.likeIv.setAlpha(abc > 1f ? 1f : abc);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDragHelper.shouldInterceptTouchEvent(ev);
    }

    private long startTime;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.startTime = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_UP:
                long endTime = System.currentTimeMillis();
                boolean isChecked = endTime - this.startTime < 0.1 * 1000L;
                if (isChecked) super.performClick();
                break;
        }
        this.mDragHelper.processTouchEvent(event);
        return true;
    }

    class DragHelperCallback extends ViewDragHelper.Callback {

        /**
         * 这是一个抽象类，必须去实现，也只有在这个方法返回true的时候下面的方法才会生效
         *
         * @param child
         * @param pointerId
         * @return
         */
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            if (baseAdapter == null
                    || baseAdapter.getCount() == 0
                    || child.getScaleX() <= 1.0f - scaleStep
                    || child.getVisibility() != View.VISIBLE || animationLock) {
                return false;
            }
            animationLock = true;
            getParent().requestDisallowInterceptTouchEvent(true);   //如果确定要滑动，就让touch事件交给自己消费
            return true;
        }

        /**
         * 当你拖动的View位置发生改变的时候回调
         *
         * @param changedView 你当前拖动的这个View
         * @param left        距离左边的距离
         * @param top         距离上边的距离
         * @param dx          x轴的变化量
         * @param dy          y轴的变化量
         */
        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
            CardItemView cardItemView = (CardItemView) changedView;
            setCardViewBtAlpha(cardItemView, left);
            processLinkageView(cardItemView);
        }

        /**
         * 当View停止拖拽的时候调用的方法
         *
         * @param releasedChild 你拖拽的这个View
         * @param xvel          x轴的速率
         * @param yvel          y轴的速率
         */
        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            animToSide((CardItemView) releasedChild);
        }

        /**
         * 竖直拖拽的时候回调的方法
         *
         * @param child 拖拽的View
         * @param top   距离顶部的距离
         * @param dy    变化量
         * @return
         */
        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            return top;
        }

        /**
         * 水平拖拽的时候回调的方法
         *
         * @param child 拖拽的View
         * @param left  距离左边的距离
         * @param dx    变化量
         * @return
         */
        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            return left;
        }
    }

    /**
     * 快速喜欢
     */
    public void speedinessLikeOnBtnClick() {
        if (this.mDragHelper.getViewDragState() == ViewDragHelper.STATE_IDLE) { //没有在拖拽
            CardItemView cardItemView = this.viewList.get(0);
            if (cardItemView.getVisibility() != View.VISIBLE) return;
            int childWidth = cardItemView.getMeasuredWidth();
            int childHeight = cardItemView.getMeasuredHeight();
            int finalY = (int) (this.bottomMarginTop + childHeight * 0.55f);
            NearUserInfo nearUserInfo = cardItemView.getNearUserInfo();
            this.vanishCallBack(2, nearUserInfo, childWidth, finalY);
            this.releasedViewList.add(cardItemView);
            if (this.mDragHelper.smoothSlideViewTo(cardItemView, childWidth, finalY)) {
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }
    }

    /**
     * 返回
     */
    public void speedinessBackOnBtnClick() {
        if (this.mDragHelper.getViewDragState() == ViewDragHelper.STATE_IDLE) { //没有在拖拽
            int abandonCount = this.abandonUsers.size();
            if (abandonCount > 0) {
                AbandonCard abandonCard = this.abandonUsers.get(abandonCount - 1);    //找到最后一个丢弃数据
                CardItemView cardItemView = this.viewList.get(this.viewCount - 1);    //拿到最后一个卡片
                cardItemView.setVisibility(View.VISIBLE);
                //调整卡片位置
                cardItemView.setLeft(abandonCard.getFinalX());
                cardItemView.setTop(abandonCard.getFinalY());
                cardItemView.setScaleX(1.f);
                cardItemView.setScaleY(1.f);
                cardItemView.setAlpha(1.f);
                //调整卡片顺序
                LayoutParams lp = cardItemView.getLayoutParams();
                super.removeViewInLayout(cardItemView);
                super.addViewInLayout(cardItemView, this.viewCount - 1, lp, true);
                //调整数据顺序
                this.viewList.remove(cardItemView);
                this.viewList.add(0, cardItemView);
                //将抛弃的数据重新装回结合
                NearUserInfo nearUserInfo = abandonCard.getNearUserInfo();
                this.baseAdapter.addObject(nearUserInfo);
                //移除这条数据
                this.abandonUsers.remove(abandonCard);
                //填充卡片数据
                this.baseAdapter.bindView(cardItemView, 0);
                //动画滑动到指定位置
                if (this.mDragHelper.smoothSlideViewTo(cardItemView, 0, this.bottomMarginTop)) {
                    ViewCompat.postInvalidateOnAnimation(this);
                }
            } else {
                UIHelper.showToast("没有需要撤回的内容了！");
            }
        }
    }

    /**
     * 对view重新排序
     */
    private synchronized void orderViewStack() {
        if (this.releasedViewList.size() == 0) return;
        CardItemView cardItemView = this.releasedViewList.get(0);
        // 1. 初始化喜欢抛弃透明度
        this.setCardViewBtAlpha(cardItemView, 0);
        // 2. 消失的卡片View位置重置，由于大多手机会重新调用onLayout函数，所以此处大可以不做处理，不信你注释掉看看
        cardItemView.offsetLeftAndRight(-cardItemView.getLeft());
        cardItemView.offsetTopAndBottom(this.bottomMarginTop - cardItemView.getTop() + yOffsetStep * 2);
        float scale = 1.0f - this.scaleStep * 2;
        cardItemView.setScaleX(scale);
        cardItemView.setScaleY(scale);
        cardItemView.setAlpha(0.f);
        // 3. 卡片View在ViewGroup中的顺次调整
        LayoutParams lp = cardItemView.getLayoutParams();
        super.removeViewInLayout(cardItemView);
        super.addViewInLayout(cardItemView, 0, lp, true);
        // 4. changedView填充新数据
        NearUserInfo nearUserInfo = cardItemView.getNearUserInfo();
        int nowIndex = this.baseAdapter.getObjectIndex(nearUserInfo);   //获取当前位置所在的索引
        int newIndex = nowIndex + this.viewCount;   //获取卡片组最后的索引
        if (newIndex < this.baseAdapter.getCount()) {
            this.baseAdapter.bindView(cardItemView, newIndex);
        } else {
            cardItemView.setVisibility(View.INVISIBLE);
        }
        // 5. viewList中的卡片view的位次调整
        this.releasedViewList.remove(0);
        this.viewList.remove(cardItemView);
        this.viewList.add(cardItemView);
        // 6. 判断当前位置，是否应该加载下一组数据了
        int residueCount = this.baseAdapter.getCount() - (nowIndex + 1);
        if (this.cardSwitchListener != null) this.cardSwitchListener.residueCardNumber(residueCount);
    }

    /**
     * 卡片消失回调
     *
     * @param type         1:左 2:右
     * @param nearUserInfo
     */
    private void vanishCallBack(int type, NearUserInfo nearUserInfo, int finalX, int finalY) {
        AbandonCard abandonCard = new AbandonCard(finalX, finalY, nearUserInfo);
        if (type == 1) this.abandonUsers.add(abandonCard);
        this.baseAdapter.removeObject(nearUserInfo);
        if (this.cardSwitchListener != null) this.cardSwitchListener.cardOperate(type, nearUserInfo);
    }

    /**
     * 拖动停止判断，是滑出屏幕，还是回弹
     */
    private void animToSide(CardItemView changedView) {
        int left = changedView.getLeft();
        float abc = (float) Math.abs(left) / Math.abs(this.xDistanceThreshold);
        if (abc >= 1) {     // 滑出屏幕
            int finalX, finalY;
            int dx = changedView.getLeft();
            int childWidth = changedView.getMeasuredWidth();
            int dy = changedView.getTop() - this.bottomMarginTop;
            if (left > 0) { // 右滑
                finalX = childWidth;
                finalY = dy * childWidth / dx + this.bottomMarginTop;
            } else {
                finalX = -childWidth;
                finalY = dy * childWidth / -dx + this.bottomMarginTop;
            }
            NearUserInfo nearUserInfo = changedView.getNearUserInfo();
            this.vanishCallBack(left > 0 ? 2 : 1, nearUserInfo, finalX, finalY);
            this.releasedViewList.add(changedView);
            if (this.mDragHelper.smoothSlideViewTo(changedView, finalX, finalY)) {
                ViewCompat.postInvalidateOnAnimation(this);
                this.mHandler.sendEmptyMessageDelayed(1000, 150);
            }
        } else {    //回到原位
            this.mDragHelper.settleCapturedViewAt(0, this.bottomMarginTop);
            super.invalidate();
        }
    }

    @Override
    public void computeScroll() {
        if (this.mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        } else {
            if (this.mDragHelper.getViewDragState() == ViewDragHelper.STATE_IDLE) {
                this.orderViewStack();
                this.animationLock = false;
            }
        }
    }

    private void processLinkageView(View changedView) {
        int changeViewLeft = changedView.getLeft();
        int changeViewTop = changedView.getTop();
        int distance = Math.abs(changeViewTop - this.bottomMarginTop) + Math.abs(changeViewLeft);
        float rate = distance / (float) MAX_SLIDE_DISTANCE_LINKAGE;
        float rate1 = rate > 1 ? 1f : rate;
        float rate2 = rate - 0.1f;
        rate2 = rate2 < 0 ? 0f : rate2 > 1 ? 1f : rate2;
        this.aJustLinkageViewItem(changedView, rate1, 1);
        this.aJustLinkageViewItem(changedView, rate2, 2);
        CardItemView bottomCardView = this.viewList.get(this.viewList.size() - 1);
        bottomCardView.setAlpha(rate2);
    }

    // 由index对应view变成index-1对应的view
    private void aJustLinkageViewItem(View changedView, float rate, int index) {
        int changeIndex = this.viewList.indexOf(changedView);
        int initPosY = this.yOffsetStep * index;
        float initScale = 1 - this.scaleStep * index;
        int nextPosY = this.yOffsetStep * (index - 1);
        float nextScale = 1 - this.scaleStep * (index - 1);
        int offset = (int) (initPosY + (nextPosY - initPosY) * rate);
        float scale = initScale + (nextScale - initScale) * rate;
        View aJustView = this.viewList.get(changeIndex + index);
        aJustView.offsetTopAndBottom(offset - aJustView.getTop() + this.bottomMarginTop);
        aJustView.setScaleX(scale);
        aJustView.setScaleY(scale);
    }

    /**
     * 卡片回调接口
     */
    public interface CardSwitchListener {

        /**
         * 剩余卡片数量
         * @param index
         */
        void residueCardNumber(int index);

        /**
         * 卡片飞向两侧
         * @param nearUserInfo 卡片
         * @param type  1左 2右
         */
        void cardOperate(int type, NearUserInfo nearUserInfo);
    }
}
