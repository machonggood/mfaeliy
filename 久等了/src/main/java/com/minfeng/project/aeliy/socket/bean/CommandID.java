package com.minfeng.project.aeliy.socket.bean;

import com.minfeng.project.aeliy.entity.UserInfo;

import java.util.List;

public enum CommandID {

    /**
     * Socket已连接，登录成功
     */
    USER_LOGIN_SUCCESS_MESSAGE("userLoginSuccess", UserInfo.class),
    /**
     * 登录失败，账号被禁
     */
    LOGIN_FAIL_ACCOUNT_FORBID_MESSAGE("socketLoginFailForbid"),
    /**
     * 设备在其他地方登陆
     */
    DEVICES_RESTS_LOGIN_MESSAGE("devicesRestsLogin"),
    /**
     * 编辑用户昵称性别生日成功
     */
    EDIT_USER_BASICS_SUCCESS_MESSAGE("editUserBasicsSuccess"),
    /**
     * 编辑用户昵称性别生日失败
     */
    EDIT_USER_BASICS_FAILED_MESSAGE("editUserBasicsFailed", String.class),
    /**
     * 获取附近的人
     */
    FIND_NEAR_BY_USER_MESSAGE("findNearByUser", List.class);

    private String methodName;      //方法名
    private Class[] mParamTypes;    //参数类型

    CommandID(String methodName) {
        this(methodName, null);
    }

    CommandID(String methodName, Class<?>... paramTypes) {
        if (paramTypes != null) this.mParamTypes = paramTypes.clone();
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class[] getmParamTypes() {
        return mParamTypes;
    }

    public void setmParamTypes(Class[] mParamTypes) {
        this.mParamTypes = mParamTypes;
    }
}
