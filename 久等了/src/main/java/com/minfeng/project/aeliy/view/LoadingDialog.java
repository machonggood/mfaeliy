package com.minfeng.project.aeliy.view;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;

public class LoadingDialog extends DialogFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_fragment_style);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_loading, container, false);
		super.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView titleTv = view.findViewById(R.id.dialog_loading_title_tv);
        int strId = getArguments().getInt("strId");
        titleTv.setText(strId);
		return view;
	}
}
