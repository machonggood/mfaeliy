package com.minfeng.project.aeliy.utils;

import java.util.Random;

public class StringUtils {

    public static boolean isEmpty(String str) {
        return isNullOrEmpty(str);
    }

    private static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().compareTo("") == 0 || str.equals("null");
    }

    public static String getStrFormat(String str) {
        return isEmpty(str) ? "" : str;
    }

    public static String getRandomString(int length) {
        //定义一个字符串（A-Z，a-z，0-9）即62位；
        String str = "zxcvbnmlkjhgfdsaqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM";
        //由Random生成随机数
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        //长度为几就循环几次
        for (int i = 0; i < length; ++i) {
            //产生0-61的数字
            int number = random.nextInt(52);
            //将产生的数字通过length次承载到sb中
            sb.append(str.charAt(number));
        }
        //将承载的字符转换成字符串
        return sb.toString();
    }
}
