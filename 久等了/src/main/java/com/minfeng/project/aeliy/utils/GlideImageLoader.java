package com.minfeng.project.aeliy.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.UserManager;
import com.youth.banner.loader.ImageLoaderInterface;

public class GlideImageLoader implements ImageLoaderInterface<ImageView> {

    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        int defaultResId = userInfo == null || userInfo.getUser().getSex() == 0 ? R.mipmap.card_default_woman : R.mipmap.card_default_man;
        RequestOptions options = new RequestOptions().placeholder(defaultResId).centerCrop();
        Glide.with(App.mContext).load(path).apply(options).into(imageView);
    }

    @Override
    public ImageView createImageView(Context context) {
        return new ImageView(context);
    }

}
