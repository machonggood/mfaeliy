package com.minfeng.project.aeliy.socket.bean;

import android.util.Base64;

import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.socket.utils.GZIPUtils;
import com.minfeng.project.aeliy.socket.utils.RSAEncrypt;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.xuhao.didi.core.iocore.interfaces.ISendable;

import java.security.interfaces.RSAPublicKey;

public class RequestBean implements ISendable {

    private String content;

    public RequestBean(String content) {
        this.content = content;
    }

    @Override
    public byte[] parse() {
        byte[] compressed = new byte[0];    // 压缩
        String content = StringUtils.getStrFormat(this.content);
        byte[] contentByte = Base64.encode(content.getBytes(), Base64.DEFAULT);
        try {
            String KeyStr = RSAEncrypt.readAssetsTxt(App.mContext);
            RSAPublicKey publicKey = RSAEncrypt.loadPublicKeyByStr(KeyStr);
            byte[] cipherData = RSAEncrypt.encryptInner(publicKey, contentByte);
            compressed = GZIPUtils.compress(cipherData);
        } catch (Exception e) {
            UIHelper.log(e.getMessage());
        }
        byte[] len = {0, 0, 0, (byte) compressed.length};
        return this.byteMerger(len, compressed);
    }

    private byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
        byte[] byte_3 = new byte[byte_1.length + byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }

    @Override
    public String toString() {
        return "content：" + this.content;
    }
}
