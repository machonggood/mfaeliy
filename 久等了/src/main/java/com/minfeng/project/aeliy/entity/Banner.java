package com.minfeng.project.aeliy.entity;

public class Banner {

    private String name;
    private String imgUrl;

    public Banner() {
    }

    public Banner(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
