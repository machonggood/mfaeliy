package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.FriendCircleComment;
import com.minfeng.project.aeliy.entity.FriendCircleInfo;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.view.CircleImageLayout;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FriendCircleAdapter extends GeneralAdapter<FriendCircleInfo> {

    public FriendCircleAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(App.mContext).inflate(R.layout.friend_circle_item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        FriendCircleInfo friendCircleInfo = (FriendCircleInfo) this.getItem(position);
        ArrayList<String> fileUrls = new ArrayList<>();
        if (!StringUtils.isEmpty(friendCircleInfo.getPicture())) {
            Type typeOfT = new TypeToken<List<String>>() {}.getType();
            List<String> photos = new Gson().fromJson(friendCircleInfo.getPicture(), typeOfT);
            fileUrls.addAll(photos);
        }
        viewHolder.imageLayout.removeAllViews();
        viewHolder.imageLayout.initData(fileUrls, 1, 0);
        int imgChildCount = viewHolder.imageLayout.getChildCount();
//        viewHolder.iconIv.setOnClickListener(new ClickListener(busCircleInfo));
        viewHolder.imageLayout.setVisibility(imgChildCount == 0 ? View.GONE : View.VISIBLE);
        String imgPath = Tools.getPhoto(friendCircleInfo.getPortrait());
        RequestOptions options = new RequestOptions().placeholder(R.mipmap.icon_user_default).circleCrop();
        Glide.with(App.mContext).asBitmap().load(imgPath).apply(options).into(viewHolder.iconIv);
        int msgCount = friendCircleInfo.getComment() != null ? friendCircleInfo.getComment().size() : 0;
        int likeResId = friendCircleInfo.getIf_support() == 0 ? R.mipmap.icon_xinaixin : R.mipmap.icon_xinaixin_over;
        viewHolder.likeCountTv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, likeResId, 0);
        int likeColorId = friendCircleInfo.getIf_support() == 0 ? Color.parseColor("#414141") : Color.parseColor("#f08d2c");
        viewHolder.likeCountTv.setText(String.valueOf(friendCircleInfo.getSupport()));
        viewHolder.sendTimeTv.setText(friendCircleInfo.getCreate_time());
        viewHolder.nickNameTv.setText(friendCircleInfo.getNickname());
        viewHolder.contentTv.setText(friendCircleInfo.getContent());
        viewHolder.msgCountTv.setText(String.valueOf(msgCount));
        viewHolder.likeCountTv.setTextColor(likeColorId);
        viewHolder.msgView.removeAllViews();
        if (friendCircleInfo.getComment() != null) {
            for (FriendCircleComment circleComment : friendCircleInfo.getComment()) {
                TextView textView = (TextView) LayoutInflater.from(App.mContext).inflate(R.layout.friend_comment_item, null);
                textView.setText(Html.fromHtml("<strong><font color='#3b3b3b'>" + circleComment.getNickname() + "：</font></strong>" + circleComment.getContent()));
                viewHolder.msgView.addView(textView);
            }
        }
        return view;
    }

    static class ViewHolder {

        ImageView iconIv;
        ViewGroup msgView;
        CircleImageLayout imageLayout;
        TextView contentTv, sendTimeTv, nickNameTv, msgCountTv, likeCountTv;

        public ViewHolder(View view) {
            this.iconIv = view.findViewById(R.id.friend_circle_item_icon_iv);
            this.msgView = view.findViewById(R.id.friend_circle_item_msg_view);
            this.contentTv = view.findViewById(R.id.friend_circle_item_content_tv);
            this.msgCountTv = view.findViewById(R.id.friend_circle_item_msg_count_tv);
            this.likeCountTv = view.findViewById(R.id.friend_circle_item_msg_like_tv);
            this.imageLayout = view.findViewById(R.id.friend_circle_item_img_layout);
            this.sendTimeTv = view.findViewById(R.id.friend_circle_item_send_time_tv);
            this.nickNameTv = view.findViewById(R.id.friend_circle_item_nick_name_tv);
        }

    }
}
