package com.minfeng.project.aeliy.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 设置昵称和生日
 */
public class setNickBirthdayActivity extends BaseActivity {

    private int sex;
    private EditText nickEt;
    private ImageButton leftView;
    private Button confirmBt, birthdayBt;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_set_nick_birthday);
    }

    @Override
    protected void initViews() {
        this.confirmBt = super.findViewById(R.id.activity_set_nick_birthday_confirm_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.birthdayBt = super.findViewById(R.id.activity_set_nick_birthday_bt);
        this.nickEt = super.findViewById(R.id.activity_set_nick_birthday_et);
        this.sex = super.getIntent().getIntExtra("sex", -1);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        if (this.sex == -1) {
            UIHelper.showToast("请先选择您的性别！");
            super.finish();
        }
    }

    @Override
    protected void initListener() {
        this.birthdayBt.setOnClickListener(new ClickListener());
        this.confirmBt.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_set_nick_birthday_confirm_bt) {
            String birthday = this.birthdayBt.getText().toString();
            String nickName = this.nickEt.getText().toString();
            if (StringUtils.isEmpty(nickName)) {
                UIHelper.showToast("请输入昵称！");
            } else if (StringUtils.isEmpty(birthday)) {
                UIHelper.showToast("请选择您的生日！");
            } else {
//                UserBasics request = new UserBasics();
//                request.setM(3);
//                request.setS(203);
//                request.setSex(this.sex);
//                request.setNickname(nickName);
//                request.setBirthday(birthday);
//                String requestMessage = new Gson().toJson(request);
//                SocketManager.Instance().sendData(requestMessage);
            }
        } else if (v.getId() == R.id.activity_set_nick_birthday_bt) {
            Calendar cal = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    int month = monthOfYear + 1;
                    birthdayBt.setText(year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            Date date = Tools.getDate("1949/10/1", "yyyy/MM/dd");
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            datePickerDialog.getDatePicker().setCalendarViewShown(true);
            datePickerDialog.getDatePicker().setMinDate(date.getTime());
            datePickerDialog.show();
        }
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("editUserBasicsSuccess");          //编辑成功
        methodNames.add("editUserBasicsFailed");           //编辑失败
        return methodNames;
    }

    /**
     * 编辑成功
     */
    public void editUserBasicsSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        super.startActivity(intent);
    }

    /**
     * 编辑失败
     * @param msg
     */
    public void editUserBasicsFailed(final String msg) {
        super.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UIHelper.showToast(StringUtils.isEmpty(msg) ? "保存失败！" : msg);
            }
        });
    }
}
