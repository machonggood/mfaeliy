package com.minfeng.project.aeliy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minfeng.project.aeliy.R;

/**
 * 喜欢我
 */
public class LikeMeFragment extends BaseFragment {

    public static LikeMeFragment newInstance() {
        LikeMeFragment fragment = new LikeMeFragment();
        Bundle data = new Bundle();
        fragment.setArguments(data);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_check, container, false);
    }

    @Override
    protected void initViews(View view) {

    }

    @Override
    protected void loadData() {

    }
}
