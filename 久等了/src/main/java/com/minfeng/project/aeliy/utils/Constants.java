package com.minfeng.project.aeliy.utils;

public class Constants {

    public static final int ERROR = 0X123;

    public static final int SUCCESS = 0X124;
    /**
     * 腾讯云通讯IM_ID
     */
    public static final int SDK_IM_ID = 1400189847;
    /**
     * tag
     */
    public final static String GENERAL_NAME = "mfAeliy";
    /**
     * debug模式
     */
    public final static boolean DEBUG_MODE = true;
    /**
     * 项目状态 1:开发 2:测试 3:产品
     */
    public final static int PROJECT_STATE = 2;
    /**
     * 七牛图片地址
     */
    public final static String QINIU_IMG_URL = "http://static.unknowns.info/";
    /**
     * apk下载目录
     */
    public final static String CACHE_APK_PATH = "/" + GENERAL_NAME + "/apk/";
    /**
     * 图片缓存目录
     */
    public final static String CACHE_IMG_PATH = "/" + GENERAL_NAME + "/img/";
    /**
     * 开发环境
     */
    public final static String[] DEVELOP_KEY_SECRET = {"5b1a027979882f11", "6f128f2c-ac61-408e-8254-e3dc13d0c08c"};
    /**
     * 测试环境
     */
    public final static String[] TEST_KEY_SECRET = {"jri7ZgTE15lsCbZ0", "8484e592-1cbd-470e-b40c-70cf96f3da32"};
    /**
     * 产品环境
     */
    public final static String[] PRODUCT_KEY_SECRET = {"3ydaluXjJB2bEwFj", "bd709e62-1be1-42d2-a1f4-14ef3eba56e6"};
    /**
     * 开发定值
     */
    public final static String DEVELOP_SALE = "t4O0msprn0UeG2KZ";
    /**
     * 测试定值
     */
    public final static String TEST_SALE = "t4O0msprn0UeG2KZ";
    /**
     * 产品定值
     */
    public final static String PRODUCT_SALE = "t4O0msprn0UeG2KZ";
    /**
     * 开发环境URL
     */
    public final static String DEVELOP_URL = "http://192.168.1.128:3000";
    /**
     * 测试环境URL
     */
    public final static String TEST_URL = "http://47.111.163.106:3000";
    /**
     * 使用的URL
     */
    public final static String BASE_URL = Constants.PROJECT_STATE == 1 ? Constants.DEVELOP_URL : Constants.PROJECT_STATE == 2 ? Constants.TEST_URL : null;
    /**
     * 用户
     */
    public final static String USERS_URL = Constants.BASE_URL + "/api/users";
    /**
     * 授权
     */
    public final static String AUTH_URL = Constants.BASE_URL + "/api/auth";


    public static class User {
        public final static String TOKEN_INFO = "TOKEN_INFO";
    }

}
