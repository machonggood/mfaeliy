package com.minfeng.project.aeliy.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.BasePagerAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.fragment.ImageCheckFragment;
import com.minfeng.project.aeliy.utils.Tools;
import com.zhihu.matisse.ui.MatisseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 本地图片查看器
 */
public class NativeImageCheckActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private Button finishBt;
    private ArrayList<Uri> uris;
    private ViewPager viewPager;
    private BasePagerAdapter pagerAdapter;
    private ImageButton leftView, deleteView;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_image_check);
    }

    @Override
    protected void initViews() {
        int position = super.getIntent().getIntExtra("position", 0);
        this.uris = super.getIntent().getParcelableArrayListExtra("Uris");
        this.deleteView = super.findViewById(R.id.activity_image_check_delete_view);
        this.finishBt = super.findViewById(R.id.activity_image_check_finish_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.viewPager = super.findViewById(R.id.activity_image_check_vp);
        this.finishBt.setText((position + 1) + "/" + this.uris.size());
        List<Fragment> tabFragments = this.getViews(this.uris);
        this.pagerAdapter = new BasePagerAdapter(this.getSupportFragmentManager(), tabFragments);
        this.viewPager.setOffscreenPageLimit(tabFragments.size());
        this.viewPager.setAdapter(this.pagerAdapter);
        this.viewPager.setCurrentItem(position);
    }

    @SuppressLint("InflateParams")
    private List<Fragment> getViews(ArrayList<Uri> uris) {
        List<Fragment> views = new ArrayList<>();
        for (Uri uri: uris) {
            String imgPath = Tools.getPathForUri(App.mContext, uri);
            views.add(ImageCheckFragment.newInstance(imgPath, false));
        }
        return views;
    }

    @Override
    protected void initListener() {
        this.deleteView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.finishBt.setOnClickListener(new ClickListener());
        this.viewPager.addOnPageChangeListener(this);
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_image_check_finish_bt) {
            super.finish();
        } else if (v.getId() == R.id.activity_image_check_delete_view) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this.mActivity);
            dialog.setMessage("确定删除该图片吗？");
            dialog.setCancelable(false);
            dialog.setNegativeButton("取消", null);
            dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    uris.remove(viewPager.getCurrentItem());
                    resultPushActivity();
                }
            });
            dialog.show();
        }
    }

    private void resultPushActivity() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(MatisseActivity.EXTRA_RESULT_SELECTION, this.uris);
        super.setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.finishBt.setText((position + 1) + "/" + this.uris.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
