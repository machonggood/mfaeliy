package com.minfeng.project.aeliy.listener;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.minfeng.project.aeliy.mvp.presenter.BasePresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;

public abstract class BaseListener<V extends IBaseView, T extends BasePresenter<V>> extends com.minfeng.project.aeliy.listener.NoDoubleClickListener {

    protected T mPresenter;
    protected AppCompatActivity mActivity;

    public BaseListener(AppCompatActivity appCompatActivity) {
        this.mPresenter = this.createPresenter();
        this.mActivity = appCompatActivity;
        this.mPresenter.attach((V) this);
    }

    public abstract T createPresenter();

    public Activity getBusActivity() {
        return this.mActivity;
    }

    public FragmentManager getMyFragmentManager() {
        return this.mActivity.getFragmentManager();
    }
}
