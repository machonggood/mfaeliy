package com.minfeng.project.aeliy.socket.bean;

import java.io.Serializable;

public class SocketRequest implements Serializable {

    private int m;
    private int s;

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }
}
