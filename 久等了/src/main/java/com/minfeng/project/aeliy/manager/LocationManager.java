package com.minfeng.project.aeliy.manager;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.minfeng.project.aeliy.application.App;

import java.util.ArrayList;
import java.util.List;

public class LocationManager extends BDAbstractLocationListener {

    private int number;
    private LocationClient locationClient;
    private List<LocationBody> locationBodies;
    private volatile static LocationManager instance = null;

    public static LocationManager Instance() {
        if (instance == null) {
            synchronized (LocationManager.class) {
                instance = new LocationManager();
            }
        }
        return instance;
    }

    public LocationManager() {
        this.locationBodies = new ArrayList<>();
        this.startLocation();
        this.number = 1;
    }

    public void setLocationBody(LocationBody locationBody) {
        this.locationBodies.add(locationBody);
    }

    private void startLocation() {
        //定位服务的客户端。宿主程序在客户端声明此类，并调用，目前只支持在主线程中启动
        this.locationClient = new LocationClient(App.mContext);
        //声明LocationClient类实例并配置定位参数
        LocationClientOption locationOption = new LocationClientOption();
        //注册监听函数
        this.locationClient.registerLocationListener(this);
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        locationOption.setLocationMode(LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
        locationOption.setCoorType("gcj02");
        //可选，默认0，即仅定位一次，设置发起连续定位请求的间隔需要大于等于1000ms才是有效的
        locationOption.setScanSpan(5000);
        //可选，设置是否需要地址信息，默认不需要
        locationOption.setIsNeedAddress(true);
        //可选，设置是否需要地址描述
        locationOption.setIsNeedLocationDescribe(true);
        //可选，设置是否需要设备方向结果
        locationOption.setNeedDeviceDirect(false);
        //可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        locationOption.setLocationNotify(true);
        //可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        locationOption.setIgnoreKillProcess(true);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        locationOption.setIsNeedLocationDescribe(true);
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        locationOption.setIsNeedLocationPoiList(true);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        locationOption.SetIgnoreCacheException(false);
        //可选，默认false，设置是否开启Gps定位
        locationOption.setOpenGps(true);
        //可选，默认false，设置定位时是否需要海拔信息，默认不需要，除基础定位版本都可用
        locationOption.setIsNeedAltitude(false);
        //设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者，该模式下开发者无需再关心定位间隔是多少，定位SDK本身发现位置变化就会及时回调给开发者
        locationOption.setOpenAutoNotifyMode();
        //设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者
        locationOption.setOpenAutoNotifyMode(3000, 1, LocationClientOption.LOC_SENSITIVITY_HIGHT);
        //需将配置好的LocationClientOption对象，通过setLocOption方法传递给LocationClient对象使用
        this.locationClient.setLocOption(locationOption);
        //开始定位
        this.locationClient.start();
    }

    public void stopLocation() {
        this.locationClient.stop();
        instance = null;
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        boolean isSuccessLocation = bdLocation != null && bdLocation.getLocType() != BDLocation.TypeServerError;
        for (int i = 0; i < this.locationBodies.size(); i++) {
            LocationBody locationBody = this.locationBodies.get(i);
            if (this.number % locationBody.getNumber() == 0 || isSuccessLocation) {  //满足计数要求
                locationBody.getLocationListener().onLocationChanged(bdLocation);
                if (locationBody.isDestroy()) {
                    this.locationBodies.remove(locationBody);
                    --i;
                }
            }
        }
        this.number++;
    }

    public interface LocationListener {

        void onLocationChanged(BDLocation bdLocation);

    }

    public static class LocationBody {

        private int number; //计数多少
        private boolean isDestroy;  //是否销毁
        private LocationListener locationListener;

        public LocationBody(int number, boolean isDestroy, LocationListener locationListener) {
            this.number = number;
            this.isDestroy = isDestroy;
            this.locationListener = locationListener;
        }

        public int getNumber() {
            return number;
        }

        public boolean isDestroy() {
            return isDestroy;
        }

        public LocationListener getLocationListener() {
            return locationListener;
        }
    }
}
