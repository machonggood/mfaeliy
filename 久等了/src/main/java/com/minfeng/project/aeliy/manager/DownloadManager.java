package com.minfeng.project.aeliy.manager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;

import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.SDCard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadManager extends AsyncTask<String, Integer, File> {

    private Activity mActivity;
    private ProgressDialog dialog;
    private DownloadListener mDownloadListener;

    public DownloadManager(Activity activity, DownloadListener downloadListener) {
        this.mDownloadListener = downloadListener;
        this.mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.dialog = new ProgressDialog(this.mActivity, android.R.style.Theme_Material_Light_Dialog);
        } else {
            this.dialog = new ProgressDialog(this.mActivity);
        }
        int style = ProgressDialog.STYLE_HORIZONTAL;
        this.dialog.setIndeterminate(false);
        this.dialog.setProgressStyle(style);
        this.dialog.setCancelable(false);
        this.dialog.setTitle("版本更新");
        this.dialog.setMax(100);
        this.dialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        this.dialog.setProgress(progress[0]);
        super.onProgressUpdate(progress);
    }

    @Override
    protected File doInBackground(String... params) {
        File baseFile = new File(SDCard.getSDCardPath(), Constants.CACHE_APK_PATH);
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        if (!baseFile.exists()) baseFile.mkdirs();
        File apkFile = new File(baseFile, params[1]);
        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                int fileLength = connection.getContentLength();
                input = connection.getInputStream();
                output = new FileOutputStream(apkFile);
                byte data[] = new byte[1024 * 4];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0) publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        } finally {
            try {
                if (input != null) input.close();
                if (output != null) output.close();
                if (connection != null) connection.disconnect();
            } catch (Exception e) {
                return null;
            }
        }
        return apkFile;
    }

    @Override
    protected void onPostExecute(File file) {
        this.dialog.dismiss();
        super.onPostExecute(file);
        this.mDownloadListener.onPostExecute(file);
    }

    public interface DownloadListener {

        void onPostExecute(File file);

    }

}
