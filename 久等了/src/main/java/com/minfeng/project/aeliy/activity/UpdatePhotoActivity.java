package com.minfeng.project.aeliy.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.FileParam;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.entity.User;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.CloudUploadManager;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.mvp.presenter.SystemPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.GlideEngine;
import com.minfeng.project.aeliy.utils.GlideRoundTransform;
import com.minfeng.project.aeliy.utils.MiniSizeFilter;
import com.minfeng.project.aeliy.utils.SDCard;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.SelectionCreator;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 添加照片
 */
public class UpdatePhotoActivity extends BaseMvpActivity<IBaseView, SystemPresenter> implements IBaseView {

    private Button saveBt;
    private File imageFile;
    private View selectView;
    private ImageButton leftView;
    private List<View> views = new ArrayList<>();

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_update_photo);
    }

    @Override
    public SystemPresenter createPresenter() {
        return new SystemPresenter();
    }

    @Override
    protected void initViews() {
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.saveBt = super.findViewById(R.id.activity_update_photo_save_bt);
        this.views.add(super.findViewById(R.id.activity_update_photo_view1));
        this.views.add(super.findViewById(R.id.activity_update_photo_view2));
        this.views.add(super.findViewById(R.id.activity_update_photo_view3));
        this.views.add(super.findViewById(R.id.activity_update_photo_view4));
        this.views.add(super.findViewById(R.id.activity_update_photo_view5));
        this.views.add(super.findViewById(R.id.activity_update_photo_view6));
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        headerTv.setTextColor(Color.BLACK);
        headerTv.setText("添加照片");
        this.initPhoto();
    }

    private void initPhoto() {
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        if (userInfo != null) {
            User user = userInfo.getUser();
            if (user.getPhoto() != null) {
                for (int i = 0; i < user.getPhoto().length; i++) {
                    String photo = user.getPhoto()[i];
                    View view = this.views.get(i);
                    String photoUrl = Tools.getPhoto(photo);
                    ImageView imageView = view.findViewById(R.id.activity_update_photo_iv);
                    RequestOptions options = new RequestOptions().placeholder(R.mipmap.pic_add).transform(new GlideRoundTransform(this, 5));
                    Glide.with(App.mContext).load(photoUrl).apply(options).into(imageView);
                    view.setTag(photo);
                }
            }
        }
    }

    @Override
    protected void initListener() {
        for (View view: this.views) view.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.saveBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_update_photo_save_bt) {
            List<String> photos = new ArrayList<>();
            for (View view: this.views) {
                Object object = view.getTag();
                if (object != null) photos.add(object.toString());
            }
            if (photos.size() > 0) {

            } else {
                UIHelper.showToast("请选择图片上传!");
            }
        } else if (v.getId() == R.id.activity_update_photo_view1 || v.getId() == R.id.activity_update_photo_view2
                || v.getId() == R.id.activity_update_photo_view3 || v.getId() == R.id.activity_update_photo_view4
                || v.getId() == R.id.activity_update_photo_view5 || v.getId() == R.id.activity_update_photo_view6) {
            this.selectImagePhoto(v);
        }
    }

    /**
     * 图片选择
     */
    private void selectImagePhoto(View view) {
        if (view.getTag() == null) {
            this.selectView = view;
            this.startImageSelectActivity();
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
            dialog.setItems(new String[]{"删除"}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ImageView imageView = view.findViewById(R.id.activity_update_photo_iv);
                    imageView.setImageResource(R.mipmap.pic_add);
                    view.setTag(null);
                }
            });
            dialog.show();
        }
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 200) {

        }
        UIHelper.showToast(result, "请求失败，请重试！");
    }

    /**
     * 开启图片选择器
     */
    private void startImageSelectActivity() {
        Set<MimeType> mimeTypes = new HashSet<>();
        mimeTypes.add(MimeType.JPEG);
        mimeTypes.add(MimeType.PNG);
        SelectionCreator sCreator = Matisse.from(this).choose(mimeTypes);
        if (UIHelper.isGetCameraPermission() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sCreator.capture(true);
            String authority = UIHelper.getPackageName() + ".fileProvider";
            sCreator.captureStrategy(new CaptureStrategy(true, authority));
        }
        int minWidth = 400, minHeight = 600;
        int maxSizeInBytes = 8 * Filter.K * Filter.K;
        sCreator.countable(true)    //有序选择图片
                .maxSelectable(1)   //最大选择数量为1
                .addFilter(new MiniSizeFilter(minWidth, minHeight, maxSizeInBytes, mimeTypes)) // 控制宽高为400*400 以上，大小为8M以下
                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.item_120))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)  //图像选择和预览活动方向
                .thumbnailScale(0.85f)  //缩放比例
                .theme(R.style.Matisse_mfAeliy_Dracula)
                .imageEngine(new GlideEngine()) //加载方式
                .forResult(10010);  //请求码
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10010 && resultCode == RESULT_OK) {
            List<Uri> mSelected = Matisse.obtainResult(data);
            if (mSelected != null && mSelected.size() > 0) {
                this.crop(mSelected.get(0), 2, 3, 400, 600);
            }
        } else if (requestCode == 10011 && resultCode == RESULT_OK) {
            if (this.imageFile != null) {
                RequestOptions options = new RequestOptions().placeholder(R.mipmap.pic_add).transform(new GlideRoundTransform(this, 5));
                ProgressBar progressBar = this.selectView.findViewById(R.id.activity_update_photo_pb);
                ImageView imageView = this.selectView.findViewById(R.id.activity_update_photo_iv);
                FileParam fileParam = new FileParam(this.imageFile.getName(), this.imageFile);
                Glide.with(App.mContext).load(this.imageFile).apply(options).into(imageView);
                TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
                if (tokenInfo != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    new CloudUploadManager() {
                        @Override
                        public void uploadCallback(String key, int index) {
                            progressBar.setVisibility(View.GONE);
                            selectView.setTag(key);
                        }
                    }.uploadImgUri(fileParam, tokenInfo.getQiniu_token());
                }
            }
        }
    }

    private void crop(Uri uri, int aspectX, int aspectY, int outputX, int outputY) {
        File file;
        if (SDCard.SDCardExist()) {
            File imgFile = new File(SDCard.getSDCardPath(), Constants.CACHE_IMG_PATH);
            if (!imgFile.exists()) imgFile.mkdirs();
            file = new File(imgFile, SDCard.getImageName());
        } else {
            file = new File(getCacheDir(), SDCard.getImageName());
        }
        this.imageFile = file;
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        super.startActivityForResult(intent, 10011);
    }
}
