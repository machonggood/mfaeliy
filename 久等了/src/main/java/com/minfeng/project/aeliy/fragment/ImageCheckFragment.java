package com.minfeng.project.aeliy.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.utils.UIHelper;

/**
 * 图片查看器
 */
public class ImageCheckFragment extends BaseFragment implements RequestListener<Bitmap> {

    private String imgPath;
    private boolean isLoaded;   //是否加载过
    private ImageView defaultIv;
    private PhotoView photoView;
    private ProgressBar progressBar;

    public static ImageCheckFragment newInstance(String imgPath, boolean isSourceNetWork) {
        ImageCheckFragment fragment = new ImageCheckFragment();
        Bundle data = new Bundle();
        data.putString("imgPath", imgPath);
        fragment.setArguments(data);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_check, container, false);
    }

    @Override
    protected void initViews(View view) {
        this.imgPath = super.getArguments().getString("imgPath");                       //图片地址（包含本地和网络）
        this.photoView = view.findViewById(R.id.fragment_image_check_iv);
        this.progressBar = view.findViewById(R.id.fragment_image_check_pr);
        this.defaultIv = view.findViewById(R.id.fragment_image_check_loading_iv);
        Glide.with(App.mContext).asBitmap().load(this.imgPath).listener(this).into(this.photoView);
    }

    @Override
    protected void loadData() {
        if (!this.isLoaded) {
            this.isLoaded = true;
        }
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
        this.defaultIv.setImageResource(R.mipmap.default_photo_error);
        UIHelper.showToast("图片加载未成功!");
        this.progressBar.setVisibility(View.GONE);
        return false;
    }

    @Override
    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
        this.progressBar.setVisibility(View.GONE);
        this.defaultIv.setVisibility(View.GONE);
        return false;
    }
}
