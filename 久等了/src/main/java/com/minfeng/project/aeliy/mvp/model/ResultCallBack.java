package com.minfeng.project.aeliy.mvp.model;

import com.minfeng.project.aeliy.entity.Result;

public interface ResultCallBack {

    /**
     * 请求数据回调
     * @param result
     */
    void findDataCallBack(Result result, Object object);

}
