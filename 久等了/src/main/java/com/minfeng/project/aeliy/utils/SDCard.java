package com.minfeng.project.aeliy.utils;

import android.os.Environment;

import java.io.File;
import java.util.UUID;

public class SDCard {

    /**
     * SD卡状态是否正常
     * @return
     */
    public static boolean SDCardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * SD卡根目录
     * @return
     */
    public static String getSDCardPath() {
        String sdPath = null;
        if (SDCardExist())
            sdPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
        return sdPath;
    }

    public static String getImageName() {
        return UUID.randomUUID().toString() + ".jpg";
    }
}
