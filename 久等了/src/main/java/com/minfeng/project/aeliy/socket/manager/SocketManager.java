package com.minfeng.project.aeliy.socket.manager;

import android.util.Base64;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.socket.bean.RequestBean;
import com.minfeng.project.aeliy.socket.bean.TokenRequest;
import com.minfeng.project.aeliy.socket.utils.Constants;
import com.minfeng.project.aeliy.socket.utils.GZIPUtils;
import com.minfeng.project.aeliy.socket.utils.RSAEncrypt;
import com.minfeng.project.aeliy.socket.utils.ResultParseUtils;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.xuhao.didi.core.iocore.interfaces.IPulseSendable;
import com.xuhao.didi.core.iocore.interfaces.ISendable;
import com.xuhao.didi.core.pojo.OriginalData;
import com.xuhao.didi.socket.client.sdk.OkSocket;
import com.xuhao.didi.socket.client.sdk.client.ConnectionInfo;
import com.xuhao.didi.socket.client.sdk.client.action.ISocketActionListener;
import com.xuhao.didi.socket.client.sdk.client.connection.IConnectionManager;

import java.security.interfaces.RSAPublicKey;


public class SocketManager implements ISocketActionListener {

    private IConnectionManager manager;
    private volatile static SocketManager instance = null;

    public static SocketManager Instance() {
        if (instance == null) {
            synchronized (SocketManager.class) {
                if (instance == null) {
                    instance = new SocketManager();
                }
            }
        }
        return instance;
    }

    /**
     * socket连接
     */
    public void connectSocket() {
        if (this.manager != null && this.manager.isConnect()) return;
        String[] hostPorts = Constants.HOST_PORT;
        String host = hostPorts[0];
        int port = Integer.parseInt(hostPorts[1]);
        ConnectionInfo connectionInfo = new ConnectionInfo(host, port);
        this.manager = OkSocket.open(connectionInfo);
        this.manager.registerReceiver(this);
        this.manager.connect();
    }

    /**
     * 断开socket连接
     */
    public void disconnectSocket() {
        if (this.manager != null) {
            this.manager.disconnect();
        }
        this.manager = null;
    }

    /**
     * Socket读写线程启动后回调
     *
     * @param s
     */
    @Override
    public void onSocketIOThreadStart(String s) {
        UIHelper.log("Socket读写线程启动后回调");
    }

    /**
     * Socket读写线程关闭后回调
     *
     * @param s
     * @param e
     */
    @Override
    public void onSocketIOThreadShutdown(String s, Exception e) {
        UIHelper.log("Socket读写线程关闭后回调");
    }

    /**
     * Socket从服务器读取到字节回调
     *
     * @param connectionInfo
     * @param s
     * @param originalData
     */
    @Override
    public void onSocketReadResponse(ConnectionInfo connectionInfo, String s, OriginalData originalData) {
        try {
            byte[] bodyBytes = GZIPUtils.uncompress(originalData.getBodyBytes());
            String KeyStr = RSAEncrypt.readAssetsTxt(App.mContext);
            RSAPublicKey publicKey = RSAEncrypt.loadPublicKeyByStr(KeyStr);
            byte[] decryptBytes = RSAEncrypt.decryptInner(publicKey, bodyBytes);
            decryptBytes = Base64.decode(decryptBytes, Base64.DEFAULT);
            String response = new String(decryptBytes);
            UIHelper.log("Socket从服务器读取到字节回调-->" + response);
            if (!StringUtils.isEmpty(response)) ResultParseUtils.doParse(response);
        } catch (Exception e) {
            UIHelper.log("收到消息，解压解密失败！" + e.getMessage());
        }
    }

    /**
     * Socket写给服务器字节后回调
     *
     * @param connectionInfo
     * @param s
     * @param iSendable
     */
    @Override
    public void onSocketWriteResponse(ConnectionInfo connectionInfo, String s, ISendable iSendable) {
        UIHelper.log("Socket写给服务器字节后回调-->" + iSendable.toString());
    }

    /**
     * 发送心跳后的回调
     *
     * @param connectionInfo
     * @param iPulseSendable
     */
    @Override
    public void onPulseSend(ConnectionInfo connectionInfo, IPulseSendable iPulseSendable) {
        UIHelper.log("发送心跳后的回调");
    }

    /**
     * Socket连接状态由连接->断开回调
     *
     * @param connectionInfo
     * @param s
     * @param e
     */
    @Override
    public void onSocketDisconnection(ConnectionInfo connectionInfo, String s, Exception e) {
        UIHelper.log("Socket连接状态由连接->断开回调");
    }

    /**
     * Socket连接成功回调
     *
     * @param connectionInfo
     * @param s
     */
    @Override
    public void onSocketConnectionSuccess(ConnectionInfo connectionInfo, String s) {
        UIHelper.log("Socket连接成功回调");
//        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
//        if (tokenInfo != null) {
            String token = "08cbbbb5-b372-4b56-afd5-69873c0ade4b";
            TokenRequest tokenRequest = new TokenRequest();
            tokenRequest.setM(1);
            tokenRequest.setS(1);
            tokenRequest.setToken(token);
            String content = new Gson().toJson(tokenRequest);
            this.manager.send(new RequestBean(content));
//        }
    }

    /**
     * Socket连接失败回调  监听这里重启socket连接
     *
     * @param connectionInfo
     * @param s
     * @param e
     */
    @Override
    public void onSocketConnectionFailed(ConnectionInfo connectionInfo, String s, Exception e) {
        UIHelper.log("Socket连接失败回调");
    }

    public void sendData(String content) {
        if (this.manager != null && this.manager.isConnect()) {
            this.manager.send(new RequestBean(content));
        }
    }
}
