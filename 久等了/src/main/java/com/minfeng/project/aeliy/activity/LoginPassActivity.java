package com.minfeng.project.aeliy.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.mvp.presenter.SystemPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.List;

/**
 * 密码登录
 */
public class LoginPassActivity extends BaseMvpActivity<IBaseView, SystemPresenter> implements IBaseView {

    private Button loginBt;
    private ImageButton leftView;
    private EditText phoneEt, passEt;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_login_pass);
    }

    @Override
    public SystemPresenter createPresenter() {
        return new SystemPresenter();
    }

    @Override
    protected void initViews() {
        this.loginBt = super.findViewById(R.id.activity_login_pass_confirm_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.phoneEt = super.findViewById(R.id.activity_login_pass_phone_et);
        this.passEt = super.findViewById(R.id.activity_login_pass_et);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
        this.loginBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_login_pass_confirm_bt) {
            String phone = this.phoneEt.getText().toString();
            String pass = this.passEt.getText().toString();
            if (StringUtils.isEmpty(phone) || phone.length() != 11) {
                UIHelper.showToast("请输入11位的电话号码！");
            } else if (StringUtils.isEmpty(pass) || pass.length() < 6) {
                UIHelper.showToast("请输入6位以上的密码！");
            } else {
                this.mPresenter.loginByPhonePwd(phone, pass);
            }
        }
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 200) {
            if ("loginByPhonePwd".equals(result.getTag())) {
                if (!StringUtils.isEmpty(result.getData())) {
                    TokenInfo tokenInfo = new Gson().fromJson(result.getData(), TokenInfo.class);
                    TokenManager.Instance().setTokenInfo(tokenInfo);
                    SocketManager.Instance().connectSocket();
                    return;
                }
            }
        }
        UIHelper.showToast(result, "请求失败，请重试！");
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("userLoginSuccess");            //Socket已连接，登录成功
        return methodNames;
    }

    /**
     * socket连接成功
     */
    public void userLoginSuccess(UserInfo userInfo) {
        UIHelper.userLoginSuccess(this.mActivity, userInfo);
    }
}
