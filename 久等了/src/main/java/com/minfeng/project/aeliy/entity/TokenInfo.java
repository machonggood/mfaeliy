package com.minfeng.project.aeliy.entity;

public class TokenInfo {

    private boolean new_user;
    private String token;
    private String qiniu_token;
    private String tls_token;
    private int user_id;

    public boolean isNew_user() {
        return new_user;
    }

    public void setNew_user(boolean new_user) {
        this.new_user = new_user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getQiniu_token() {
        return qiniu_token;
    }

    public void setQiniu_token(String qiniu_token) {
        this.qiniu_token = qiniu_token;
    }

    public String getTls_token() {
        return tls_token;
    }

    public void setTls_token(String tls_token) {
        this.tls_token = tls_token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
