package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.listener.NoDoubleClickListener;
import com.minfeng.project.aeliy.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息设置
 */
public class UserEditActivity extends BaseActivity {

    private Button rightBt;
    private ImageButton leftView;
    private ViewGroup qaViewGroup;
    private TextView hyTv, gzlyTv, szcsTv, xhydTv, xhyyTv, xhmsTv, xhysTv, xhsjTv, xhlxTv, tjwtTv;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_user_edit);
    }

    @Override
    protected void initViews() {
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.rightBt = super.findViewById(R.id.header_layout_right_operate_view);
        this.qaViewGroup = super.findViewById(R.id.activity_user_edit_qa_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.gzlyTv = super.findViewById(R.id.activity_user_edit_gzly_tv);
        this.szcsTv = super.findViewById(R.id.activity_user_edit_szcs_tv);
        this.xhydTv = super.findViewById(R.id.activity_user_edit_xhyd_tv);
        this.xhyyTv = super.findViewById(R.id.activity_user_edit_xhyy_tv);
        this.xhmsTv = super.findViewById(R.id.activity_user_edit_xhms_tv);
        this.xhysTv = super.findViewById(R.id.activity_user_edit_xhys_tv);
        this.xhsjTv = super.findViewById(R.id.activity_user_edit_xhsj_tv);
        this.xhlxTv = super.findViewById(R.id.activity_user_edit_xhlx_tv);
        this.tjwtTv = super.findViewById(R.id.activity_user_edit_tjwt_tv);
        this.hyTv = super.findViewById(R.id.activity_user_edit_hy_tv);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        this.rightBt.setTextColor(Color.BLACK);
        headerTv.setTextColor(Color.BLACK);
        this.rightBt.setText("跳过");
        headerTv.setText("个人设置");
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
        this.rightBt.setOnClickListener(new ClickListener());
        this.gzlyTv.setOnClickListener(new ClickListener());
        this.szcsTv.setOnClickListener(new ClickListener());
        this.xhydTv.setOnClickListener(new ClickListener());
        this.xhyyTv.setOnClickListener(new ClickListener());
        this.xhmsTv.setOnClickListener(new ClickListener());
        this.xhysTv.setOnClickListener(new ClickListener());
        this.xhsjTv.setOnClickListener(new ClickListener());
        this.xhlxTv.setOnClickListener(new ClickListener());
        this.tjwtTv.setOnClickListener(new ClickListener());
        this.hyTv.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_user_edit_hy_tv) {           //职业
            this.selectData((TextView) v, "行业", R.array.public_trade);
        } else if (v.getId() == R.id.activity_user_edit_gzly_tv) {  //工作领域
            this.selectData((TextView) v, "工作领域", R.array.public_work);
        } else if (v.getId() == R.id.activity_user_edit_szcs_tv) {  //所在城市
            this.selectData((TextView) v, "所在城市", R.array.public_province);
        } else if (v.getId() == R.id.activity_user_edit_xhyd_tv) {  //喜欢运动
            this.selectMultiChoice((TextView) v, "运动", R.array.public_sports);
        } else if (v.getId() == R.id.activity_user_edit_xhyy_tv) {  //喜欢音乐
            this.selectMultiChoice((TextView) v, "音乐", R.array.public_music);
        } else if (v.getId() == R.id.activity_user_edit_xhms_tv) {  //喜欢美食
            this.selectMultiChoice((TextView) v, "美食", R.array.public_foods);
        } else if (v.getId() == R.id.activity_user_edit_xhys_tv) {  //喜欢影视
            this.selectMultiChoice((TextView) v, "影视", R.array.public_films);
        } else if (v.getId() == R.id.activity_user_edit_xhsj_tv) {  //喜欢书籍
            this.selectMultiChoice((TextView) v, "书籍", R.array.public_books);
        } else if (v.getId() == R.id.activity_user_edit_xhlx_tv) {  //喜欢旅行
            this.selectMultiChoice((TextView) v, "旅行", R.array.public_travels);
        } else if (v.getId() == R.id.activity_user_edit_tjwt_tv) {  //添加问题
            this.addQuestion("选择一个问题", R.array.public_question);
        } else if (v.getId() == R.id.header_layout_right_operate_view) {
            Intent intent = new Intent(this, UpdatePhotoActivity.class);
            super.startActivity(intent);
        }
    }

    /**
     * 添加问题
     * @param title
     * @param itemReIds
     */
    private void addQuestion(String title, int itemReIds) {
        new MaterialDialog.Builder(this)
                .title(title)
                .items(itemReIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence question) {
                        inputAnswer(question);
                    }
                })
                .show();
    }

    /**
     * 回答问题
     * @param question
     */
    private void inputAnswer(CharSequence question) {
        new MaterialDialog.Builder(this)
                .title(question)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence answer) {
                        addQa(question, answer);
                    }
                })
                .negativeText("取消")
                .positiveText("确定")
                .show();
    }

    /**
     * 加入问答
     * @param question
     * @param answer
     */
    private void addQa(CharSequence question, CharSequence answer) {
        View view = LayoutInflater.from(this).inflate(R.layout.user_qa_item, null);
        TextView questionTv = view.findViewById(R.id.user_qa_item_question_tv);
        TextView answerTv = view.findViewById(R.id.user_qa_item_answer_tv);
        this.qaViewGroup.addView(view);
        questionTv.setText(question);
        answerTv.setText(answer);
        view.setOnClickListener(new NoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(View v) {
                editAnswer(question, answer, v);
            }
        });
    }

    /**
     * 修改问题回答
     * @param question
     * @param answer
     * @param view
     */
    private void editAnswer(CharSequence question, CharSequence answer, View view) {
        new MaterialDialog.Builder(this)
                .title(question)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(null, answer, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence answer) {
                        if (answer == null || StringUtils.isEmpty(answer.toString())) {
                            qaViewGroup.removeView(view);
                        } else {
                            TextView answerTv = view.findViewById(R.id.user_qa_item_answer_tv);
                            answerTv.setText(answer);
                        }
                    }
                })
                .negativeText("取消")
                .positiveText("确定")
                .show();
    }

    /**
     * 多选
     * @param textView
     * @param title
     * @param itemReIds
     */
    private void selectMultiChoice(TextView textView, String title, int itemReIds) {
        Integer[] selectedIndices = null;
        String value = textView.getText().toString();
        if (!StringUtils.isEmpty(value)) {
            String[] as = value.split(",");
            List<Integer> integers = new ArrayList<>();
            String[] allData = super.getResources().getStringArray(itemReIds);
            for (int i = 0; i < allData.length; i++) {
                for (String data: as) {
                    if (data.equals(allData[i])) {
                        integers.add(i);
                        break;
                    }
                }
            }
            selectedIndices = new Integer[integers.size()];
            for (int i = 0; i < integers.size(); i++) {
                selectedIndices[i] = integers.get(i);
            }
        }
        new MaterialDialog.Builder(this)
                .title(title)
                .positiveText("确定")
                .negativeText("清空")
                .items(itemReIds)
                .itemsCallbackMultiChoice(selectedIndices, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        if (text != null) {
                             for (int i = 0; i < text.length; i++) {
                                 if (i == 0) {
                                     textView.setText(text[i]);
                                 } else {
                                     textView.append("," + text[i]);
                                 }
                             }
                        }
                        return true;
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        textView.setText(null);
                    }
                })
                .show();
    }

    /**
     * 选择数据
     * @param textView
     * @param itemReIds
     */
    private void selectData(TextView textView, String title, int itemReIds) {
        new MaterialDialog.Builder(this)
                .title(title)
                .positiveText("取消")
                .negativeText("清空")
                .items(itemReIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        textView.setText(text);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        textView.setText(null);
                    }
                })
                .show();
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();

        return methodNames;
    }

}
