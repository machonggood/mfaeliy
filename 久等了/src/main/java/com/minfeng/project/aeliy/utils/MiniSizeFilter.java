package com.minfeng.project.aeliy.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;

import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;

import java.util.HashSet;
import java.util.Set;

public class MiniSizeFilter extends Filter {

    private int mMaxSize;
    private int mMinWidth;
    private int mMinHeight;
    private Set<MimeType> mimeTypes;

    public MiniSizeFilter(int minWidth, int minHeight, int maxSizeInBytes, Set<MimeType> mimeTypes) {
        this.mMinWidth = minWidth;
        this.mimeTypes = mimeTypes;
        this.mMinHeight = minHeight;
        this.mMaxSize = maxSizeInBytes;
    }

    /**
     * 约束的图片类型
     *
     * @return
     */
    @Override
    protected Set<MimeType> constraintTypes() {
        Set<MimeType> mimeTypes = new HashSet<>();
        mimeTypes.add(MimeType.JPEG);
        mimeTypes.add(MimeType.PNG);
        return mimeTypes;
    }

    @Override
    public IncapableCause filter(Context context, Item item) {
        if (!super.needFiltering(context, item)) {
            return null;
        }
        Uri uri = item.getContentUri();
        ContentResolver resolver = context.getContentResolver();
        boolean checkType = false;
        for (MimeType mimeType: this.mimeTypes) {
            checkType = mimeType.checkType(resolver, uri);
            if (checkType) break;
        }
        if (checkType) {
            Point size = PhotoMetadataUtils.getBitmapBound(resolver, uri);
            if (size.x < this.mMinWidth || size.y < this.mMinHeight) {
                return new IncapableCause(IncapableCause.TOAST, "图片尺寸不能小于" + this.mMinWidth + "x" + this.mMinHeight);
            } else if (item.size > this.mMaxSize) {
                return new IncapableCause(IncapableCause.TOAST, "图片过大,请选择小于8M的图片");
            }
        } else {
            return new IncapableCause(IncapableCause.TOAST, "内容格式不合规!");
        }
        return null;
    }
}
