package com.minfeng.project.aeliy.socket.bean;

import java.io.Serializable;

public class SocketResponse implements Serializable {

    protected int m;
    protected int s;
    protected int ret_code;
    protected String reason;

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getRetCode() {
        return ret_code;
    }

    public void setRet_code(int ret_code) {
        this.ret_code = ret_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
