package com.minfeng.project.aeliy.over;

import android.database.DataSetObservable;
import android.database.DataSetObserver;

import com.minfeng.project.aeliy.entity.NearUserInfo;

public abstract class CardAdapter {

    private final DataSetObservable mDataSetObservable = new DataSetObservable();

    /**
     * layout文件ID，调用者必须实现
     */
    public abstract int getLayoutId();

    /**
     * item数量，调用者必须实现
     */
    public abstract int getCount();

    /**
     * View与数据绑定回调，可重载
     */
    public abstract void bindView(CardItemView cardItemView, int index);

    /**
     * 获取数据用
     */
    public abstract Object getItem(int index);

    /**
     * 获取数据的索引
     * @param object
     * @return
     */
    public abstract int getObjectIndex(Object object);

    /**
     * 加入新的数据
     * @param nearUserInfo
     */
    public abstract void addObject(NearUserInfo nearUserInfo);

    /**
     * 移除数据
     * @param nearUserInfo
     */
    public abstract void removeObject(NearUserInfo nearUserInfo);

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservable.registerObserver(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservable.unregisterObserver(observer);
    }

    public void notifyDataSetChanged() {
        this.mDataSetObservable.notifyChanged();
    }
}
