package com.minfeng.project.aeliy.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.mvp.presenter.SystemPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.List;

/**
 * 手机号码绑定
 */
public class BindingPhoneActivity extends BaseMvpActivity<IBaseView, SystemPresenter> implements IBaseView {

    private Button confirmBt;
    private Button sendCodeBt;
    private ImageButton leftView;
    private EditText phoneEt, authCodeEt;
    private CountDownTimer countDownTimer;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        SocketManager.Instance().disconnectSocket();
        super.setContentView(R.layout.activity_binding_phone);
    }

    @Override
    public SystemPresenter createPresenter() {
        return new SystemPresenter();
    }

    @Override
    protected void initViews() {
        this.authCodeEt = super.findViewById(R.id.activity_binding_phone_auth_et);
        this.phoneEt = super.findViewById(R.id.activity_binding_phone_phone_et);
        this.sendCodeBt = super.findViewById(R.id.activity_binding_phone_send_bt);
        this.confirmBt = super.findViewById(R.id.activity_binding_phone_confirm_bt);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
        this.sendCodeBt.setOnClickListener(new ClickListener());
        this.confirmBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_binding_phone_confirm_bt) {
            String authCode = this.authCodeEt.getText().toString();
            String phone = this.phoneEt.getText().toString();
            if (StringUtils.isEmpty(phone) || phone.length() != 11) {
                UIHelper.showToast("请输入11位的电话号码！");
            } else if (StringUtils.isEmpty(authCode)) {
                UIHelper.showToast("请输入验证码！");
            } else {
                this.mPresenter.bindByPhone(phone, authCode);
            }
        } else if (v.getId() == R.id.activity_binding_phone_send_bt) { //发送验证码
            String phone = this.phoneEt.getText().toString();
            if (StringUtils.isEmpty(phone)) {
                UIHelper.showToast("请输入11位的电话号码！");
            } else {
                this.mPresenter.sendBindCode(phone);
            }
        }
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 200) {
            if (result.getTag().equals("sendBindCode")) {
                this.setDownTimer();
                return;
            } else if (result.getTag().equals("bindByPhone")) {
                if (!StringUtils.isEmpty(result.getData())) {
                    SocketManager.Instance().connectSocket();
                }
            }
        }
        UIHelper.showToast(result, "请求失败，请重试！");
    }

    /**
     * 验证码发送成功倒计时
     */
    private void setDownTimer() {
        if (this.countDownTimer != null) {
            this.countDownTimer.cancel();
        }
        this.countDownTimer = new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                sendCodeBt.setText(millisUntilFinished / 1000 + "s后重发");
                sendCodeBt.setEnabled(false);
            }

            @Override
            public void onFinish() {
                sendCodeBt.setText("获取验证码");
                sendCodeBt.setEnabled(true);
            }
        };
        this.countDownTimer.start();
    }

    @Override
    protected void onPause() {
        if (this.countDownTimer != null) this.countDownTimer.cancel();
        this.sendCodeBt.setText("获取验证码");
        this.sendCodeBt.setEnabled(true);
        super.onPause();
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("userLoginSuccess");            //Socket已连接，登录成功
        return methodNames;
    }

    /**
     * socket连接成功
     */
    public void userLoginSuccess(UserInfo userInfo) {
        UIHelper.userLoginSuccess(this.mActivity, userInfo);
    }
}
