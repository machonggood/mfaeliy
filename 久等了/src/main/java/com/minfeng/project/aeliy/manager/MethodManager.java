package com.minfeng.project.aeliy.manager;

import com.minfeng.project.aeliy.socket.bean.CommandID;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MethodManager {

    private Map<String, List<Object>> listMaps;
    private volatile static MethodManager instance = null;

    public static MethodManager Instance() {
        if (instance == null) {
            synchronized (MethodManager.class) {
                if (instance == null) {
                    instance = new MethodManager();
                }
            }
        }
        return instance;
    }

    private MethodManager() {
        this.listMaps = new HashMap<>();
    }

    public void put(String methodName, Object mTarget) {
        List<Object> targetLists = this.listMaps.get(methodName);
        if (targetLists == null) targetLists = new ArrayList<>();
        targetLists.add(mTarget);
        this.listMaps.put(methodName, targetLists);
    }

    public void remove(String methodName, Object mTarget) {
        List<Object> targetLists = this.listMaps.get(methodName);
        if (targetLists != null) {
            targetLists.remove(mTarget);
            if (targetLists.size() == 0) this.listMaps.remove(methodName);
        }
    }

    /**
     * 调用方法
     * @param commandID
     */
    public void invokingMethod(CommandID commandID, Object... paramValues) {
        String methodName = commandID.getMethodName();
        List<Object> targetLists = this.listMaps.get(methodName);
        if (targetLists != null) {
            for (Object object: targetLists) {
                try {
                    Class cls = object.getClass();
                    Method method = cls.getMethod(methodName, commandID.getmParamTypes());
                    method.invoke(object, paramValues);
                } catch (Exception e) {
                    UIHelper.log("反射调用方法失败！" + e.getMessage());
                }
            }
        }
    }

    /**
     * 调用方法
     * @param commandID
     */
    public void invokingMethod(CommandID commandID) {
        this.invokingMethod(commandID, null);
    }
}
