package com.minfeng.project.aeliy.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.view.View;
import android.view.View.OnScrollChangeListener;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.Banner;
import com.minfeng.project.aeliy.utils.GlideImageLoader;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.minfeng.project.aeliy.view.AomaBanner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.M)
public class UserCenterActivity extends BaseActivity implements OnScrollChangeListener {

    private int screenWidth;    //屏幕宽度
    private AomaBanner aomaBanner;
    private ScrollView scrollView;
    private TextView headerTv, tagTv;
    private ImageButton leftView, rightView;
    private View headerView, headerLineView, bannerContentView;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_user_center);
    }

    @Override
    protected void initViews() {
        this.bannerContentView = super.findViewById(R.id.activity_user_center_banner_content_view);
        this.headerLineView = super.findViewById(R.id.activity_user_center_header_line_view);
        this.scrollView = super.findViewById(R.id.activity_user_center_scroll_view);
        this.screenWidth = super.getResources().getDisplayMetrics().widthPixels;
        this.tagTv = super.findViewById(R.id.activity_user_center_tag_tv);
        this.headerView = super.findViewById(R.id.header_content_layout);
        this.aomaBanner = super.findViewById(R.id.activity_user_center_banner_view);
        this.rightView = super.findViewById(R.id.header_layout_right_operate_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.headerLineView.setBackgroundColor(Color.parseColor("#00f3f3f3"));
        this.headerView.setBackgroundColor(Color.parseColor("#00ffffff"));
        this.headerTv.setTextColor(Color.parseColor("#00000000"));
        this.rightView.setImageResource(R.mipmap.icon_white_more);
        this.leftView.setImageResource(R.mipmap.icon_white_back);
        this.setBannerStyle(this.aomaBanner);
        this.headerTv.setText("详情");
        this.addListHeaderView();
        this.initData();
    }

    private void setBannerStyle(AomaBanner aomaBanner) {
        aomaBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        aomaBanner.setIndicatorGravity(BannerConfig.CENTER);
        aomaBanner.setImageLoader(new GlideImageLoader());      //设置图片加载器
        aomaBanner.setBannerAnimation(Transformer.Accordion);   //设置banner动画效果
        aomaBanner.isAutoPlay(true);    //设置自动轮播，默认为true
        aomaBanner.setDelayTime(3000);  //设置轮播时间
        aomaBanner.setIndicatorGravity(BannerConfig.CENTER);    //设置指示器位置（当banner模式中有指示器时）
        aomaBanner.setBanners(new Banner());
        aomaBanner.start();
    }

    private void initData() {
        String tagHtml = "765 <font color='#f08b27'>打赏</font> 334 <font color='#f08b27'>匹配</font> 5.0 <font color='#f08b27'>评分</font> 86% <font color='#f08b27'>概率</font>";
        this.tagTv.setText(Html.fromHtml(tagHtml));
    }

    private void addListHeaderView() {
        int width = this.getResources().getDisplayMetrics().widthPixels;
        UIHelper.setImageLayoutParams(width, 1.2f, this.aomaBanner); //设置banner样式
        UIHelper.setImageLayoutParams(width, 1.2f, this.bannerContentView);
//        UserInfo userInfo = UserManager.Instance().getUserInfo();
//        if (userInfo != null) {
//            if (!StringUtils.isEmpty(userInfo.getPhoto())) {
//                Type typeOfT = new TypeToken<List<String>>() {}.getType();
//                List<String> photos = new Gson().fromJson(userInfo.getPhoto(), typeOfT);
//                this.rollPagerAdapter.onRefresh(photos, userInfo);
//            }
//        }
        List<Banner> banners = new ArrayList<>();
        banners.add(new Banner("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557587724659&di=dbccba03f7de46f1e1040b1ac2498de9&imgtype=0&src=http%3A%2F%2Fimg1.ph.126.net%2ForQJkDaxzIBgKXJWCmGTHA%3D%3D%2F6598293523099543147.jpg"));
        this.aomaBanner.update(banners);
    }

    @Override
    protected void initListener() {
        this.rightView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.scrollView.setOnScrollChangeListener(this);
    }

    @Override
    protected void onDelayClick(View v) {

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        this.conversionHeaderColor(scrollY);
    }

    /**
     * 变换header颜色
     */
    private void conversionHeaderColor(int scrolledY) {
        float pct = scrolledY / (this.screenWidth * 0.75f - UIHelper.getStatusBarHeight());
        pct = pct > 1.0 ? 1.0f : pct < 0.0 ? 0f : pct;
        String alpha = this.getAlpha(pct);
        this.headerLineView.setBackgroundColor(Color.parseColor(String.format("#%sf3f3f3", alpha)));
        this.headerView.setBackgroundColor(Color.parseColor(String.format("#%sffffff", alpha)));
        this.rightView.setColorFilter(Color.parseColor(String.format("#%s000000", alpha)));
        this.leftView.setColorFilter(Color.parseColor(String.format("#%s000000", alpha)));
        this.headerTv.setTextColor(Color.parseColor(String.format("#%s000000", alpha)));
    }

    private String getAlpha(double pct) {
        double i = Math.round(pct * 100) / 100.0d;
        int alpha = (int) Math.round(i * 255);
        String hex = Integer.toHexString(alpha).toUpperCase();
        if (hex.length() == 1) hex = "0" + hex;
        return hex;
    }
}
