package com.minfeng.project.aeliy.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.minfeng.project.aeliy.activity.BaseActivity;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

/**
 * 充值反馈页面
 * Created by machong on 2018/3/13.
 */
public class WXPayEntryActivity extends BaseActivity implements IWXAPIEventHandler {

    private IWXAPI msgApi;
    private Button finishBt;
    private ImageButton leftIb;
    private TextView callbackTv;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
//        super.setContentView(R.layout.activity_recharge_callback);
    }

    private void initWxApi() {
//        String wxAppId = Tools.getBusLiftParam(Constants.User.WXAPP_ID);
//        this.msgApi = WXAPIFactory.createWXAPI(this, wxAppId);
//        this.msgApi.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.msgApi.handleIntent(intent, this);
    }

    public void initViews() {
//        this.finishBt = (Button) super.findViewById(R.id.activity_recharge_callback_finish_bt);
//        this.callbackTv = (TextView) super.findViewById(R.id.activity_recharge_callback_msg_tv);
//        this.leftIb = (ImageButton) super.findViewById(R.id.header_layout_left_operate_ib);
//        TextView headerTv = (TextView) super.findViewById(R.id.header_layout_content_tv);
//        this.leftIb.setImageResource(R.mipmap.back_icon);
//        headerTv.setText("电子卡充值中心");
//        this.initWxApi();
    }

    public void initListener() {
        this.leftIb.setOnClickListener(new ClickListener());
        this.finishBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
//        if (v.getId() == R.id.activity_recharge_callback_finish_bt) {
//            super.finish();
//        }
    }

    @Override
    public void onReq(BaseReq req) { }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
//            if (resp.errCode == 0) {
//                this.callbackTv.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_finish, 0, 0);
//                this.finishBt.setBackgroundResource(R.drawable.button_bg_12c700_style);
//                this.callbackTv.setTextColor(Color.parseColor("#12c700"));
//                this.callbackTv.setText("充值马上到账!");
//            } else if (resp.errCode == -2) {
//                this.callbackTv.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_sorry, 0, 0);
//                this.finishBt.setBackgroundResource(R.drawable.button_bg_8a8a8a_style);
//                this.callbackTv.setTextColor(Color.parseColor("#8a8a8a"));
//                this.callbackTv.setText("很遗憾,充值取消!");
//            } else {
//                this.callbackTv.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_error, 0, 0);
//                this.finishBt.setBackgroundResource(R.drawable.button_bg_d81e06_style);
//                this.callbackTv.setTextColor(Color.parseColor("#d81e06"));
//                this.callbackTv.setText("很遗憾,充值失败!");
//            }
        }
    }

}
