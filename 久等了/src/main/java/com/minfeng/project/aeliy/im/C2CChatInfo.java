package com.minfeng.project.aeliy.im;

import com.tencent.imsdk.TIMConversationType;

/**
 * Created by mc on 2018/8/21.
 */
public class C2CChatInfo extends BaseChatInfo {

    public C2CChatInfo() {
        setType(TIMConversationType.C2C);
    }

}
