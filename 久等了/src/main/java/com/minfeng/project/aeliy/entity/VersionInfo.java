package com.minfeng.project.aeliy.entity;

import java.io.Serializable;

public class VersionInfo implements Serializable {

    /**
     * 是否强制安装 1强制 2非强制
     */
    private int isForce;
    /**
     * 版本号
     */
    private int versionCode;
    /**
     * 更新地址
     */
    private String updateUrl;
    /**
     * 更新日志
     */
    private String updateLog;
    /**
     * app大小
     */
    private double appSize;

    public double getAppSize() {
        return appSize;
    }

    public void setAppSize(double appSize) {
        this.appSize = appSize;
    }

    public int getIsForce() {
        return isForce;
    }

    public void setIsForce(int isForce) {
        this.isForce = isForce;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }
}
