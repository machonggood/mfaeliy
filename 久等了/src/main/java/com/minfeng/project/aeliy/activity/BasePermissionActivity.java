package com.minfeng.project.aeliy.activity;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.mvp.presenter.BasePresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * 权限获取父类
 */
public abstract class BasePermissionActivity<V extends IBaseView, T extends BasePresenter<V>> extends BaseMvpActivity<V, T> {

    private List<String> notGainPermLists;
    private HashMap<String, String> permissionOps;
    private final static int REQUEST_PERMISSION_CODE = 102;

    @Override
    protected void initViews() {
        this.permissionOps = this.getPermissionOps();
        this.notGainPermLists = this.getNotGainPermission();
    }

    /**
     * 将未获取的权限包装到list
     *
     * @return
     */
    private List<String> getNotGainPermission() {
        List<String> notGainPermLists = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && this.permissionOps != null) {
            Iterator<String> permissions = this.permissionOps.keySet().iterator();
            while (permissions.hasNext()) {
                String key = permissions.next();
                String value = this.permissionOps.get(key);
                if (!UIHelper.isGetPermission(key, value)) {
                    notGainPermLists.add(key);
                }
            }
        }
        return notGainPermLists;
    }

    /**
     * 请求读写权限
     */
    protected void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.notGainPermLists.size() > 0) {
                String[] notGainPermissions = {this.notGainPermLists.get(0)};
                ActivityCompat.requestPermissions(this, notGainPermissions, REQUEST_PERMISSION_CODE);
            } else {
                this.onGranted();
            }
        } else {
            this.onGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults != null && grantResults.length > 0
                    && permissions != null && permissions.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {   //权限被禁用
                boolean isPermRat = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);  //判断是否不在提示
                if (isPermRat) {    //true可以再次开启
                    String permName = "该";
                    if (WRITE_EXTERNAL_STORAGE.equals(permissions[0])) {
                        permName = "存储";
                    } else if (ACCESS_FINE_LOCATION.equals(permissions[0])) {
                        permName = "定位";
                    } else if (READ_PHONE_STATE.equals(permissions[0])) {
                        permName = "读取手机状态";
                    } else if (CAMERA.equals(permissions[0])) {
                        permName = "相机";
                    }
                    String permRatTitle = String.format(super.getString(R.string.perm_rat_title), permName);
                    this.startWritePermissionDialog(permRatTitle);
                } else {    //false不在提示
                    this.reStartRequestPermission();
                }
            } else {    //权限被确认开启
                this.reStartRequestPermission();
            }
        }
    }

    /**
     * 打开请求设置权限的对话
     */
    private void startWritePermissionDialog(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setNegativeButton("稍后再说", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                reStartRequestPermission();
            }
        });
        dialog.setPositiveButton("马上开启", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermission();
            }
        });
        dialog.show();
    }

    /**
     * 重新请求权限
     */
    private void reStartRequestPermission() {
        if (this.notGainPermLists.size() > 0) {
            this.notGainPermLists.remove(0);
        }
        this.requestPermission();
    }

    /**
     * 授权成功
     */
    protected abstract void onGranted();
    /**
     * 生成权限,AppOps集合
     * @return
     */
    protected abstract HashMap<String, String> getPermissionOps();
}
