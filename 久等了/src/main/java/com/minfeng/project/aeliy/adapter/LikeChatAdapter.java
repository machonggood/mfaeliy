package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jauker.widget.BadgeView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;

public class LikeChatAdapter extends GeneralAdapter<String> {

    public LikeChatAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(App.mContext).inflate(R.layout.like_chat_item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.badgeView.setText("999");
        return view;
    }

    static class ViewHolder {

        BadgeView badgeView;

        public ViewHolder(View view) {
            this.badgeView = view.findViewById(R.id.like_chat_item_badge_view);
        }
    }
}
