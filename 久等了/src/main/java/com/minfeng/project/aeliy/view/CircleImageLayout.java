package com.minfeng.project.aeliy.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.activity.NetWorkImageCheckActivity;
import com.minfeng.project.aeliy.adapter.FriendCircleImageAdapter1;
import com.minfeng.project.aeliy.adapter.FriendCircleImageAdapter2;
import com.minfeng.project.aeliy.adapter.GeneralAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.listener.NoDoubleClickListener;
import com.minfeng.project.aeliy.listener.NoDoubleItemClickListener;
import com.minfeng.project.aeliy.utils.BlurTransformation;
import com.minfeng.project.aeliy.utils.Tools;

import java.util.ArrayList;

/**
 * 圈子图片
 * Created by machong on 2017/9/5.
 */
public class CircleImageLayout extends LinearLayout {

    public CircleImageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CircleImageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleImageLayout(Context context) {
        super(context);
    }

    /**
     * 初始化数据
     *
     * @param fileUrls        文件地址
     * @param permutationType 排列类型  1:列表 2:详细
     * @param sourceType      文件类型 0:图片 1:视频
     */
    public void initData(final ArrayList<String> fileUrls, int permutationType, int sourceType) {
        if (sourceType == 1) {
            if (permutationType == 1 && fileUrls.size() > 0) {
                LayoutInflater inflater = LayoutInflater.from(App.mContext);
                View view = inflater.inflate(R.layout.friend_circle_video_item, null);
                ImageView contentIv = view.findViewById(R.id.friend_circle_video_item_content_iv);
                int width = App.mContext.getResources().getDisplayMetrics().widthPixels;
                ViewGroup.LayoutParams lp = contentIv.getLayoutParams();
                lp.width = width;
                lp.height = (int) (width * 0.50f);
                contentIv.setLayoutParams(lp);
                contentIv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                this.addView(view);

                String imgPath = Tools.getPhoto(fileUrls.get(0));
                RequestOptions requestOptions = RequestOptions.frameOf(5);
                requestOptions.set(VideoBitmapDecoder.FRAME_OPTION, MediaMetadataRetriever.OPTION_CLOSEST);
                requestOptions.placeholder(R.mipmap.default_rectangle_pic).transform(new BlurTransformation());
                Glide.with(App.mContext).load(imgPath).apply(requestOptions).into(contentIv);
            }
        } else {
            if (fileUrls.size() > 1) {
                LayoutInflater inflater = LayoutInflater.from(App.mContext);
                GridView gridView = (GridView) inflater.inflate(R.layout.circle_image_grid, null);
                GeneralAdapter baseAdapter = permutationType == 1 ? new FriendCircleImageAdapter1(App.mContext) : new FriendCircleImageAdapter2(App.mContext);
                gridView.setAdapter(baseAdapter);
                baseAdapter.onRefresh(fileUrls);
                this.addView(gridView);

                gridView.setOnItemClickListener(new NoDoubleItemClickListener() {
                    @Override
                    public void onNoDoubleItemClick(AdapterView<?> parent, View view, int position, long id) {
                        startImageCheckActivity(fileUrls, position);
                    }
                });
            } else if (fileUrls.size() == 1) {
                ImageView imageView = new ImageView(App.mContext);
                imageView.setImageResource(R.mipmap.default_square_pic);
                this.addView(imageView);

                String imgPath = Tools.getPhoto(fileUrls.get(0));
                RequestOptions options = new RequestOptions().centerCrop();
                Glide.with(App.mContext).load(imgPath).apply(options).into(new GlideSimpleTarget(imageView));

                imageView.setOnClickListener(new NoDoubleClickListener() {
                    @Override
                    public void onNoDoubleClick(View v) {
                        startImageCheckActivity(fileUrls, 0);
                    }
                });
            }
        }
    }

    private void startImageCheckActivity(ArrayList<String> imageUrls, int position) {
        Intent intent = new Intent(App.mContext, NetWorkImageCheckActivity.class);
        intent.putStringArrayListExtra("imageUrls", imageUrls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("position", position);
        App.mContext.startActivity(intent);
    }

    static class GlideSimpleTarget extends SimpleTarget<Drawable> {

        private ImageView imageView;
        private int maxWidth, maxHeight;


        public GlideSimpleTarget(ImageView imageView) {
            int widthPixels = App.mContext.getResources().getDisplayMetrics().widthPixels;
            this.maxHeight = this.maxWidth = (int) (widthPixels * 0.5f);
            this.imageView = imageView;
        }

        @Override
        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
            //获取原图的宽高
            int bitmapWidth = resource.getIntrinsicWidth();
            int bitmapHeight = resource.getIntrinsicHeight();
            float scale = bitmapHeight / (float) bitmapWidth;
            int width, height;
            if (bitmapWidth > bitmapHeight) {
                width = this.maxWidth;
                height = (int) (width * scale);
            } else if (bitmapWidth == bitmapHeight) {
                width = this.maxWidth;
                height = width;
            } else {
                height = this.maxHeight;
                width = (int) (height / scale);
            }
            ViewGroup.LayoutParams params = this.imageView.getLayoutParams();
            params.width = width;
            params.height = height;
            this.imageView.setLayoutParams(params);
            //填充图片
            this.imageView.setImageDrawable(resource);
            //如果是动画则加载
            if (resource.getClass().isAssignableFrom(GifDrawable.class)) {
                GifDrawable gifDrawable = (GifDrawable) resource;
                gifDrawable.start();
            }
        }
    }
}