package com.minfeng.project.aeliy.im;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class ChatAdapter extends IChatAdapter {

    @Override
    public void notifyDataSetChanged(int type, int data) {

    }

    @Override
    public void setDataSource(IChatProvider provider) {

    }

    @Override
    public MessageInfo getItem(int position) {
        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public void showLoading() {

    }
}
