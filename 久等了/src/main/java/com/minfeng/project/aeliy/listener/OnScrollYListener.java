package com.minfeng.project.aeliy.listener;

import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.Map;

public class OnScrollYListener implements AbsListView.OnScrollListener {

    private ListView mListView;
    private OnListScrollListener onListScrollListener;
    private Map<Integer, Integer> mItemHeights = new HashMap<>();

    public OnScrollYListener(ListView listView, OnListScrollListener onListScrollListener) {
        this.mListView = listView;
        this.onListScrollListener = onListScrollListener;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.mListView == null || this.mListView.getAdapter() == null) {
            return;
        }
        boolean enable = true;
        if (view != null && view.getChildCount() > 0) {
            boolean firstItemVisible = view.getFirstVisiblePosition() == 0;
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() == 0;
            enable = firstItemVisible && topOfFirstItemVisible;
        }
        //listView当前在屏幕可见的第一个item，也就是firstVisibleItem索引对应的item
        View firstVisibleChild = this.mListView.getChildAt(0);
        if (firstVisibleChild == null) {
            return;
        }
        if (!this.mItemHeights.containsKey(firstVisibleItem)) {
            //将每个item以 key:index, value:height 存入map
            this.mItemHeights.put(firstVisibleItem, firstVisibleChild.getHeight());
        }
        int scrollY = computeScrollY(firstVisibleItem, firstVisibleChild);
        this.onListScrollListener.onScrollY(scrollY, enable);
    }

    private int computeScrollY(int firstVisibleItem, View firstVisibleChild) {
        int scrollY = 0;
        int sum = 0, count = 0;
        for (int i = 0; i <= firstVisibleItem; i++) {
            Integer h = this.mItemHeights.get(i);
            //当快速滑动listView时，firstVisibleItem不是连续增长，ex 0,1,3,7....
            if (h == null) {
                continue;
            }
            sum += h;
            count++;
        }
        if (count == 0) {
            return 0;
        }
        //已经记录的item的高度的平均值
        int avarage = sum / count;
        //已记录的item高度总和+没有记录的item的高度总和
        scrollY = sum + avarage * (firstVisibleItem + 1 - count);
        //第一个item可能这时候还有一半在屏幕内，这时候要减去这个item在屏幕内部分的高度
        scrollY -= firstVisibleChild.getBottom();
        return scrollY;
    }

    public interface OnListScrollListener {
        void onScrollY(int scrolledY, boolean enable);
    }

}

