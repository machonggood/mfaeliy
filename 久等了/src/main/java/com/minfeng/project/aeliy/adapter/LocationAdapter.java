package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baidu.mapapi.search.core.PoiInfo;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;

public class LocationAdapter extends GeneralAdapter<PoiInfo> {

    public LocationAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(App.mContext).inflate(R.layout.location_item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        PoiInfo poiInfo = (PoiInfo) this.getItem(position);
        if (poiInfo != null) {
            viewHolder.nameTv.setText(poiInfo.getName());
            viewHolder.addressTv.setVisibility(View.VISIBLE);
            viewHolder.addressTv.setText(poiInfo.getAddress());
            viewHolder.nameTv.setTextColor(Color.parseColor("#333333"));
        } else {
            viewHolder.nameTv.setText("不显示位置");
            viewHolder.addressTv.setVisibility(View.GONE);
            viewHolder.nameTv.setTextColor(Color.parseColor("#f08b27"));
        }
        return view;
    }

    static class ViewHolder {

        TextView nameTv, addressTv;

        public ViewHolder(View view) {
            this.addressTv = view.findViewById(R.id.location_item_address_tv);
            this.nameTv = view.findViewById(R.id.location_item_name_tv);
        }

    }
}
