package com.minfeng.project.aeliy.wxapi;

import android.os.Bundle;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.umeng.socialize.weixin.view.WXCallbackActivity;

/**
 * 微信回调
 * @author 马冲
 * @version 1.0
 */
public class WXEntryActivity extends WXCallbackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() != ConstantsAPI.COMMAND_PAY_BY_WX) {
            super.onResp(resp);
        }
    }
    
}
