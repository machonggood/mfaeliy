package com.minfeng.project.aeliy.listener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.Calendar;

public abstract class NoDoubleItemClickListener implements OnItemClickListener {

    private long lastClickTime = 0;
    public static final int MIN_CLICK_DELAY_TIME = 1000;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - this.lastClickTime > MIN_CLICK_DELAY_TIME) {
            this.lastClickTime = currentTime;
            this.onNoDoubleItemClick(parent, view, position, id);
        }
    }

    public abstract void onNoDoubleItemClick(AdapterView<?> parent, View view, int position, long id);

}
