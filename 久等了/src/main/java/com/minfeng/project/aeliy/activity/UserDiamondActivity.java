package com.minfeng.project.aeliy.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.BasePagerAdapter;
import com.minfeng.project.aeliy.fragment.DiamondFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户钻石
 */
public class UserDiamondActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton leftView;
    private BasePagerAdapter pagerAdapter;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_user_diamond);
    }

    @Override
    protected void initViews() {
        this.tabLayout = super.findViewById(R.id.activity_user_diamond_tab_layout);
        this.viewPager = super.findViewById(R.id.activity_user_diamond_pager_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        headerTv.setTextColor(Color.BLACK);
        headerTv.setText("我的钻石");

        List<Fragment> tabFragments = this.getViews();
        this.pagerAdapter = new BasePagerAdapter(super.getSupportFragmentManager(), tabFragments);
        this.viewPager.setOffscreenPageLimit(tabFragments.size());
        this.tabLayout.setupWithViewPager(this.viewPager);
        this.viewPager.setAdapter(this.pagerAdapter);
        this.viewPager.setCurrentItem(0);
        this.setTabLayout();
    }

    @SuppressLint("InflateParams")
    private List<Fragment> getViews() {
        List<Fragment> views = new ArrayList<>();
        views.add(DiamondFragment.newInstance(0));
        views.add(DiamondFragment.newInstance(1));
        views.add(DiamondFragment.newInstance(2));
        return views;
    }

    private void setTabLayout() {
        String[] items = {"全部", "充值", "消费"};
        for (int i = 0; i < items.length; i++) {
            View view = this.buildView(items[i]);
            TabLayout.Tab tab = this.tabLayout.getTabAt(i);
            tab.setCustomView(view);
        }
    }

    private View buildView(String tabTxt) {
        TextView tabTextView = (TextView) LayoutInflater.from(this).inflate(R.layout.view_pager_indicator, null);
        tabTextView.setText(tabTxt);
        return tabTextView;
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {

    }
}
