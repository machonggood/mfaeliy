package com.minfeng.project.aeliy.entity;

import com.minfeng.project.aeliy.socket.bean.SocketResponse;

public class UserInfo extends SocketResponse {

    private User user;  //用户基本信息
    private UserAssets userAssets;  //用户会员信息

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserAssets getUserAssets() {
        return userAssets;
    }

    public void setUserAssets(UserAssets userAssets) {
        this.userAssets = userAssets;
    }
}
