package com.minfeng.project.aeliy.view;

import android.app.DialogFragment;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.minfeng.project.aeliy.R;

public class EffectDialog extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_fragment_style);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.effect_loading, container, false);
        ImageView imageView = view.findViewById(R.id.effect_loading_iv);
        int effectId = getArguments().getInt("effectId");
        imageView.setImageResource(effectId);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
        this.animationDismiss(animationDrawable);
        animationDrawable.start();
        return view;
    }

    private void animationDismiss(AnimationDrawable animationDrawable) {
        int duration = 0;
        for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
            duration += animationDrawable.getDuration(i);
        }
        new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1000) dismiss();
                super.handleMessage(msg);
            }
        }.sendEmptyMessageDelayed(1000, duration);
    }
}
