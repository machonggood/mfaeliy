package com.minfeng.project.aeliy.listener;

import android.view.View;
import android.view.View.OnClickListener;

import java.util.Calendar;

public abstract class NoDoubleClickListener implements OnClickListener {

    private long lastClickTime = 0;
    public static final int MIN_CLICK_DELAY_TIME = 1000;

    @Override
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - this.lastClickTime > MIN_CLICK_DELAY_TIME) {
            this.lastClickTime = currentTime;
            this.onNoDoubleClick(v);
        }
    }

    public abstract void onNoDoubleClick(View v);

}
