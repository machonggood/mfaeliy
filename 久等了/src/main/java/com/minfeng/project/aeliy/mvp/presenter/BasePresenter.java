package com.minfeng.project.aeliy.mvp.presenter;

import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.mvp.model.ResultCallBack;
import com.minfeng.project.aeliy.mvp.view.IBaseView;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<T extends IBaseView> implements ResultCallBack {

    protected WeakReference<T> wef;

    /**
     * 解绑
     */
    public void detach() {
        if (this.wef != null) this.wef.clear();
    }

    /**
     * 绑定
     * @param t
     */
    public void attach(T t) {
        this.wef = new WeakReference<T>(t);
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (this.wef.get() != null) this.wef.get().findDataCallBack(result, object);
    }
}
