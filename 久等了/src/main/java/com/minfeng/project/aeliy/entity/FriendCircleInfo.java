package com.minfeng.project.aeliy.entity;

import java.util.List;

public class FriendCircleInfo {

    private long msg_id;
    private int uid;
    private String nickname;
    private String portrait;
    private String content;
    private String picture;
    private String location;
    private String create_time;
    private int support;
    private int if_support;     //0 | 1
    private List<FriendCircleComment> comment;

    public long getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(long msg_id) {
        this.msg_id = msg_id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    public int getIf_support() {
        return if_support;
    }

    public void setIf_support(int if_support) {
        this.if_support = if_support;
    }

    public List<FriendCircleComment> getComment() {
        return comment;
    }

    public void setComment(List<FriendCircleComment> comment) {
        this.comment = comment;
    }
}
