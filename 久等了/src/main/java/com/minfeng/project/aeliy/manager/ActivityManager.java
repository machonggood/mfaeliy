package com.minfeng.project.aeliy.manager;

import android.app.Activity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Activity管理器
 */
public class ActivityManager {

    public Map<String, LinkedList<Activity>> activityMaps;
    private volatile static ActivityManager instance = null;

    public static ActivityManager Instance() {
        if (instance == null) {
            synchronized (ActivityManager.class) {
                if (instance == null) {
                    instance = new ActivityManager();
                }
            }
        }
        return instance;
    }

    public ActivityManager() {
        this.activityMaps = new HashMap<>();
    }

    /**
     * 关闭所有的Activity
     */
    public void finishAll() {
        for (Map.Entry entry: this.activityMaps.entrySet()) {
            LinkedList<Activity> activities = (LinkedList<Activity>) entry.getValue();
            for (Activity activity : activities) {
                if (activity!= null && !activity.isFinishing()) {
                    activity.finish();
                }
            }
        }
    }

    /**
     * 关闭所有Activity，保留目标Activity
     * @param activityClass
     */
    public void exitAllActivityRetainTargetActivity(Class<?> activityClass){
        for (Map.Entry entry: this.activityMaps.entrySet()) {
            LinkedList<Activity> activities = (LinkedList<Activity>) entry.getValue();
            for (Activity activity : activities) {
                if (activity!= null && !activity.isFinishing() && !activity.getClass().equals(activityClass)) {
                    activity.finish();
                }
            }
        }
    }

    /**
     * 关闭指定组的Activity
     * @param target
     */
    public void exitAssignGroupActivity(String target) {
        LinkedList<Activity> activities = this.activityMaps.get(target);
        for (Activity activity : activities) {
            if (activity!= null && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    /**
     * 添加Activity到指定组
     * @param target
     * @param activity
     */
    public void addAssignGroupActivity(String target, Activity activity) {
        LinkedList<Activity> activities = this.activityMaps.get(target);
        if (activities == null) {
            activities = new LinkedList<>();
            this.activityMaps.put(target, activities);
        }
        activities.add(activity);
    }
}
