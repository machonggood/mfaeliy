package com.minfeng.project.aeliy.manager;

import com.minfeng.project.aeliy.entity.FileParam;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

import java.util.List;

/**
 * 七牛云存储管理器
 */
public abstract class CloudUploadManager {

    public void uploadImgUri(List<FileParam> fileParams, String qiNiuToken) {
        Configuration configuration = new Configuration.Builder().build();
        UploadManager uploadManager = new UploadManager(configuration);
        for (int i = 0; i < fileParams.size(); i++) {
            final int index = i;
            final FileParam fileParam = fileParams.get(i);
            uploadManager.put(fileParam.getFile(), fileParam.getName(), qiNiuToken, new UpCompletionHandler() {
                @Override
                public void complete(String key, ResponseInfo info, JSONObject response) {
                    if (info.isOK()) {
                        uploadCallback(key, index);
                    } else {
                        uploadCallback(null, index);
                    }
                }
            }, null);
        }
    }

    public void uploadImgUri(FileParam fileParam, String qiNiuToken) {
        Configuration configuration = new Configuration.Builder().build();
        UploadManager uploadManager = new UploadManager(configuration);
        uploadManager.put(fileParam.getFile(), fileParam.getName(), qiNiuToken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {
                if (info.isOK()) {
                    uploadCallback(key, 0);
                } else {
                    uploadCallback(null, 0);
                }
            }
        }, null);
    }

    public abstract void uploadCallback(String key, int index);

}
