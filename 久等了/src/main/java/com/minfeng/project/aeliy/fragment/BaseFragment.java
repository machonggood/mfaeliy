package com.minfeng.project.aeliy.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.listener.NoDoubleClickListener;
import com.minfeng.project.aeliy.listener.NoDoubleItemClickListener;
import com.minfeng.project.aeliy.utils.UIHelper;

/**
 * Created by 马冲 on 2017/9/19.
 */
public abstract class BaseFragment extends Fragment {

    protected FragmentActivity mActivity;

    private boolean isVisible = false;

    protected void loadData() {}

    protected void fragmentHide() {}

    protected abstract void initViews(View view);

    protected void initListener() {}

    protected void onDelayClick(View v) {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.initActivity((FragmentActivity) context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.initActivity((FragmentActivity) activity);
    }

    protected void initActivity(FragmentActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisible = isVisibleToUser;
        this.visibleOperate();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initViews(super.getView());
        this.visibleOperate();
    }

    private void visibleOperate() {
        if (super.getView() != null) {
            if (this.isVisible) {
                this.loadData();
            } else {
                this.fragmentHide();
            }
        }
    }

    /**
     * 设置沉浸式状态栏样式
     */
    protected void setBarStatus(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ViewGroup viewGroup = view.findViewById(R.id.header_content_layout);
            View statusView = UIHelper.createStatusView(Color.TRANSPARENT);
            viewGroup.addView(statusView, 0);
        }
    }

    public class ClickListener extends NoDoubleClickListener {
        @Override
        public void onNoDoubleClick(View v) {
            onDelayClick(v);
        }
    }

    public class ClickItemListener extends NoDoubleItemClickListener {
        @Override
        public void onNoDoubleItemClick(AdapterView<?> parent, View view, int position, long id) {
            onDelayItemClick(parent, view, position, id);
        }
    }

    /**
     * listView单项点击事件
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    public void onDelayItemClick(AdapterView<?> parent, View view, int position, long id) {
        UIHelper.log("listView item click");
    }

}
