package com.minfeng.project.aeliy.socket.bean;

/**
 * 用户动作
 */
public class UserAction extends SocketRequest {

    private int type;       //1喜欢 2不喜欢 3关注 4收藏 5打赏 6解除匹配
    private long to_id;     //对象用户ID

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTo_id() {
        return to_id;
    }

    public void setTo_id(long to_id) {
        this.to_id = to_id;
    }

}
