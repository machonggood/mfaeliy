package com.minfeng.project.aeliy.socket.bean;

public class PushRelation extends SocketRequest {

    private int platform;   //1安卓 2苹果
    private String registerId;
    private int vender = 5;

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public int getVender() {
        return vender;
    }

    public void setVender(int vender) {
        this.vender = vender;
    }
}
