package com.minfeng.project.aeliy.im;

import com.minfeng.project.aeliy.utils.Tools;

/**
 * Created by mc on 2018/10/11.
 */
public abstract class DynamicChatUserIconView extends DynamicLayoutView<MessageInfo> {

    private int iconRadius = -1;

    public int getIconRadius() {
        return iconRadius;
    }

    /**
     * 设置聊天头像圆角
     *
     * @param iconRadius
     */
    public void setIconRadius(int iconRadius) {
        this.iconRadius = Tools.getPxByDp(iconRadius);
    }
}
