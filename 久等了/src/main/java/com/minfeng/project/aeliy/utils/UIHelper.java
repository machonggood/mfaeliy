package com.minfeng.project.aeliy.utils;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.google.gson.Gson;
import com.minfeng.project.aeliy.BuildConfig;
import com.minfeng.project.aeliy.activity.MainActivity;
import com.minfeng.project.aeliy.activity.setSexActivity;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.socket.bean.LocationReport;
import com.minfeng.project.aeliy.socket.bean.PushRelation;
import com.minfeng.project.aeliy.socket.bean.ReportDeviceInfo;
import com.minfeng.project.aeliy.socket.manager.SocketManager;

import java.lang.reflect.Method;

import cn.jpush.android.api.JPushInterface;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class UIHelper {

    private static long lastTime = 0;

    public static void showToast(Result result, String customMsg) {
        String resultMsg = result != null ? result.getMsg() : StringUtils.getStrFormat(null);
        UIHelper.showToast(!StringUtils.isEmpty(resultMsg) ? resultMsg : customMsg);
    }

    public static void showToast(Result result, int customResId) {
        String resultMsg = result != null ? result.getMsg() : StringUtils.getStrFormat(null);
        if (!StringUtils.isEmpty(resultMsg)) {
            UIHelper.showToast(resultMsg);
        } else {
            UIHelper.showToast(customResId);
        }
    }

    /**
     * 显示消息提示
     * @param paramString
     */
    public static void showToast(String paramString) {
        if (System.currentTimeMillis() - lastTime > 2000) {
            Toast.makeText(App.mContext, paramString, Toast.LENGTH_SHORT).show();
            lastTime = System.currentTimeMillis();
        }
    }

    /**
     * 显示消息提示
     * @param paramResId
     */
    public static void showToast(int paramResId) {
        if (System.currentTimeMillis() - lastTime > 2000) {
            Toast.makeText(App.mContext, paramResId, Toast.LENGTH_SHORT).show();
            lastTime = System.currentTimeMillis();
        }
    }

    public static void log(String msg) {
        if (Constants.DEBUG_MODE) {
            Log.i(Constants.GENERAL_NAME, msg);
        }
    }

    /**
     * 生成一个和状态栏大小相同的矩形条
     * @param color 状态栏颜色值
     * @return 状态栏矩形条
     */
    public static View createStatusView(int color) {
        int statusBarHeight = UIHelper.getStatusBarHeight();
        return UIHelper.createStatusView(statusBarHeight, color);
    }

    /**
     * 获取状态栏高度
     * @return
     */
    public static int getStatusBarHeight() {
        Resources resources = App.mContext.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    /**
     * 获取虚拟按键的高度
     * @return
     */
    public static int getNavigationBarHeight() {
        int result = 0;
        if (UIHelper.hasNavBar(App.mContext)) {
            Resources res = App.mContext.getResources();
            int resourceId = res.getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = res.getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    /**
     * 检查是否存在虚拟按键栏
     * @param context
     * @return
     */
    private static boolean hasNavBar(Context context) {
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("config_showNavigationBar", "bool", "android");
        if (resourceId != 0) {
            boolean hasNav = res.getBoolean(resourceId);
            String sNavBarOverride = UIHelper.getNavBarOverride();
            if ("1".equals(sNavBarOverride)) {
                hasNav = false;
            } else if ("0".equals(sNavBarOverride)) {
                hasNav = true;
            }
            return hasNav;
        } else {
            return !ViewConfiguration.get(context).hasPermanentMenuKey();
        }
    }

    /**
     * 判断虚拟按键栏是否重写
     * @return
     */
    private static String getNavBarOverride() {
        String sNavBarOverride = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                Class c = Class.forName("android.os.SystemProperties");
                Method m = c.getDeclaredMethod("get", String.class);
                m.setAccessible(true);
                sNavBarOverride = (String) m.invoke(null, "qemu.hw.mainkeys");
            } catch (Throwable e) {
            }
        }
        return sNavBarOverride;
    }

    /**
     * 生成一个和状态栏大小相同的矩形条
     * @param statusBarHeight
     * @param color
     * @return
     */
    public static View createStatusView(int statusBarHeight, int color) {
        View statusView = new View(App.mContext);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, statusBarHeight);
        statusView.setLayoutParams(params);
        statusView.setBackgroundColor(color);
        return statusView;
    }

    /**
     * 获取包名
     * @return
     */
    public static String getPackageName() {
        return BuildConfig.APPLICATION_ID;
    }

    /**
     * 是否获取权限
     * @param permission
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isGetPermission(String permission, String op) {
        int checkSelf = ContextCompat.checkSelfPermission(App.mContext, permission);
        if (!UIHelper.getAppOps(App.mContext, op) || checkSelf != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    /**
     * 是否获取读取手机状态权限
     * @return
     */
    public static boolean isGetReadPhonePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return UIHelper.isGetPermission(READ_PHONE_STATE, AppOpsManager.OPSTR_READ_PHONE_STATE);
        }
        return true;
    }

    /**
     * 是否获取读写权限
     * @return
     */
    public static boolean isGetWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return UIHelper.isGetPermission(WRITE_EXTERNAL_STORAGE, AppOpsManager.OPSTR_WRITE_EXTERNAL_STORAGE);
        }
        return true;
    }

    /**
     * 是否获取相机权限
     * @return
     */
    public static boolean isGetCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return UIHelper.isGetPermission(CAMERA, AppOpsManager.OPSTR_CAMERA);
        }
        return true;
    }

    /**
     * 是否获取定位权限
     * @return
     */
    public static boolean isGetLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return UIHelper.isGetPermission(ACCESS_FINE_LOCATION, AppOpsManager.OPSTR_FINE_LOCATION);
        }
        return true;
    }

    /**
     * 判断定位服务是否开启
     * @return true开启
     */
    public static boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(App.mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(App.mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * 双重保险, 判断权限
     * @param context
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean getAppOps(Context context, String op) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int checkOp = appOpsManager.checkOp(op, Binder.getCallingUid(), context.getPackageName());
            if (checkOp == AppOpsManager.MODE_IGNORED) {    //MODE_ALLOWED有权限 MODE_IGNORED被禁止 MODE_ERRORED出错了
                return false;
            }
        }
        return true;
    }

    /**
     * 启动应用的设置
     */
    public static void startAppSettings(Context context) {
        Uri packageURI = Uri.parse("package:" + context.getPackageName());
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
        context.startActivity(intent);
    }

    /**
     * 按宽高比显示view
     *
     * @param width
     * @param scale
     * @param view
     */
    public static void setImageLayoutParams(int width, float scale, View view) {
        int height = (int) (width * scale);
        LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    /**
     * 用户登录成功
     * @param userInfo
     */
    public static void userLoginSuccess(Activity activity, UserInfo userInfo) {
        UIHelper.reportDeviceInfo();
        UIHelper.pushRelationBinding();
        UIHelper.getUserInfoResponse(activity, userInfo);
    }

    /**
     * 获取用户信息后的反馈
     * @param activity
     * @param userInfo
     */
    public static void getUserInfoResponse(final Activity activity, final UserInfo userInfo) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (userInfo != null) {
                    Class cls = userInfo.getUser().getSex() != 2 ? MainActivity.class : setSexActivity.class;
                    UserManager.Instance().setUserInfo(userInfo);
                    Intent intent = new Intent(activity, cls);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        });
    }

    /**
     * 上报设备信息
     */
    public static void reportDeviceInfo() {
        ReportDeviceInfo request = new ReportDeviceInfo();
        request.setM(3);
        request.setS(1);
        request.setType(1);
        request.setModel(android.os.Build.MODEL);
        request.setBrand(android.os.Build.MANUFACTURER);
        String requestMessage = new Gson().toJson(request);
        SocketManager.Instance().sendData(requestMessage);
    }

    /**
     * 推送关系绑定
     */
    public static void pushRelationBinding() {
        String registerId = JPushInterface.getRegistrationID(App.mContext);
        PushRelation request = new PushRelation();
        request.setM(3);
        request.setS(2);
        request.setPlatform(1);
        request.setRegisterId(registerId);
        String requestMessage = new Gson().toJson(request);
        SocketManager.Instance().sendData(requestMessage);
    }

    /**
     * 上报经纬度
     * @param bdLocation
     */
    public static void sendLocationReport(BDLocation bdLocation) {
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        if (userInfo != null) {
            LocationReport request = new LocationReport();
            request.setM(3);
            request.setS(3);
            request.setLatitude(String.valueOf(bdLocation.getLatitude()));
            request.setLongitude(String.valueOf(bdLocation.getLongitude()));
            String requestMessage = new Gson().toJson(request);
            SocketManager.Instance().sendData(requestMessage);
        }
    }
}
