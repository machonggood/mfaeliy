package com.minfeng.project.aeliy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.im.TUIKit;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by machong on 16/9/18.
 */
public class Tools {

    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewHtml(String htmlText) {
        try {
            Document doc = Jsoup.parse(htmlText);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }
            return doc.toString();
        } catch (Exception e) {
            return htmlText;
        }
    }

    public static long getDateLong(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr)) {
            return 0;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            Date date = dateFormat.parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static Date getDate(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String getDateStr(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date());
    }

    public static String getDateStr(String format, long ms) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        return dateFormat.format(ms);
    }

    public static String getDateStr(String format, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static String getReplaceStr(String resouceStr, String oldStr, String newStr) {
        int index = resouceStr.lastIndexOf(oldStr);
        if (index == -1) {
            return resouceStr;
        } else {
            return resouceStr.replace(oldStr, newStr);
        }
    }

    /**
     * 获取SP
     *
     * @return
     */
    public static SharedPreferences getDataSp() {
        SharedPreferences sp = App.mContext.getSharedPreferences(Constants.GENERAL_NAME, Context.MODE_MULTI_PROCESS);
        return sp;
    }

    /**
     * 单数据修改保存
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean updateDataSp(String key, String value) {
        SharedPreferences.Editor editor = Tools.getDataSp().edit();
        return editor.putString(key, value).commit();
    }

    /**
     * 多数据修改保存
     *
     * @param params
     * @return
     */
    public static boolean updateDataSp(HashMap<String, String> params) {
        SharedPreferences.Editor editor = Tools.getDataSp().edit();
        Iterator<String> iterator = params.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            editor.putString(key, params.get(key));
        }
        return editor.commit();
    }

    /**
     * 拿取SP中的全部数据
     *
     * @return
     */
    public static Map<String, ?> getDataParams() {
        SharedPreferences sp = Tools.getDataSp();
        return sp.getAll();
    }

    /**
     * 获取SP中的某个数据
     *
     * @param key
     * @return
     */
    public static String getDataParam(String key) {
        SharedPreferences sp = Tools.getDataSp();
        return sp.getString(key, null);
    }

    /**
     * 获取SP中的某个数据
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getDataParam(String key, String defaultValue) {
        SharedPreferences sp = Tools.getDataSp();
        return sp.getString(key, defaultValue);
    }

    /**
     * 清空SP
     *
     * @return
     */
    public static boolean clearDataSp() {
        SharedPreferences.Editor editor = Tools.getDataSp().edit();
        return editor.clear().commit();
    }

    /**
     * 单条数据删除
     *
     * @param key
     * @return
     */
    public static boolean removeDataSp(String key) {
        SharedPreferences.Editor editor = Tools.getDataSp().edit();
        return editor.remove(key).commit();
    }

    public static String getStrEncode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return value;
        }
    }

    /**
     * 通过包名 在应用商店打开应用
     *
     * @param packageName
     * @param activity
     */
    public static void openApplicationMarket(String packageName, Activity activity) {
        try {
            String str = "market://details?id=" + packageName;
            Intent localIntent = new Intent(Intent.ACTION_VIEW);
            localIntent.setData(Uri.parse(str));
            activity.startActivity(localIntent);
        } catch (Exception e) { //打开应用商店失败 可能是没有手机没有安装应用市场
            Tools.openLinkBySystem(activity);
        }
    }

    /**
     * 调用系统浏览器打开网页
     */
    public static void openLinkBySystem(Activity activity) {

    }

    /**
     * 判断是否gif图片
     *
     * @param imgPath
     * @return
     */
    public static boolean isGif(String imgPath) {
        if (!StringUtils.isEmpty(imgPath)) {
            int index = imgPath.lastIndexOf(".");
            if (index != -1) {
                String type = imgPath.substring(index + 1).toLowerCase();
                if (type.startsWith("gif")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 通过拍照生成的Uri，获取文件绝对路径
     *
     * @param uri
     * @return
     */
    public static String getFPUriToPath(Uri uri) {
        String path = uri.getEncodedPath();
        int splitIndex = path.indexOf('/', 1);
        path = Uri.decode(path.substring(splitIndex + 1));
        File file = new File(File.separator, path);
        return file.getAbsolutePath();
    }

    /**
     * 根据uri获取文件绝对路径
     *
     * @param context
     * @param uri
     * @return
     */
    public static String getPathForUri(Context context, Uri uri) {
        String authority = UIHelper.getPackageName() + ".fileProvider";
        String imgPath;
        if (authority.equals(uri.getAuthority())) {
            imgPath = Tools.getFPUriToPath(uri);
        } else {
            imgPath = PhotoMetadataUtils.getPath(context.getContentResolver(), uri);
        }
        return imgPath;
    }

    public static int getPxByDp(int dp) {
        float scale = TUIKit.getAppContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * 获取七牛图片完整路径
     *
     * @param photo
     * @return
     */
    public static String getPhoto(String photo) {
        return Constants.QINIU_IMG_URL + photo;
    }

    //由出生日期获得年龄
    public static int getAge(String format, String birthDayStr) {
        Date birthDay = Tools.getDate(birthDayStr, format);
        if (birthDay == null) return 0;
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) return 0;
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            } else {
                age--;
            }
        }
        return age;
    }

    //由出生日期获得星座
    public static String getConstellation(String format, String birthDayStr) {
        Date birthDay = Tools.getDate(birthDayStr, format);
        if (birthDay == null) return null;
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) return null;
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        String constellation = null;
        if (month == 1 && day >= 20 || month == 2 && day <= 18) {
            constellation = "水瓶座";
        }
        if (month == 2 && day >= 19 || month == 3 && day <= 20) {
            constellation = "双鱼座";
        }
        if (month == 3 && day >= 21 || month == 4 && day <= 19) {
            constellation = "白羊座";
        }
        if (month == 4 && day >= 20 || month == 5 && day <= 20) {
            constellation = "金牛座";
        }
        if (month == 5 && day >= 21 || month == 6 && day <= 21) {
            constellation = "双子座";
        }
        if (month == 6 && day >= 22 || month == 7 && day <= 22) {
            constellation = "巨蟹座";
        }
        if (month == 7 && day >= 23 || month == 8 && day <= 22) {
            constellation = "狮子座";
        }
        if (month == 8 && day >= 23 || month == 9 && day <= 22) {
            constellation = "处女座";
        }
        if (month == 9 && day >= 23 || month == 10 && day <= 23) {
            constellation = "天秤座";
        }
        if (month == 10 && day >= 24 || month == 11 && day <= 22) {
            constellation = "天蝎座";
        }
        if (month == 11 && day >= 23 || month == 12 && day <= 21) {
            constellation = "射手座";
        }
        if (month == 12 && day >= 22 || month == 1 && day <= 19) {
            constellation = "摩羯座";
        }
        return constellation;
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}