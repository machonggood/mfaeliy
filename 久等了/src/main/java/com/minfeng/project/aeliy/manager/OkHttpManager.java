package com.minfeng.project.aeliy.manager;

import com.minfeng.project.aeliy.entity.HeaderParam;
import com.minfeng.project.aeliy.entity.NormalParam;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.UIHelper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpManager {

	private int timeOut = 30;

	public interface ResultCallback {

		void onError();

		void onResponse(Response response);

	}

	public Result getSync(String url) {
		try {
			Call call = this.getCall(url);
			Response response = call.execute();
			if (response.isSuccessful()) {
				String resultStr = response.body().string();
				JSONObject obj = new JSONObject(resultStr);
				int code = obj.optInt("code");
				String msg = obj.optString("msg");
				String data = obj.optString("result");
				return new Result(null, Constants.SUCCESS, code, msg, data);
			}
		} catch (Exception e) {
			UIHelper.log(e.getMessage());
		}
		return new Result(null, Constants.ERROR);
	}

	public Call getAsync(final String url, final ResultCallback callback) {
		Call call = this.getCall(url);
		call.enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				callback.onError();
			}
			@Override
			public void onResponse(Call call, Response response) throws IOException {
				callback.onResponse(response);
			}
		});
		return call;
	}

	private Call getCall(String url) {
		List<NormalParam> normalParams = this.getGeneralNormalParams(url);
		StringBuilder urlSb = new StringBuilder(url);
		for (NormalParam normalParam: normalParams) {
			urlSb.append((urlSb.toString().contains("?") ? "&" : "?") + normalParam.getKey() + "=" + normalParam.getValue());
		}
		OkHttpClient mOkHttpClient = new OkHttpClient.Builder()
				.connectTimeout(timeOut, TimeUnit.SECONDS)
				.writeTimeout(timeOut, TimeUnit.SECONDS)
				.readTimeout(timeOut, TimeUnit.SECONDS).build();
		Builder rBuilder = new Builder().url(urlSb.toString());
		rBuilder.header("Content-Type", "application/json;charset=UTF-8");
		return mOkHttpClient.newCall(rBuilder.build());
	}

	/**
	 * 异步的post请求
	 *
	 * @param url
	 * @param normalParams
	 * @param callback
	 */
	public Call postAsync(final String url, List<NormalParam> normalParams, List<HeaderParam> headerParams, final ResultCallback callback) {
		Call call = this.postCall(url, normalParams, headerParams);
		call.enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				callback.onError();
			}
			@Override
			public void onResponse(Call call, Response response) throws IOException {
				callback.onResponse(response);
			}
		});
		return call;
	}

	/**
	 * 异步的post请求
	 * @param url
	 * @param normalParams
	 * @param callback
	 */
	public Call postAsync(String url, List<NormalParam> normalParams, final ResultCallback callback) {
		return this.postAsync(url, normalParams, null, callback);
	}

    private Call postCall(String url, List<NormalParam> normalParams, List<HeaderParam> headerParams) {
		OkHttpClient mOkHttpClient = new OkHttpClient.Builder()
				.connectTimeout(timeOut, TimeUnit.SECONDS)
				.writeTimeout(timeOut, TimeUnit.SECONDS)
				.readTimeout(timeOut, TimeUnit.SECONDS).build();
		FormBody.Builder fBuilder = new FormBody.Builder();
		if (normalParams == null) {
			normalParams = this.getGeneralNormalParams(url);
		} else {
			normalParams.addAll(this.getGeneralNormalParams(url));
		}
		for (NormalParam normalParam : normalParams) {
			fBuilder.add(normalParam.getKey(), normalParam.getValue());
		}
		RequestBody requestBody = fBuilder.build();
		Builder rBuilder = new Builder();
		if (headerParams == null) {
			headerParams = this.getGeneralHeaderParams();
		} else {
			headerParams.addAll(this.getGeneralHeaderParams());
		}
		if (headerParams != null) {
			for (HeaderParam headerParam: headerParams) {
				rBuilder.addHeader(headerParam.getKey(), headerParam.getValue());
			}
		}
		rBuilder.header("Content-Type", "application/x-www-form-urlencoded");
		Request request = rBuilder.url(url).post(requestBody).build();
		Call call = mOkHttpClient.newCall(request);
		return call;
	}

	private List<HeaderParam> getGeneralHeaderParams() {
		List<HeaderParam> headerParams = new ArrayList<>();
		return headerParams;
	}

	private List<NormalParam> getGeneralNormalParams(String url) {
		List<NormalParam> normalParams = new ArrayList<>();
		return normalParams;
	}
}
