package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.FileParam;
import com.minfeng.project.aeliy.utils.Tools;
import com.minfeng.project.aeliy.view.ConstrainImageView2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 朋友圈适配器
 */
public class FriendCirclePushAdapter extends GeneralAdapter<Uri> {

    public FriendCirclePushAdapter(Context context) {
        super(context);
    }

    @Override
    public int getCount() {
        return this.objects.size() < 9 ? this.objects.size() + 1 : this.objects.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ImageView imageView = new ConstrainImageView2(App.mContext);
        if (this.objects.size() < 9 && position == this.getCount() - 1) {
            imageView.setImageResource(R.mipmap.icon_addpic_focused);
        } else {
            Uri uri = (Uri) this.getItem(position);
            String imgPath = Tools.getPathForUri(App.mContext, uri);
            RequestOptions options = new RequestOptions().placeholder(R.mipmap.default_square_pic).centerCrop();
            Glide.with(App.mContext).load(imgPath).apply(options).into(imageView);
        }
        return imageView;
    }

    public int getObjectCount() {
        return this.objects.size();
    }

    public ArrayList<Uri> getObjects() {
        return this.objects;
    }

    public ArrayList<FileParam> getFileParams() {
        ArrayList<FileParam> fileParams = new ArrayList<>();
        List<Uri> uris = this.getObjects();
        for (Uri uri: uris) {
            File file = new File(Tools.getPathForUri(App.mContext, uri));
            fileParams.add(new FileParam(file.getName(), file));
        }
        return fileParams;
    }
}
