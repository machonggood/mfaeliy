package com.minfeng.project.aeliy.mvp.model;

import android.app.FragmentManager;

import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.manager.RequestManager;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.utils.Constants;

import java.util.HashMap;
import java.util.Map;

public class SystemModel {

    /**
     * 发送验证码
     * @param tag
     * @param phone
     * @param fragmentManager
     * @param resultCallBack
     */
    public void sendCode(String tag, String phone, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/sendLoginCaptcha");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        paramMaps.put("phone", phone);
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 获取绑定手机验证码
     * @param tag
     * @param phone
     * @param fragmentManager
     * @param resultCallBack
     */
    public void sendBindCode(String tag, String phone, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/sendBindCaptcha");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        paramMaps.put("phone", phone);
        paramMaps.put("access_token", tokenInfo.getToken());
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 获取修改密码手机验证码
     * @param tag
     * @param fragmentManager
     * @param resultCallBack
     */
    public void sendUpdatePwdCaptcha(String tag, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/sendUpdatePwdCaptcha");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        paramMaps.put("access_token", tokenInfo.getToken());
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 电话验证码登录
     * @param tag
     * @param phone
     * @param authCode
     * @param fragmentManager
     * @param resultCallBack
     */
    public void loginByPhone(String tag, String phone, String authCode, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/loginByPhone");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        paramMaps.put("phone", phone);
        paramMaps.put("captcha", authCode);
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 手机号码绑定
     * @param tag
     * @param phone
     * @param authCode
     * @param fragmentManager
     * @param resultCallBack
     */
    public void bindByPhone(String tag, String phone, String authCode, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/bindPhone");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        paramMaps.put("phone", phone);
        paramMaps.put("captcha", authCode);
        paramMaps.put("access_token", tokenInfo.getToken());
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 设置密码
     * @param tag
     * @param password
     * @param captcha
     * @param fragmentManager
     * @param resultCallBack
     */
    public void updatePwd(String tag, String password, String captcha, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/updatePwd");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        paramMaps.put("captcha", captcha);
        paramMaps.put("password", password);
        paramMaps.put("access_token", tokenInfo.getToken());
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 密码登录
     * @param tag
     * @param phone
     * @param pass
     * @param fragmentManager
     * @param resultCallBack
     */
    public void loginByPhonePwd(String tag, String phone, String pass, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.USERS_URL);
        stringBuffer.append("/loginByPhonePwd");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        paramMaps.put("phone", phone);
        paramMaps.put("password", pass);
        rManager.postAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * 微信登录
     * @param tag
     * @param openId
     * @param refreshToken
     * @param nickName
     * @param iconUrl
     * @param fragmentManager
     * @param resultCallBack
     */
    public void wxLogin(String tag, String openId, String refreshToken, String nickName, String iconUrl, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.AUTH_URL);
        stringBuffer.append("/wechat");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        paramMaps.put("openid", openId);
        paramMaps.put("nickname", nickName);
        paramMaps.put("headimgurl", iconUrl);
        paramMaps.put("refresh_token", refreshToken);
        rManager.getAsync(stringBuffer.toString(), paramMaps);
    }

    /**
     * qq登录
     * @param tag
     * @param openid
     * @param token
     * @param fragmentManager
     * @param resultCallBack
     */
    public void qqLogin(String tag, String openid, String token, FragmentManager fragmentManager, final ResultCallBack resultCallBack) {
        StringBuffer stringBuffer = new StringBuffer(Constants.AUTH_URL);
        stringBuffer.append("/qq");
        RequestManager rManager = new RequestManager(tag, new RequestManager.ResultCallback() {
            @Override
            public void findDataCallBack(Result result) {
                resultCallBack.findDataCallBack(result, null);
            }
        });
        rManager.showDialog(fragmentManager);
        Map<String, String> paramMaps = new HashMap<>();
        paramMaps.put("openid", openid);
        paramMaps.put("token", token);
        rManager.getAsync(stringBuffer.toString(), paramMaps);
    }
}
