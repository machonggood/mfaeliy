package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchResult;
import com.baidu.mapapi.search.poi.PoiIndoorResult;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.poi.PoiSortType;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.GeneralAdapter;
import com.minfeng.project.aeliy.adapter.LocationAdapter;
import com.minfeng.project.aeliy.application.App;

import java.util.List;

/**
 * 朋友圈发布
 */
public class LocationActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener, OnGetPoiSearchResultListener {

    private ListView locationLv;
    private PoiSearch mPoiSearch;
    private ImageButton leftView;
    private SpringView springView;
    private GeneralAdapter baseAdapter;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_location);
    }

    @Override
    protected void initViews() {
        this.refreshLayout = super.findViewById(R.id.general_refresh_list_sr_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.springView = super.findViewById(R.id.general_refresh_list_sp_view);
        this.locationLv = super.findViewById(R.id.general_refresh_list_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.locationLv.setBackgroundColor(Color.parseColor("#f5f5f5"));
        this.locationLv.setDivider(new ColorDrawable(Color.parseColor("#e4e4e4")));
        this.locationLv.setDividerHeight(super.getResources().getDimensionPixelOffset(R.dimen.item_1));
        this.springView.setHeader(new DefaultHeader(App.mContext));
        this.springView.setFooter(new DefaultFooter(App.mContext));
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        this.baseAdapter = new LocationAdapter(App.mContext);
        this.mPoiSearch = PoiSearch.newInstance();
        this.springView.setEnableHeader(false);
        this.springView.setEnableFooter(false);
        headerTv.setTextColor(Color.BLACK);
        headerTv.setText("所在位置");
        this.locationLv.setAdapter(this.baseAdapter);
        this.setRefreshing(this.refreshLayout, true);
    }

    @Override
    protected void initListener() {
        this.locationLv.setOnItemClickListener(new ClickItemListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.mPoiSearch.setOnGetPoiSearchResultListener(this);
        this.refreshLayout.setOnRefreshListener(this);
        this.locationLv.setOnScrollListener(this);
    }

    @Override
    public void onDelayItemClick(AdapterView<?> parent, View view, int position, long id) {
        PoiInfo poiInfo = (PoiInfo) parent.getItemAtPosition(position);
        Intent intent = new Intent();
        intent.putExtra("poiInfo", poiInfo);
        super.setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    protected void onDelayClick(View v) {

    }

    @Override
    public void onRefresh() {
        BDLocation bdLocation = ((App) super.getApplication()).getBdLocation();
        PoiNearbySearchOption option = new PoiNearbySearchOption();
        option.keyword("写字楼");
        option.sortType(PoiSortType.distance_from_near_to_far);
        option.location(new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude()));
        option.radius(1000);
        option.pageCapacity(20);
        this.mPoiSearch.searchNearby(option);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        boolean enable = true;
        if (view != null && view.getChildCount() > 0) {
            boolean firstItemVisible = view.getFirstVisiblePosition() == 0;
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() == 0;
            enable = firstItemVisible && topOfFirstItemVisible;
        }
        this.refreshLayout.setEnabled(enable);
    }

    private void setRefreshing(final SwipeRefreshLayout mSRLayout, final boolean refreshing) {
        mSRLayout.post(new Runnable() {
            @Override
            public void run() {
                mSRLayout.setRefreshing(refreshing);
                if (refreshing) onRefresh();
            }
        });
    }

    @Override
    public void onGetPoiResult(PoiResult poiResult) {
        if (poiResult != null) {
            List<PoiInfo> poiInfos = poiResult.getAllPoi();
            if (poiInfos != null && poiInfos.size() > 0) {
                poiInfos.add(0, null);
                this.baseAdapter.onRefresh(poiInfos, true);
            }
        }
        this.setRefreshing(this.refreshLayout, false);
    }

    @Override
    public void onGetPoiDetailResult(PoiDetailResult poiDetailResult) {

    }

    @Override
    public void onGetPoiDetailResult(PoiDetailSearchResult poiDetailSearchResult) {

    }

    @Override
    public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {

    }
}
