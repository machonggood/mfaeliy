package com.minfeng.project.aeliy.activity;

import android.app.AppOpsManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.manager.TokenManager;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.mvp.presenter.SystemPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.SDCard;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareConfig;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LauncherActivity extends BaseDownActivity<IBaseView, SystemPresenter> implements IBaseView, UMAuthListener {

    private Button loginBt;
    private ImageButton wxIb, qqIb;

    @Override
    protected void updateCancelOperate() {
        /**
         * TODO 这里是取消更新APP的操作
         */
    }

    @Override
    protected void goAppStoreUpdate() {
        /**
         * TODO 这里跳转去网络地址下载APP
         */
    }

    @Override
    protected void onGranted() {
        if (UIHelper.isGetWritePermission()) {
            File imgFile = new File(SDCard.getSDCardPath(), Constants.CACHE_IMG_PATH);
            if (!imgFile.exists()) imgFile.mkdirs();
            File apkFile = new File(SDCard.getSDCardPath(), Constants.CACHE_APK_PATH);
            if (!apkFile.exists()) apkFile.mkdirs();
        }
    }

    @Override
    protected HashMap<String, String> getPermissionOps() {
        HashMap<String, String> permissionOps = new HashMap<>();
        permissionOps.put(WRITE_EXTERNAL_STORAGE, AppOpsManager.OPSTR_WRITE_EXTERNAL_STORAGE);  //读写权限
        permissionOps.put(ACCESS_FINE_LOCATION, AppOpsManager.OPSTR_FINE_LOCATION); //定位权限
        permissionOps.put(READ_PHONE_STATE, AppOpsManager.OPSTR_READ_PHONE_STATE);  //手机状态
        permissionOps.put(CAMERA, AppOpsManager.OPSTR_CAMERA);  //相机权限
        return permissionOps;
    }

    @Override
    public SystemPresenter createPresenter() {
        return new SystemPresenter();
    }

    @Override
    protected void initListener() {
        this.loginBt.setOnClickListener(new ClickListener());
        this.wxIb.setOnClickListener(new ClickListener());
        this.qqIb.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_launcher_login_bt) {
            this.clearCache();
            Intent intent = new Intent(this, MainActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_launcher_qq_ib) {
            this.startPlatformLogin(SHARE_MEDIA.QQ);
        } else if (v.getId() == R.id.activity_launcher_wx_ib) {
            this.startPlatformLogin(SHARE_MEDIA.WEIXIN);
        }
    }

    /**
     * 清除缓存，断开连接
     */
    private void clearCache() {
        UserManager.Instance().setUserInfo(null);
        TokenManager.Instance().setTokenInfo(null);
        SocketManager.Instance().disconnectSocket();
    }

    private void startPlatformLogin(SHARE_MEDIA share_media) {
        this.clearCache();
        UMShareAPI umShareAPI = UMShareAPI.get(App.mContext);
        boolean isInstall = umShareAPI.isInstall(this, share_media);
        if (isInstall) {
            UMShareConfig config = new UMShareConfig();
            config.isNeedAuthOnGetUserInfo(true);
            umShareAPI.setShareConfig(config);
            umShareAPI.getPlatformInfo(this, share_media, this);
        } else {
            UIHelper.showToast("没有检测到客户端!");
        }
    }

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_launcher);
    }

    @Override
    protected void initViews() {
        super.initViews();
        super.requestPermission();
        TokenInfo tokenInfo = TokenManager.Instance().getTokenInfo();
        this.qqIb = super.findViewById(R.id.activity_launcher_qq_ib);
        this.wxIb = super.findViewById(R.id.activity_launcher_wx_ib);
//        if (tokenInfo != null) SocketManager.Instance().connectSocket();
        SocketManager.Instance().connectSocket();
        this.loginBt = super.findViewById(R.id.activity_launcher_login_bt);
    }

    @Override
    protected List<String> getMethodNames() {
        List<String> methodNames = super.getMethodNames();
        methodNames.add("userLoginSuccess");            //Socket已连接，登录成功
        return methodNames;
    }

    /**
     * socket连接成功
     */
    public void userLoginSuccess(UserInfo userInfo) {
        UIHelper.userLoginSuccess(this.mActivity, userInfo);
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 200) {
            if (!StringUtils.isEmpty(result.getData())) {
                TokenInfo tokenInfo = new Gson().fromJson(result.getData(), TokenInfo.class);
                TokenManager.Instance().setTokenInfo(tokenInfo);
                if (tokenInfo.isNew_user()) {
                    Intent intent = new Intent(this, BindingPhoneActivity.class);
                    super.startActivity(intent);
                } else {
                    SocketManager.Instance().connectSocket();
                }
                return;
            }
        }
        UIHelper.showToast(result, "请求失败，请重试！");
    }

    /**
     * @param platform 平台名称
     * @desc 授权开始的回调
     */
    @Override
    public void onStart(SHARE_MEDIA platform) {
        UIHelper.log("授权开始");
    }

    /**
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     * @param data     用户资料返回
     * @desc 授权成功的回调
     */
    @Override
    public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
        if (platform == SHARE_MEDIA.WEIXIN) {
            String name = data.get("name");         //昵称
            String openId = data.get("openid");
            String iconUrl = data.get("iconurl");   //头像
            String refreshToken = data.get("refreshToken");
            this.mPresenter.wxLogin(openId, refreshToken, name, iconUrl);
        } else {
            String openid = data.get("openid");
            String token = data.get("accessToken");
            this.mPresenter.qqLogin(openid, token);
        }
    }

    /**
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     * @param t        错误原因
     * @desc 授权失败的回调
     */
    @Override
    public void onError(SHARE_MEDIA platform, int action, Throwable t) {
        UIHelper.log("授权失败:" + t.getMessage());
        UIHelper.showToast("授权失败！");
    }

    /**
     * @param platform 平台名称
     * @param action   行为序号，开发者用不上
     * @desc 授权取消的回调
     */
    @Override
    public void onCancel(SHARE_MEDIA platform, int action) {
        UIHelper.log("授权取消");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) return true;
        return super.onKeyDown(keyCode, event);
    }
}
