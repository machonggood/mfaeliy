package com.minfeng.project.aeliy.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;
import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.DiamondAdapter;
import com.minfeng.project.aeliy.adapter.GeneralAdapter;
import com.minfeng.project.aeliy.application.App;
import com.minfeng.project.aeliy.entity.Result;
import com.minfeng.project.aeliy.mvp.presenter.DiamondPresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.utils.Constants;

import java.lang.reflect.Type;
import java.util.List;

/**
 * 我的钻石
 */
public class DiamondFragment extends BaseMvpFragment<IBaseView, DiamondPresenter> implements IBaseView, SpringView.OnFreshListener, SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {

    private int type;
    private ListView listView;
    private SpringView springView;
    private GeneralAdapter baseAdapter;
    private SwipeRefreshLayout refreshLayout;

    public static DiamondFragment newInstance(int type) {
        DiamondFragment fragment = new DiamondFragment();
        Bundle data = new Bundle();
        data.putInt("type", type);
        fragment.setArguments(data);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.swipe_refresh_list, container, false);
    }

    @Override
    protected void initViews(View view) {
        this.springView = view.findViewById(R.id.general_refresh_list_sp_view);
        this.type = super.getArguments().getInt("type", 0);
        this.listView = view.findViewById(R.id.general_refresh_list_view);
        this.springView.setFooter(new DefaultFooter(App.mContext));
        this.baseAdapter = new DiamondAdapter(this.mActivity);
        this.refreshLayout = (SwipeRefreshLayout) view;
        this.listView.setDivider(new ColorDrawable( ContextCompat.getColor(this.mActivity, R.color.color_eeeeee)));
        this.listView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.item_0_5));
        this.listView.setAdapter(this.baseAdapter);
        this.springView.setEnableHeader(false);
        this.springView.setEnableFooter(false);
        this.initListener();
    }

    @Override
    protected void loadData() {
        boolean isNewsVerify = this.baseAdapter != null && this.baseAdapter.getCount() == 0;
        if (isNewsVerify) this.setRefreshing(this.refreshLayout, true);
    }

    @Override
    protected void initListener() {
        this.refreshLayout.setOnRefreshListener(this);
        this.listView.setOnScrollListener(this);
        this.springView.setListener(this);
    }

    /**
     * 下拉刷新
     */
    @Override
    public void onRefresh() {
        this.mPresenter.findDiamondBill(this.type, 1);
    }

    /**
     * 加载更多
     */
    @Override
    public void onLoadmore() {
        int pageIndex = 1;
        if (this.baseAdapter != null && this.baseAdapter.getCount() > 0) {
            pageIndex += (int) Math.ceil(this.baseAdapter.getCount() / 20.f);
        }
        this.mPresenter.findDiamondBill(this.type, pageIndex);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        boolean enable = true;
        if (view != null && view.getChildCount() > 0) {
            boolean firstItemVisible = view.getFirstVisiblePosition() == 0;
            boolean topOfFirstItemVisible = view.getChildAt(0).getTop() == 0;
            enable = firstItemVisible && topOfFirstItemVisible;
        }
        this.refreshLayout.setEnabled(enable);
    }

    private void setRefreshing(final SwipeRefreshLayout mSRLayout, final boolean refreshing) {
        mSRLayout.post(new Runnable() {
            @Override
            public void run() {
                mSRLayout.setRefreshing(refreshing);
                if (refreshing) onRefresh();
            }
        });
    }

    @Override
    public DiamondPresenter createPresenter() {
        return new DiamondPresenter();
    }

    @Override
    public void findDataCallBack(Result result, Object object) {
        this.springView.setEnableFooter(false);
        if (result.getStatus() == Constants.SUCCESS && result.getRetCode() == 101) {
            int page = Integer.parseInt(object.toString());
            Type typeOfT = new TypeToken<List<String>>() {}.getType();
            List<String> diamonds = new Gson().fromJson(result.getData(), typeOfT);
            this.baseAdapter.onRefresh(diamonds, page == 1);
//            this.springView.setEnableFooter(rechargeRecord.getPageIndex() < rechargeRecord.getPageCount());
        }
        this.fragmentHide();
    }

    @Override
    protected void fragmentHide() {
        this.springView.onFinishFreshAndLoad();
        this.setRefreshing(this.refreshLayout, false);
    }
}
