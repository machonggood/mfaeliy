package com.minfeng.project.aeliy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.manager.ActivityManager;
import com.minfeng.project.aeliy.utils.UIHelper;

/**
 * 设置性别
 */
public class setSexActivity extends BaseActivity {

    private Button confirmBt;
    private RadioGroup sexRg;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_set_sex);
    }

    @Override
    protected void initViews() {
        this.confirmBt = super.findViewById(R.id.activity_set_sex_confirm_bt);
        this.sexRg = super.findViewById(R.id.activity_set_sex_rg);
    }

    @Override
    protected void initListener() {
        this.confirmBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_set_sex_confirm_bt) {
            int checkId = this.sexRg.getCheckedRadioButtonId();
            if (checkId == -1) {
                UIHelper.showToast("请选择您的性别！");
            } else {
                Intent intent = new Intent(this, setNickBirthdayActivity.class);
                int sex = checkId == R.id.activity_set_sex_man_rb ? 1 : 0;
                intent.putExtra("sex", sex);
                super.startActivity(intent);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) return true;
        return super.onKeyDown(keyCode, event);
    }
}
