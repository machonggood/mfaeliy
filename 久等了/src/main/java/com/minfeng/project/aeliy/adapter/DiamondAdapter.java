package com.minfeng.project.aeliy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.application.App;

public class DiamondAdapter extends GeneralAdapter<String> {

    public DiamondAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(App.mContext).inflate(R.layout.diamond_bill_item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        return view;
    }

    static class ViewHolder {

        public ViewHolder(View view) {

        }
    }
}
