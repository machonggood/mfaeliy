package com.minfeng.project.aeliy.manager;

import com.google.gson.Gson;
import com.minfeng.project.aeliy.entity.TokenInfo;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.StringUtils;
import com.minfeng.project.aeliy.utils.Tools;

/**
 * token管理器
 * @author 马冲
 * @version 1.0
 */
public class TokenManager {

    private TokenInfo tokenInfo;
    private volatile static TokenManager instance = null;

    public static TokenManager Instance() {
        if (instance == null) {
            synchronized (TokenManager.class) {
                if (instance == null) {
                    instance = new TokenManager();
                }
            }
        }
        return instance;
    }

    public TokenInfo getTokenInfo() {
        String tokenInfoStr = Tools.getDataParam(Constants.User.TOKEN_INFO);
        if (!StringUtils.isEmpty(tokenInfoStr)) {
            return new Gson().fromJson(tokenInfoStr, TokenInfo.class);
        }
        return null;
    }

    public void setTokenInfo(TokenInfo tokenInfo) {
        String tokenInfoStr = new Gson().toJson(this.tokenInfo = tokenInfo);
        Tools.updateDataSp(Constants.User.TOKEN_INFO, tokenInfoStr);
    }
}
