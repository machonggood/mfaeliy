package com.minfeng.project.aeliy.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.BasePagerAdapter;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.fragment.LikeMeFragment;
import com.minfeng.project.aeliy.fragment.ManVipFragment;
import com.minfeng.project.aeliy.fragment.ShoppingMallFragment;
import com.minfeng.project.aeliy.fragment.WoManVipFragment;
import com.minfeng.project.aeliy.manager.UserManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户特权
 */
public class UserPrivilegeActivity extends BaseActivity {

    private Button rightBt;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton leftView;
    private BasePagerAdapter pagerAdapter;
    private View diamondView, rechargeView;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_user_privilege);
    }

    @Override
    protected void initViews() {
        this.rechargeView = super.findViewById(R.id.activity_user_privilege_recharge_tv);
        this.diamondView = super.findViewById(R.id.activity_user_privilege_diamond_tv);
        this.tabLayout = super.findViewById(R.id.activity_user_privilege_tab_layout);
        this.viewPager = super.findViewById(R.id.activity_user_privilege_pager_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.rightBt = super.findViewById(R.id.header_layout_right_operate_view);
        TextView headerTv = super.findViewById(R.id.header_layout_header_tv);
        this.leftView.setImageResource(R.mipmap.icon_black_back);
        this.rightBt.setTextColor(Color.BLACK);
        headerTv.setTextColor(Color.BLACK);
        this.rightBt.setText("常见问题");
        headerTv.setText("我的特权");

        List<Fragment> tabFragments = this.getViews();
        this.pagerAdapter = new BasePagerAdapter(super.getSupportFragmentManager(), tabFragments);
        this.viewPager.setOffscreenPageLimit(tabFragments.size());
        this.tabLayout.setupWithViewPager(this.viewPager);
        this.viewPager.setAdapter(this.pagerAdapter);
        this.viewPager.setCurrentItem(0);
        this.setTabLayout();
    }

    @SuppressLint("InflateParams")
    private List<Fragment> getViews() {
        List<Fragment> views = new ArrayList<>();
        UserInfo userInfo = UserManager.Instance().getUserInfo();
        if (userInfo != null && userInfo.getUser().getSex() == 0) {
            views.add(WoManVipFragment.newInstance());
        } else {
            views.add(ManVipFragment.newInstance());
        }
        views.add(LikeMeFragment.newInstance());
        views.add(ShoppingMallFragment.newInstance());
        return views;
    }

    private void setTabLayout() {
        String[] items = {"VIP特权", "谁喜欢我", "道具商城"};
        for (int i = 0; i < items.length; i++) {
            View view = this.buildView(items[i]);
            TabLayout.Tab tab = this.tabLayout.getTabAt(i);
            tab.setCustomView(view);
        }
    }

    private View buildView(String tabTxt) {
        TextView tabTextView = (TextView) LayoutInflater.from(this).inflate(R.layout.view_pager_indicator, null);
        tabTextView.setText(tabTxt);
        return tabTextView;
    }

    @Override
    protected void initListener() {
        this.rechargeView.setOnClickListener(new ClickListener());
        this.diamondView.setOnClickListener(new ClickListener());
        this.leftView.setOnClickListener(new ClickListener());
        this.rightBt.setOnClickListener(new ClickListener());
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.header_layout_right_operate_view) {
            Intent intent = new Intent(this, UseFagActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_user_privilege_diamond_tv) {
            Intent intent = new Intent(this, UserDiamondActivity.class);
            super.startActivity(intent);
        } else if (v.getId() == R.id.activity_user_privilege_recharge_tv) { //充值
            Intent intent = new Intent(this, UserRechargeActivity.class);
            super.startActivity(intent);
        }
    }


}
