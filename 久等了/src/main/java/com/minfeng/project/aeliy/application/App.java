package com.minfeng.project.aeliy.application;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.SDKInitializer;
import com.google.gson.Gson;
import com.minfeng.project.aeliy.entity.UserInfo;
import com.minfeng.project.aeliy.im.BaseUIKitConfigs;
import com.minfeng.project.aeliy.im.TUIKit;
import com.minfeng.project.aeliy.manager.LocationManager;
import com.minfeng.project.aeliy.manager.LocationManager.LocationBody;
import com.minfeng.project.aeliy.manager.LocationManager.LocationListener;
import com.minfeng.project.aeliy.manager.UserManager;
import com.minfeng.project.aeliy.socket.bean.LocationReport;
import com.minfeng.project.aeliy.socket.manager.SocketManager;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.UIHelper;
import com.tencent.imsdk.session.SessionWrapper;
import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.MobclickAgent.EScenarioType;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;

import cn.jpush.android.api.JPushInterface;

public class App extends MultiDexApplication implements LocationListener {

    private BDLocation bdLocation;
    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        this.initUmeng();
        this.initJpush();
        this.initBaiDuMap();
        this.initLocation();
        this.initIm();
    }

    private void initBaiDuMap() {
        SDKInitializer.initialize(this);
        BMapManager.init();
    }

    private void initLocation() {
        LocationBody locationBody = new LocationBody(Integer.MAX_VALUE, false, this);
        LocationManager.Instance().setLocationBody(locationBody);
    }

    public BDLocation getBdLocation() {
        return bdLocation;
    }

    {
        //微信分享
        PlatformConfig.setWeixin("wxa9bab46ab64d6151", "3b544791153665cf3b97abe7d6ac7d6c");
        //QQ分享
        PlatformConfig.setQQZone("1108738387", "YZngsTw9aKmgAYL4");
    }

    /**
     * 初始化友盟
     */
    private void initUmeng() {
        UMConfigure.init(App.mContext, UMConfigure.DEVICE_TYPE_PHONE, null);     //初始化友盟配置
        MobclickAgent.setScenarioType(App.mContext, EScenarioType.E_UM_NORMAL);     //设置统计场景
        UMConfigure.setEncryptEnabled(!Constants.DEBUG_MODE);   //设置日志加密
        UMConfigure.setLogEnabled(Constants.DEBUG_MODE);
    }

    /**
     * 初始化极光推送
     */
    private void initJpush() {
        JPushInterface.setDebugMode(Constants.DEBUG_MODE);
        JPushInterface.init(this);
    }

    private void initIm() {
        if (SessionWrapper.isMainProcess(super.getApplicationContext())) {
            TUIKit.init(this, Constants.SDK_IM_ID, BaseUIKitConfigs.getDefaultConfigs());
        }
    }

    @Override
    public void onLocationChanged(BDLocation bdLocation) {
        UIHelper.sendLocationReport(this.bdLocation = bdLocation);
    }

}
