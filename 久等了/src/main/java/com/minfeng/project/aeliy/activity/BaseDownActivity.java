package com.minfeng.project.aeliy.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.entity.VersionInfo;
import com.minfeng.project.aeliy.manager.DownloadManager;
import com.minfeng.project.aeliy.manager.DownloadManager.DownloadListener;
import com.minfeng.project.aeliy.mvp.presenter.BasePresenter;
import com.minfeng.project.aeliy.mvp.view.IBaseView;
import com.minfeng.project.aeliy.utils.Constants;
import com.minfeng.project.aeliy.utils.SDCard;
import com.minfeng.project.aeliy.utils.UIHelper;

import java.io.File;

/**
 * Activity下载管理器
 */
public abstract class BaseDownActivity<V extends IBaseView, T extends BasePresenter<V>> extends BasePermissionActivity<V, T> implements DownloadListener {

    /**
     * 提醒app升级
     */
    protected void alertAppUpdate(final VersionInfo versionInfo) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("发现新版本,请升级新版本获取更好的体验!");
        dialog.setCancelable(false);
        if (versionInfo.getIsForce() != 1) {        //1强制  2非强制
            dialog.setNegativeButton("稍后再说", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    updateCancelOperate();
                }
            });
        }
        dialog.setPositiveButton("立即升级", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String[] params = {versionInfo.getUpdateUrl(), getString(R.string.app_name) + ".apk"};
                new DownloadManager(mActivity, BaseDownActivity.this).execute(params);
            }
        });
        dialog.show();
    }

    @Override
    public void onPostExecute(File file) {
        if (file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                boolean haveInstallPermission = super.getPackageManager().canRequestPackageInstalls();
                if (!haveInstallPermission) {//没有权限
                    this.permissionConfirmDialog();
                    return;
                }
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {   // 判断版本大于等于7.0
                String authority = UIHelper.getPackageName() + ".fileProvider";
                data = FileProvider.getUriForFile(this, authority, file);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } else {
                data = Uri.fromFile(file);
            }
            intent.setDataAndType(data, "application/vnd.android.package-archive");
            this.startActivity(intent);
            super.finish();
        } else {
            this.appStoreUpdate("在线更新失败,建议去应用商城更新应用!");
        }
    }

    /**
     * 提示去商场中下载更新
     *
     * @param message
     */
    private void appStoreUpdate(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setNegativeButton("稍后再说", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateCancelOperate();
            }
        });
        dialog.setPositiveButton("现在就去", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goAppStoreUpdate();
            }
        });
        dialog.show();
    }

    /**
     * 未知源权限提示
     */
    private void permissionConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("安装应用需要打开未知来源权限，请去设置中开启权限!");
        dialog.setCancelable(false);
        dialog.setNegativeButton("稍后再说", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateCancelOperate();
            }
        });
        dialog.setPositiveButton("马上开启", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startInstallPermissionSettingActivity();
                }
            }
        });
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {
        Uri packageURI = Uri.parse("package:" + super.getPackageName());
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, packageURI);
        super.startActivityForResult(intent, 10086);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10086) {
            if (resultCode == RESULT_OK) {
                File baseFile = new File(SDCard.getSDCardPath(), Constants.CACHE_APK_PATH);
                File apkFile = new File(baseFile, super.getString(R.string.app_name) + ".apk");
                this.onPostExecute(apkFile);
            } else {
                this.appStoreUpdate("由于系统权限原因,建议去应用商城更新应用?");
            }
        }
    }

    /**
     * 更新取消操作
     */
    protected abstract void updateCancelOperate();

    /**
     * 去应用商城更新
     */
    protected abstract void goAppStoreUpdate();
}
