package com.minfeng.project.aeliy.im;

import android.content.Context;

import com.tencent.imsdk.TIMConnListener;
import com.tencent.imsdk.TIMConversation;
import com.tencent.imsdk.TIMGroupEventListener;
import com.tencent.imsdk.TIMGroupTipsElem;
import com.tencent.imsdk.TIMLogLevel;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMMessageListener;
import com.tencent.imsdk.TIMOfflinePushListener;
import com.tencent.imsdk.TIMOfflinePushNotification;
import com.tencent.imsdk.TIMRefreshListener;
import com.tencent.imsdk.TIMSdkConfig;
import com.tencent.imsdk.TIMUserConfig;
import com.tencent.imsdk.TIMUserStatusListener;
import com.tencent.imsdk.ext.message.TIMUserConfigMsgExt;

import java.util.List;

public class TUIKit {

    private static Context appContext;
    private static BaseUIKitConfigs baseConfigs;

    /**
     * TUIKit的初始化函数
     * @param context  应用的上下文，一般为对应应用的ApplicationContext
     * @param sdkAppID 您在腾讯云注册应用时分配的sdkAppID
     * @param configs  TUIKit的相关配置项，一般使用默认即可，需特殊配置参考API文档
     */
    public static void init(Context context, int sdkAppID, BaseUIKitConfigs configs) {
        appContext = context;
        baseConfigs = configs;
        initIM(context, sdkAppID);


//        long current = System.currentTimeMillis();
//        System.out.println("TUIKIT>>>>>>>>>>>>>>>>>>" + (System.currentTimeMillis() - current));
//        current = System.currentTimeMillis();
//
//        BackgroundTasks.initInstance();
//        FileUtil.initPath();
//        System.out.println("TUIKIT>>>>>>>>>>>>>>>>>>" + (System.currentTimeMillis() - current));
//        current = System.currentTimeMillis();
//        FaceManager.loadFaceFiles();
//        System.out.println("TUIKIT>>>>>>>>>>>>>>>>>>" + (System.currentTimeMillis() - current));
//
//        SessionManager.getInstance().init();
//        C2CChatManager.getInstance().init();
//        GroupChatManager.getInstance().init();
    }

    private static void initIM(Context context, int sdkAppID) {
        TIMSdkConfig config = getBaseConfigs().gettIMSdkConfig();
        if (config == null) {
            config = new TIMSdkConfig(sdkAppID).setLogLevel(TIMLogLevel.DEBUG);
            config.enableLogPrint(true);
        }
        TIMManager.getInstance().init(context, config);
        // 设置离线消息通知
        TIMManager.getInstance().setOfflinePushListener(new TIMOfflinePushListener() {
            @Override
            public void handleNotification(TIMOfflinePushNotification var1) {

            }
        });
        TIMUserConfig userConfig = new TIMUserConfig();
        userConfig.setUserStatusListener(new TIMUserStatusListener() {
            @Override
            public void onForceOffline() {      //被踢下线时回调
                if (baseConfigs.getiMEventListener() != null) {
                    baseConfigs.getiMEventListener().onForceOffline();
                }
                TUIKit.unInit();
            }
            @Override
            public void onUserSigExpired() {    //用户票据过期
                if (baseConfigs.getiMEventListener() != null) {
                    baseConfigs.getiMEventListener().onUserSigExpired();
                }
                TUIKit.unInit();
            }
        });
        userConfig.setConnectionListener(new TIMConnListener() {
            @Override
            public void onConnected() {         //连接建立
                if (getBaseConfigs().getiMEventListener() != null)
                    getBaseConfigs().getiMEventListener().onConnected();
            }
            @Override
            public void onDisconnected(int code, String desc) { //连接断开
                if (getBaseConfigs().getiMEventListener() != null)
                    getBaseConfigs().getiMEventListener().onDisconnected(code, desc);
            }
            @Override
            public void onWifiNeedAuth(String name) {   //WIFI需要验证
                if (getBaseConfigs().getiMEventListener() != null)
                    getBaseConfigs().getiMEventListener().onWifiNeedAuth(name);
            }
        });
        userConfig.setRefreshListener(new TIMRefreshListener() {
            @Override
            public void onRefresh() {

            }
            @Override
            public void onRefreshConversation(List<TIMConversation> conversations) {
//                SessionManager.getInstance().onRefreshConversation(conversations);
                if (TUIKit.getBaseConfigs().getiMEventListener() != null) {
                    TUIKit.getBaseConfigs().getiMEventListener().onRefreshConversation(conversations);
                }
            }
        });
        userConfig.setGroupEventListener(new TIMGroupEventListener() {
            @Override
            public void onGroupTipsEvent(TIMGroupTipsElem elem) {   //群Tips事件通知回调
                if (TUIKit.getBaseConfigs().getiMEventListener() != null) {
                    TUIKit.getBaseConfigs().getiMEventListener().onGroupTipsEvent(elem);
                }
            }
        });
        TIMManager.getInstance().addMessageListener(new TIMMessageListener() {
            @Override
            public boolean onNewMessages(List<TIMMessage> msgs) {   //收到新消息回调
                if (TUIKit.getBaseConfigs().getiMEventListener() != null) {
                    TUIKit.getBaseConfigs().getiMEventListener().onNewMessages(msgs);
                }
                return false;
            }
        });
        TIMUserConfigMsgExt ext = new TIMUserConfigMsgExt(userConfig);
        ext.setMessageRevokedListener(UIKitMessageRevokedManager.getInstance());
        // 禁用自动上报，通过调用已读上报接口来实现已读功能
        ext.setAutoReportEnabled(false);
        TIMManager.getInstance().setUserConfig(ext);
    }

    public static void unInit() {
//        SessionManager.getInstance().destroySession();
//        C2CChatManager.getInstance().destroyC2CChat();
//        GroupChatManager.getInstance().destroyGroupChat();
}

    public static Context getAppContext() {
        return appContext;
    }

    public static BaseUIKitConfigs getBaseConfigs() {
        return baseConfigs;
    }
}
