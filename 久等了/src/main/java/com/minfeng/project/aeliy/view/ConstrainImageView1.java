package com.minfeng.project.aeliy.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class ConstrainImageView1 extends ImageView {

    public ConstrainImageView1(Context context) {
        super(context);
    }

    public ConstrainImageView1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ConstrainImageView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.setMeasuredDimension(getDefaultSize(0, widthMeasureSpec), getDefaultSize(0, heightMeasureSpec));
        int childWidthSize = super.getMeasuredWidth();
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidthSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, (int) (widthMeasureSpec * 1.5));
    }

}
