package com.minfeng.project.aeliy.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.minfeng.project.aeliy.R;
import com.minfeng.project.aeliy.adapter.BasePagerAdapter;
import com.minfeng.project.aeliy.fragment.ImageCheckFragment;
import com.minfeng.project.aeliy.utils.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 网络图片查看器
 */
public class NetWorkImageCheckActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private Button finishBt;
    private ViewPager viewPager;
    private ImageButton leftView;
    private ArrayList<String> imageUrls;
    private BasePagerAdapter pagerAdapter;

    @Override
    protected void onMyCreate(Bundle savedInstanceState) {
        super.setContentView(R.layout.activity_image_check);
    }

    @Override
    protected void initViews() {
        this.imageUrls = super.getIntent().getStringArrayListExtra("imageUrls");
        int position = super.getIntent().getIntExtra("position", 0);
        View deleteView = super.findViewById(R.id.activity_image_check_delete_view);
        this.leftView = super.findViewById(R.id.header_layout_left_operate_view);
        this.finishBt = super.findViewById(R.id.activity_image_check_finish_bt);
        this.viewPager = super.findViewById(R.id.activity_image_check_vp);
        this.finishBt.setText((position + 1) + "/" + this.imageUrls.size());
        deleteView.setVisibility(View.GONE);

        List<Fragment> tabFragments = this.getViews(this.imageUrls);
        this.pagerAdapter = new BasePagerAdapter(this.getSupportFragmentManager(), tabFragments);
        this.viewPager.setOffscreenPageLimit(tabFragments.size());
        this.viewPager.setAdapter(this.pagerAdapter);
        this.viewPager.setCurrentItem(position);
    }

    @SuppressLint("InflateParams")
    private List<Fragment> getViews(ArrayList<String> imageUrls) {
        List<Fragment> views = new ArrayList<>();
        for (String imageUrl: imageUrls) {
            String imagePath = Tools.getPhoto(imageUrl);
            views.add(ImageCheckFragment.newInstance(imagePath, true));
        }
        return views;
    }

    @Override
    protected void initListener() {
        this.leftView.setOnClickListener(new ClickListener());
        this.finishBt.setOnClickListener(new ClickListener());
        this.viewPager.addOnPageChangeListener(this);
    }

    @Override
    protected void onDelayClick(View v) {
        if (v.getId() == R.id.activity_image_check_finish_bt) {
            super.finish();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.finishBt.setText((position + 1) + "/" + this.imageUrls.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
